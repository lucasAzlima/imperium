/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React,{useState, useEffect} from 'react';
import api from "../../services/api";
import './style.css';

function PerfilProfessor({history}) {
    const [leciona, setleciona] = useState([])
    const [professor, setprofessor] = useState([])

    useEffect(() => {
        setprofessor(history.location.state.data)

        Turmas(history.location.state.data.id);
      },[]);
  
      function Turmas(id) {
        const token = localStorage.getItem("token");
        api
          .get("/getTurmas", {
            headers: { Authorization: `Bearer ${token}` }
          })
          .then(res => {
            var dadosProfessor = [];
              res.data.forEach(turma =>{
              turma.materias.forEach(materia =>{
                  if(id === materia.idProf){
                    dadosProfessor.push({materia:materia.nome, turma: turma.ano})
                    }
                    //materia e nome da turma que o professor está associado
                  })
            })
            
            setleciona(dadosProfessor)
          })
          .catch(e => {
            console.log(e);
            console.log(
              "Erro de autenticação, verifique seu email e senha"
            );
          });
      }
  
    return (
      <div id="container-perfilprofessor">
        <div id="container-dadosPerfilprofessor">
          <h5>Dados do Professor</h5>
          <div id="dados-perfilprofessor">
              <p>Nome:  {professor.nome}</p>
              <p>Email:  {professor.email}</p>
              <p>Telefone:  {professor.telefone}</p>
              <p>Matrícula:  {professor.matricula}</p>
          </div>
        </div>
        <div id="container-professorAtivi">
          <div id="title-container-professorAtivi">
            <p>Turmas</p>
            <p>Matérias</p>
          </div>
          <div id="container-perfilprofessor-turmaeDisc">
            <table>
              {leciona.map(data=>
                <tr>
                  <td>
                    <p>{data.turma}</p>
                  </td>
                  <td>
                    <p>{data.materia}</p>
                  </td>
                </tr>
                )}
            </table>
          </div>
        </div>
      </div>
    );
}

export default PerfilProfessor;