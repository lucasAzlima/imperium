import React, { useState, useEffect } from "react";
import api from "../../services/api";
import firebase from "../../services/firebase";
import { Card, CardColumns, Button } from 'react-bootstrap'
import './styles.css'

export default function EditarNoticia({ history }) {
  const [Noticias, setNoticias] = useState([]);

  useEffect(() => {
    GetNotice();
  }, []);

  async function GetNotice() {
    const token = localStorage.getItem("token");
    await api
      .get("/getNoticiasInstituicao", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var data = [];
        res.data.map(async (Noticia, index) => {
          await firebase.storage().ref().child(Noticia.imagem).getDownloadURL().then(function (url) {
            Noticia.imagem = url;
          });
          data = data.concat(Noticia);
          setNoticias(data)
        })
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function deleteNotice(event) {
    const token = localStorage.getItem("token");
    api.put("/deleteNoticia",
      {
        id: event
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        firebase.storage().ref().child(res.data.path).delete();
        setNoticias([]);
        GetNotice();
      })
      .catch(e => {
        console.log(e);
      });
  }


  return (
    <div>
      <Button id='bt-cads' onClick={() => history.push('/cadastroDeNoticias')}>Cadastrar nova</Button>
      <Card id='card-ln'>
        <Card.Header >
          <Card.Subtitle style={{ fontWeight: 'bolder', textAlign: 'center' }}>Notícias Cadastradas</Card.Subtitle>
        </Card.Header>
        <CardColumns className='cardNoticiaT'>
          {Noticias.map((data, index) => {
            return (
              <Card id='cardNoticia' >
                <div>
                  <img id="card-img-top" src={data.imagem} alt="img-noticia" />
                  <Card.Body>
                    <Card.Subtitle >{data.titulo}</Card.Subtitle>
                    <LongText text={data.corpo} />
                  </Card.Body></div>
                <Card.Footer >
                  <Card.Body id='buttons-not'>
                    <Button onClick={() => history.push({ pathname: "/EditarNoticia", state: { data: data } })}>Editar</Button>
                    <Button value={data.id} onClick={event => deleteNotice(event.target.value)}>Excluir</Button>
                  </Card.Body>
                </Card.Footer>
              </Card>
            );
          })}
        </CardColumns>
      </Card>
    </div>
  );
}


function LongText(props) {
  const [showAll, setshowAll] = useState(false);

  if (props.text.length <= 50) {
    return <Card.Text>{props.text}</Card.Text>
  }
  if (showAll) {
    return <Card.Text>
      {props.text}
      <text onClick={() => setshowAll(false)} style={{color:'#323bb4'}}> Ler Menos</text>
    </Card.Text>
  }
  // In the final case, we show a text with ellipsis and a `Read more` button
  const toShow = props.text.substring(0, 50) + "...";
  return <div>
    <Card.Text>{toShow}<text onClick={() => setshowAll(true)} style={{color:'#323bb4'}}> Ler Mais</text></Card.Text>
    </div>
}
