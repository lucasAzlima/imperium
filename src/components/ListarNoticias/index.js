/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import firebase from "../../services/firebase";
import { Button, Form, Table, OverlayTrigger, Tooltip, Modal, ProgressBar } from 'react-bootstrap'
import './styles.css'
import { BsSearch } from "react-icons/bs";
import ReactPaginate from "react-paginate";
import { FaPencilAlt, FaTrash } from "react-icons/fa";
import Inot from '../../assets/imgnot.png';

export default function EditarNoticia({ history }) {
  const [Noticias, setNoticias] = useState([]);
  const [filter, setfilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const [postData, setpostData] = useState('');
  const [offset, setoffset] = useState(0);
  const perPage = 3;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const [isLoading, setisLoading] = useState(true);
  const filteredData = Noticias.filter(item => {
    return Object.keys(item).some(key => item['titulo'].toLowerCase().includes(lowercasedFilter));
  });
  useEffect(() => {
    GetNotice();
  }, []);

  async function GetNotice() {
    const token = localStorage.getItem("token");
    await api
      .get("/getNoticiasInstituicao", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var data = [];
        res.data.map(async (Noticia, index) => {
          await firebase.storage().ref().child(Noticia.imagem).getDownloadURL().then(function (url) {
            Noticia.imagem = url;
          }).catch(()=>{
            Noticia.imagem = 'NImage'
          });
          data = data.concat(Noticia);
          setNoticias(data)
        })
        setisLoading(false);
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function deleteNotice(event) {
    const token = localStorage.getItem("token");
    api.put("/deleteNoticia",
      {
        id: event
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        firebase.storage().ref().child(res.data.path).delete();
        setNoticias([]);
        GetNotice();
      })
      .catch(e => {
        console.log(e);
      });
  }

  useEffect(() => { receivedData(offset, filter); }, [offset, filter, Noticias])

  function receivedData(offset, filter) {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(pd =>


      <tr>
        <td><img src={(pd.imagem !== 'NImage')?pd.imagem: Inot} width="100vw" height="100vh" alt="img-noticia" /></td>
        <td>{pd.titulo}</td>
        <td><p>{pd.corpo.substring(0, 50) + "..."}</p></td>
        <td>
          <OverlayTrigger placement="auto" overlay={<Tooltip>Editar</Tooltip>}>
            <Button id="BotaoResp" 
            onClick={() => history.push({ pathname: "/EditarNoticia", state: { data: pd } })}>
              <FaPencilAlt size={25} />
            </Button>
          </OverlayTrigger>
          
          <OverlayTrigger placement="auto" overlay={<Tooltip>Excluir</Tooltip>}>
            <Button id="BotaoResp" 
            onClick={() => deleteNotice(pd.id)}>
              <FaTrash size={25} />
            </Button>
          </OverlayTrigger>
        </td>

      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);
  }

  useEffect(()=>{
    if(filter !== ''){
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  },[filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if(filter === ''){
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  return (
    <>
       <div id="tagBarraPesquisa" >
        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Nome'}
          />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>


        <Button id='bt-cads' onClick={() => history.push('/cadastroDeNoticias')}>Cadastrar</Button>
      </div>
      
      <div id='listagem-not'>
        <Table id="tabelaPadrao" borderless striped responsive="sm">
          <thead>
            <tr id="title-aluno-container">
              <th>Foto</th>
              <th>Titulo</th>
              <th>Corpo</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            {postData}
          </tbody>
        </Table>

        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"} 
          forcePage={forceNew}/>

      </div>
      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>
    </>
  );
}
