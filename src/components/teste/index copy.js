import React, { useState, useCallback } from 'react'
import { OverlayTrigger, Tooltip, Button } from "react-bootstrap"
import { MdCancel } from "react-icons/md";

const dogImg =
  'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000'

export default function Demo() {
  const [filename, setfilename] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");

  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
        cropImage(pictureConverter.result, 1 / 1));
         
      }
      catch (e) {
        console.log("Formato não suportado");
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {
        
        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };

  return (
    <div>
      <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0}} alt="" src={ImageShow} />
      <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0}} alt="" src={dogImg} />
      <div id='input-wrapper-EdN'>
        <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
          Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
          <div className="input-wrapper-EdN">
            <label for='input-file-EdN'>Alterar arquivo</label>
            <input type="file" onChange={(event) => setPic(event)} id='input-file-EdN' />
            <span id='file-name'>{filename}</span>
          </div>
        </OverlayTrigger>
        {(picture !== undefined) ?
          <MdCancel onClick={() => {
            setImage('')
            setfilename('')
            setPicture(undefined)
            document.getElementById("input-file-EdN").value = ''
          }} id='icon-cancel-ca' /> : <div />}
      </div>
    </div>
  )
}

