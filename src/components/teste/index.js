/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import './styles.css'
import { Card, Form, Button, Modal, Tabs, Tab, Table } from 'react-bootstrap'
import { FaUpload } from 'react-icons/fa';
import { useHistory } from 'react-router-dom'
import api from "../../services/api";
import firebase from "../../services/firebase";
import { BsSearch } from 'react-icons/bs';
import BtLoad from "../ButtonLoading"

export default function Disciplinas() {
  const [file, setfile] = useState(undefined);
  const [filename, setfilename] = useState("");
  const [titulo, settitulo] = useState("");
  const [corpo, setcorpo] = useState("");
  const [destin, setdestin] = useState(false);
  const history = useHistory();
  const [alunos, setalunos] = useState([]);
  const [turmas, setturmas] = useState([]);
  const [users, setusers] = useState([]);
  const [keys, setkeys] = useState([]);
  const [stateBt, setstateBt] = useState(false);
  const instituicao = localStorage.getItem('instituicao');
  const [show, setshow] = useState(false);
  const [Porcentagem, setPorcentagem] = useState(0);

  useEffect(() => {
    GetAluno();
    getTurmas();
    GetListaUsers();
  }, [])

  function GetAluno() {
    const token = localStorage.getItem("token");
    api
      .get("/getAlunos", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = []
        res.data.alunos.map(data =>
          trm.push({
            nome: data.nome,
            id: data.id,
            state: false,
            turma: data.turma
          }));

        setalunos(trm);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function getTurmas() {
    const token = localStorage.getItem("token");
    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = []
        res.data.map(data =>
          trm.push({
            ano: data.ano,
            id: data.id,
            state: false,
            turno: data.turno,
            sala: data.sala
          }));
        setturmas(trm);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function GetListaUsers() {
    const token = localStorage.getItem("token");
    api
      .get("/getListUsers", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setusers(res.data)
      })
      .catch(e => {
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function fileinput(file) {
    if (file.target.files[0] !== undefined)
      setfilename(file.target.files[0].name)
    setfile(file.target.files[0])
  }

  function cadasnotificacao(event) {
    event.preventDefault();
    var curr = new Date();
    curr.setDate(curr.getDate());
    var date = curr.toISOString().substr(0, 10);
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarNotificacoes",
        {
          titulo: titulo,
          corpo: corpo,
          key: keys,
          filename: filename,
          data: date,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        console.log(res.data.path)
        if (file !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(file, { contentType: 'application/pdf', })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) {
                setTimeout(() => { setshow(false); history.push("/Comunicados"); }, 1000);
              }
            });
          setfile("");
        }
        settitulo("");
        setcorpo("");
        setkeys([]);
        if (file === undefined) history.push("/Comunicados");
      })
      .catch(e => {
        console.log(e);
      });
  }

  return <div id='cadNotificacao'>
    <Card>
      <Card.Header><Card.Title>Cadastrar Comunidado</Card.Title></Card.Header>
      <Card.Body>
        <Form>
          <Form.Group>
            <Form.Label>Título do Comunicado</Form.Label>
            <Form.Control
              as='text'
              value={titulo}
              onChange={(event) => settitulo(event.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Corpo do Comunicado</Form.Label>
            <Form.Control
              as='textarea'
              value={corpo}
              onChange={(event) => setcorpo(event.target.value)}
            />
          </Form.Group>
          <Form.Row>
            <Form.Group>
              <Form.Label>Enviar arquivo(pdf)</Form.Label>
              <div>
                <label for="container-cadnotificação-input" id="style-upload-cadnotificação">
                  Upload do Arquivo
                <FaUpload size={25} style={{ marginLeft: 15, marginBottom: 10 }}></FaUpload>
                </label>
                <input id="container-cadnotificação-input" type="file" onChange={(event) => fileinput(event)}></input>
              </div>
            </Form.Group>
            <Button onClick={() => setdestin(true)}>Selecionar Destinatários</Button>
          </Form.Row>
        </Form>
      </Card.Body>
      <Card.Footer>
      <BtLoad
            onClick={cadasnotificacao}
            state={stateBt}
            title='Cadastrar'
            id="button-cadastro-notificação"
          />
      </Card.Footer>
    </Card>
    <Modal show={destin} onHide={() => setdestin(false)} centered size="xl">
      <Modal.Header>
        <Modal.Title >Selecionar destinatarios</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Destinatarios
          alunos={alunos}
          turmas={turmas}
          keys={(event) => setkeys(event)}
        />
      </Modal.Body>
    </Modal>
    <Modal show={show} style={{
        fontSize: 15, display: 'flex',
        justifyContent: 'center', alignItems: 'center'
      }}>
        <Modal.Title>
          <p id="container-cadnotificação-label">Upload de arquivo</p></Modal.Title>
        <Modal.Body>
          Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
      </Modal>
  </div>;
}

async function sendPushNotification(destin) {
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    mode: 'no-cors',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': true,
    },
    body: JSON.stringify({
      to: destin,
      sound: 'default',
      body: 'Novo comunicado!',
      data: { data: 'goes here' },
    }),
  });
}

function Destinatarios({ keys, alunos, turmas }) {
  const [selecionados, setselecionados] = useState([]);
  const [selectView, setselectView] = useState([]);
  const [filter, setfilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const filteredData = alunos.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });

  function select({ id, nome, state, type }) {
    if (state) {
      var select1 = selectView.slice();
      var select2 = selecionados.slice();
      select1.push({ type: type, nome: nome, id: id });
      select2.push(id);
      setselectView(select1);
      setselecionados(select2);
      keys(select2)
    } else {
      keys(selecionados.filter(Doc => Doc !== id))
      setselectView(selectView.filter(Doc => Doc.id !== id));
      setselecionados(selecionados.filter(Doc => Doc !== id));


    }
  }

  return <div>
    <div id='selectDist'>
      <Button onClick={() => console.log(selecionados, selectView)}>Teste</Button>
      <Tabs defaultActiveKey="Instituicao" transition={false}>
        {/* Aba de Instituição */}
        <Tab eventKey='Instituicao' title='Instituição'>
          <div className="">
            <p>O destinatário como instituição, mandará a
                          notificação para todos os responsáveis que tem acesso ao App</p>
          </div>
          <Button>Selecionar</Button>
        </Tab>
        {/* Aba de alunos */}
        <Tab eventKey='Alunos' title='Alunos'>
          <span className="filter-cadNot">
            <Form.Control
              id='form-cadnot'
              value={filter}
              onChange={event => setfilter(event.target.value)}
              placeholder={'Pesquisar por Nome ou Turma'}
            />
            <BsSearch id='icon' size='18px' />
          </span>
          <Table borderless striped responsive="sm">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Grau</th>
                <th>Turma</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody>
              {filteredData.map((aluno, _id) =>
                <Aluno
                  ano={aluno.turma.ano}
                  nome={aluno.nome}
                  nivel={aluno.nivel}
                  key={_id}
                  id={aluno.id}
                  select={select}
                  estado={aluno.state}
                />
              )}
            </tbody>
          </Table>
        </Tab>
        {/* Aba de Turmas */}
        <Tab eventKey='Turmas' title='Turmas'>
          <Table borderless striped responsive="sm">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Sala</th>
                <th>Turno</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody>
              {turmas.map((turma, _id) =>
                <Turma
                  ano={turma.ano}
                  turno={turma.turno}
                  sala={turma.sala}
                  key={_id}
                  id={turma.id}
                  select={select}
                  estado={turma.state}
                />
              )}
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey='Destinatarios' title='Destinatários selecionados'>
          <Table borderless striped responsive="sm">
            <thead>
              <tr>
                <th>Tipo</th>
                <th>Nome</th>
              </tr>
            </thead>
            <tbody>
              {selectView.map((item, _id) =>
                <tr key={_id}>
                  <td>{item.type}</td>
                  <td>{item.nome}</td>
                </tr>
              )}
            </tbody>
          </Table>
        </Tab>
      </Tabs>
    </div>
  </div>;
}

function Turma({ ano, turno, sala, key, id, estado, select }) {
  const [state, setstate] = useState(false);

  return (
    <tr
      style={{ cursor: 'pointer', fontWeight: (state | estado) ? 'bold' : 'normal' }}
      key={key}
      onClick={() => { select({ id: id, nome: ano, state: !state, type: 'Turma' }); setstate(!state); }}>
      <td>{ano}</td>
      <td>{sala}</td>
      <td>{turno}</td>
      <td>{state ? <p>Selecionado</p> : <p>Não Selecionado</p>}</td>
    </tr>

  )
}

function Aluno({ ano, nome, nivel, key, estado, id, select }) {
  const [state, setstate] = useState(false);

  return (
    <tr
      style={{ cursor: 'pointer', fontWeight: (state | estado) ? 'bold' : 'normal' }}
      key={key}
      onClick={() => { select({ id: id, nome: nome, state: !state, type: 'Aluno' }); setstate(!state); }}>
      <td>{nome}</td>
      <td>{nivel}</td>
      <td>{ano}</td>
      <td>{state ? <p>Selecionado</p> : <p>Não Selecionado</p>}</td>
    </tr>

  )
}
