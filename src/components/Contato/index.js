import React, {useState, useEffect} from "react";
import './style.css';
import {TiArrowLeft} from 'react-icons/ti';
import api from '../../services/api';
import { Form, Button, Card , Tooltip, OverlayTrigger} from "react-bootstrap";
import {useHistory} from "react-router-dom";
import BtLoad from "../ButtonLoading"

export default function Contato(props){
    const history = useHistory();
    const title = [
        "Selecionar um título" ,
        "Suporte Técnico",
        "Ajuda",
        "Dúvida de Funcionalidade",
        "Mal funcionamento",
        "Mudança de plano",
        "Outro"];
    const [titulo,settitulo] = useState('');
    const [nome,setnome] = useState('');
    const [telefone,settelefone] = useState('');
    const [corpo, setcorpo] = useState('');
    const [instituição, setinstituição] = useState('');
    const [stateBt, setstateBt] = useState(false);

    useEffect(()=>{
        try {
            settitulo(props.history.location.state.titulo)
            setinstituição(props.history.location.state.escola)
            setnome(props.history.location.state.nome)
            settelefone(props.history.location.state.telefone)

        } catch (error) { console.log('errro') }
    },[
        props.history.location.state.escola, 
        props.history.location.state.nome, 
        props.history.location.state.telefone,
        props.history.location.state.titulo])


    function EnviarMensagem(){
        setstateBt(true)
        api
        .post(
            "sendEmail",
            {
                emailFrom: 'lucas313lima@gmail.com',
                emailTo: String('testeslucas313@gmail.com'),
                titulo: String(titulo),
                corpo: String(`${corpo}\n\n${instituição}\n${nome} - ${telefone}`),
                senha: 'luc@slim@313'
            },
        )
        .then(res => {
            setstateBt(false)
        })
        .catch(e => {
            console.log(e);
        });
    }

    return(
        <div id='container-contato'>
            <Card id="scroll-conainer">
                <Card.Header id="ajust-backcor-contato">
                    <div id="container-header-contato">
                        <OverlayTrigger placement="top" overlay={<Tooltip>Retornar</Tooltip>}>
                            <Button id="BotaoResp" onClick={() => history.goBack()}>
                                <TiArrowLeft size="40" color={'gray'}></TiArrowLeft>
                            </Button>
                        </OverlayTrigger>
                        <p>Contato</p>
                    </div>
                </Card.Header>
                <Card.Body id="ajust-backcor-contato">
                    <p id="text-central-contato">Olá, seja bem vinda ao campo de Contato, caso tenha alguma dúvida do sistema, funcionalidade, procura por suporte ou outras informações, entre em contato conosco que em breve retornaremos!!. Preencha os campos abaixo e em seguida envie a mensagem.</p>
                </Card.Body>
                <Form id="ajust-backcor-contato">
                    <Form.Row id='form-row1-contato'>
                        <Form.Group htmlFor="title">
                            <Form.Label id="title-comunicado-contato">Título do Comunicado</Form.Label>
                            <Form.Control 
                                type="text"
                                as="select"
                                value={titulo}
                                onChange={e=>settitulo(e.target.value)}
                                >
                                {title.map(title =>(
                                    <option>{title}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group htmlFor="date">
                            <Form.Label id="title-comunicado-contato">Data de Envio</Form.Label>
                            <Form.Control 
                                type="date"
                                >
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label id="title-comunicado-contato">
                                Instituição
                            </Form.Label>
                            <Form.Control
                                value={instituição}
                                >
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row id="form-row2-contato">
                        <Form.Group id="form-grup-contato">
                            <Form.Label id="title-comunicado-contato">
                                Qual sua dúvida ou ajuda?
                            </Form.Label>
                            <textarea
                                id="textarea-contato"
                                rows="5"
                                value={corpo}
                                onChange={e=>setcorpo(e.target.value)}
                                >
                            </textarea>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row id="form-row3-contato">
                        
                        <BtLoad  onClick={EnviarMensagem} state={stateBt} title='Enviar mensagem' id='btni'/>
                    </Form.Row>
                </Form>
            </Card>
        </div>
    );
}