import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import { Button, Form, Tooltip, OverlayTrigger } from 'react-bootstrap'
import { FaChevronLeft } from 'react-icons/fa'
import { Col } from 'reactstrap';
import api from "../../services/api";
import './styles.css'

function FormSolicit() {
    const [name, setname] = useState('');
    const [nameInst, setnameInst] = useState('');
    const [email, setemail] = useState('');
    const [Body, setBody] = useState('');
    const [titulo, settitulo] = useState('');
    const [tel, settel] = useState('');
    const [Address, setAddress] = useState('');

    function sendEmail() {
        api
            .post(
                "sendEmail",
                {
                    emailFrom: 'lucas313lima@gmail.com',
                    emailTo: 'lucas313lima@gmail.com',
                    titulo: String(`Título: ${titulo}`),
                    corpo: String(`Corpo da mensagem: ${Body}\n\n Nome da instituição: ${nameInst} \n Endereço: ${Address}\n Nome: ${name} \n Telefone ${tel} \n Email: ${email}`),
                    senha: 'luc@slim@313'
                    
                },
            )
            .then(res => {
                alert("Mensagem enviada com sucesso, iremos analisar seu pedido e entraremos em contato");
                
                document.location.reload(true);
            })
            .catch(e => {
                alert('Ação não realizada');
                console.log(e);
            });
    }

    return (
        <div id='fundo-form' >

            <OverlayTrigger overlay={<Tooltip>Voltar</Tooltip>} placement="right">

            <Button id='button-back-form'>
                <Link to='/'>
                    <FaChevronLeft />
                </Link>
            </Button>

            </OverlayTrigger>
            <header id="header-form">
                <h4>Formulário de solicitação</h4>
                <p><strong>No formulário abaixo você preencherá com algumas informações para que o
                        administrador do sistema entre em contato com o senhor(a)</strong></p>
            </header>
                    
                        
                
            
            <Form id='form-sol'  >
                <Form.Row>
                    <Form.Group as={Col} >
                        <Form.Label>Seu nome</Form.Label>
                        <Form.Control
                            type="text"
                            required
                            value={name}
                            onChange={e => setname(e.target.value)} />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Email para contato</Form.Label>
                        <Form.Control
                            type="email"
                            required
                            value={email}
                            onChange={e => setemail(e.target.value)} />
                    </Form.Group>

                    <Form.Group as={Col} >
                        <Form.Label>Telefone para contato</Form.Label>
                        <Form.Control
                            type="number"
                            required
                            value={tel}
                            onChange={e => settel(e.target.value)} />
                    </Form.Group>
                </Form.Row>
                <Form.Group >
                    <Form.Label>Título</Form.Label>
                    <Form.Control
                        required
                        value={titulo}
                        onChange={e => settitulo(e.target.value)} />
                </Form.Group>

                <Form.Group >
                    <Form.Label htmlFor="name">Motivo da solicitação</Form.Label>
                    <Form.Control
                        required
                        as="textarea"
                        id="name"
                        rows="5"
                        value={Body}
                        onChange={e => setBody(e.target.value)}
                    />
                </Form.Group>
                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label>Nome da instituição</Form.Label>
                        <Form.Control
                            required
                            value={nameInst}
                            onChange={e => setnameInst(e.target.value)} />
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Endereço da instituição</Form.Label>
                        <Form.Control
                            required
                            value={Address}
                            onChange={e => setAddress(e.target.value)}
                            />
                    </Form.Group>
                </Form.Row>
                <Button onClick={() => sendEmail()}  id="button-form" > Enviar solicitação </Button>
            </Form>
        </div>
    );
}

export default FormSolicit;