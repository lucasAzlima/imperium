/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button,Table, Form ,OverlayTrigger,Tooltip} from 'react-bootstrap'
import './styles.css';
import { BsSearch } from 'react-icons/bs';
import {TiArrowLeft} from 'react-icons/ti';
import { useHistory } from 'react-router-dom';


export default function Teste(props) {
  const [materias, setmaterias] = useState([]);
  const EstrutEduc = Number(localStorage.getItem("EstrutCurric"));
  const [alunos,setalunos] = useState('');
  
  const [aluno,setaluno] = useState('');
  const [matrix, setMatrix] = useState(undefined);
  const [filter, setfilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const [turma,setturma] = useState('');
  const history = useHistory();
  const filteredData = materias.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });

  useEffect(() => {
  

    setmaterias(props.history.location.state.materias);
    putNotes(props.history.location.state.materias, props.history.location.state.id);
    setalunos(props.history.location.state.aluno);
    setturma(props.history.location.state.turma);
  }, []);
 
    

  function putNotes(materias, idAluno) {
    var matriz = Array.from({ length: materias.length }, () => Array.from({ length: EstrutEduc }, () => 0))
    materias.forEach((data, lin) => {
      data.alunos.forEach(aluno=>{
        if(aluno.id === idAluno){
          setaluno(aluno.nome)
          for (let col = 0; col < EstrutEduc; col++) {
            matriz[lin][col] = Number(aluno.notas[col])
          }}
          
      })
    })
    setMatrix(matriz)
  }

  function media(tabela) {
    var notaP = 0;
    for (let i = 0; i < EstrutEduc; i++) {
      notaP += tabela[i];
    }
    return notaP / EstrutEduc;
  }


  return (
    <div>
      <div id="header-notasAluno">
        <span className="barraDePesquisa">
              <Form.Control
                id="barraPesquisa"
                value={filter}
                onChange={event => setfilter(event.target.value)}
                placeholder={'Pesquisar por Nome ou Turma'}
              />
              <BsSearch type="button" id="iconPesquisa" />
        </span>
      </div>
      <div id='todasNotas'>
      <div id='title-notas-aluno'>
        <OverlayTrigger placement="top" overlay={<Tooltip>Retornar</Tooltip>}>
          <Button id="BotaoResp" onClick={()=>history.push({pathname:"/Alunosturma", state:{alunos:alunos,turma:turma}})}>
            <TiArrowLeft size="35" color={'gray'}></TiArrowLeft>
          </Button>
        </OverlayTrigger>
        <p>{aluno}</p>
        </div>
        <Table borderless striped responsive="sm">
          <thead>
            <tr >
              <th id="nome">Nome</th>
              {Array(EstrutEduc).fill(0).map((d, indx) => <th>{`Nota ${indx + 1}`}</th>)}
              <th>Média</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map((data, lin) => {
              return (
                <tr>
                  <td><p>{data.nome}</p></td>
              {Array(EstrutEduc).fill(0).map((index, col) => <td><p>{matrix[lin][col]}</p></td>)}
              <td><p>{media(matrix[lin])}</p></td>
                </tr>
              )
            })}
          </tbody>
        </Table>
        </div>
    </div>
  );
}
