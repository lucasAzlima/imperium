/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Form, Col, Modal, Button, Row, OverlayTrigger, Tooltip, Container, Image } from "react-bootstrap";
import { FiArrowLeft } from 'react-icons/fi'
import { Link} from 'react-router-dom';
import "./styles.css";
import api from "../../services/api"
import firebase from "../../services/firebase";
import Responsavel from "./Card"
import { MdCancel } from "react-icons/md";
import BtLoad from "../ButtonLoading"


export default function CadastroAluno(props) {
  const [DadosResp, setDadosResp] = useState([]);
  const [nameStudent, setNameStudent] = useState("");
  const [cpfStudent, setCpfStudent] = useState("");
  const [gender, setGender] = useState(0);
  const [matricula, setmatricula] = useState("");
  const [dateStudent, setDateStudent] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");
  const [show, setshow] = useState(false);
  const [Porcentagem, setPorcentagem] = useState(0);
  const sexo = ["Selecionar sexo", "Masculino", "Feminino"];
  const nivel = ["Primário", "Ensino fundamental", "Ensino médio"];
  const [nivelAluno, setnivelAluno] = useState("Primário");
  const [filename, setfilename] = useState("");
  const [stateBt, setstateBt] = useState(false);

  useEffect(() => {
    setNameStudent(props.data.nome);
    setmatricula(props.data.matricula);
    setDateStudent(props.data.nascimento)
    setGender(props.data.sexo);
    setCpfStudent(props.data.cpf);
    setDadosResp(props.data.responsaveis);
    setnivelAluno(props.data.nivel)
    firebase.storage().ref().child(props.data.foto).getDownloadURL().then(function (url) {
      setImage(url);
    });
  }, []);




  function editarAluno() {
    setstateBt(true)
    const token = localStorage.getItem("token");

    api
      .post(
        "editarAluno",
        {
          matricula: matricula,
          nascimento: dateStudent,
          nome: nameStudent,
          responsaveis: DadosResp,
          sexo: gender,
          cpfAluno: cpfStudent,
          id: props.data.id
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        if (picture !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(picture, { contentType: 'image/jpeg' })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) setTimeout(() => { setshow(false); }, 1000)
            });
        }
        setstateBt(false)

      })
      .catch(e => {
        console.log(e);
      });
  }

  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
        cropImage(pictureConverter.result, 1 / 1));
         
      }
      catch (e) {
        console.log("Formato não suportado");
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {
        
        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };
  
  function RespSelect(event) {
    var data = DadosResp.slice();
    var disc = data.filter(doc => doc.cpf !== event.cpf);
    disc.push(event);
    setDadosResp(disc)

  }

  function delListResp(event) {
    var Disciplina = DadosResp.filter(Disc => Disc.cpf !== event.cpf);
    setDadosResp(Disciplina);
  }

  return (
    <>
      <div id='box-EdAl'>
      <hr />
        <Container fluid>
            <Row>
              <Col sm={2}>
                <Link id="bt-return" to="/Alunos">
                  <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                  Retornar
                </Link>
              </Col>
              <Col sm={8}><h5>Dados do Aluno</h5></Col>
            </Row>
          </Container>
        <hr />
        <div id='input-wrapper-EdAl2'>
          <Image  src={ImageShow} rounded/>
        </div>
        <div id='input-wrapper-EdAl'>
          <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
            Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
            <div className="input-wrapper-EdAl">
              <label for='input-file-EdAl'>Selecionar um arquivo</label>
              <input type="file" onChange={(event) => setPic(event)} id='input-file-EdAl' />
              <span id='file-name'>{filename}</span>
            </div>
          </OverlayTrigger>
          {(picture !== undefined) ?
            <MdCancel onClick={() => {
              setImage('')
              setfilename('')
              setPicture(undefined)
            }} id='icon-cancel-ca' /> : <div />}
        </div>
        <Form id='form-EdAl'>
          <Form.Row>
            <Col>
              <Form.Label htmlFor="name">Nome completo do aluno</Form.Label>
              <Form.Control
                type="text"
                id="name"
                value={nameStudent}
                onChange={event => setNameStudent(event.target.value)}
              />
            </Col>
            <Col>
              <Form.Label htmlFor="number">
                CPF do aluno<span>(somente números)</span>
              </Form.Label>
              <Form.Control
                type="number"
                id="name"
                value={cpfStudent}
                onChange={event => setCpfStudent(event.target.value)}
              />
            </Col>
          </Form.Row>

          <Form.Row>
            <Col>
              <Form.Label htmlFor="date">
                Data de nascimento do aluno
              </Form.Label>
              <Form.Control
                type="date"
                id="date"
                value={dateStudent}
                onChange={event => setDateStudent(event.target.value)}
              />
            </Col>

            <Col>
              <Form.Label htmlFor="number">
                Matrícula do aluno
              </Form.Label>
              <Form.Control
                type="number"
                id="name"
                value={matricula}
                onChange={event => setmatricula(event.target.value)}
              />
            </Col>

            <Col>
              <Form.Label htmlFor="name">Nível</Form.Label>
              <Form.Control as="select"
                value={nivelAluno}
                onChange={event => setnivelAluno(event.target.value)}>
                {nivel.map(nvl => (
                  <option>{nvl}</option>))}
              </Form.Control>
            </Col>

            <Col>
              <Form.Label htmlFor="name">Sexo</Form.Label>
              <Form.Control as="select"
                value={gender}
                onChange={event => setGender(event.target.value)}>
                {sexo.map(sexo => (
                  <option>{sexo}</option>))}
              </Form.Control>
            </Col>
          </Form.Row>

          
          <br/>
          <br/>
          <hr />
          <h5>Dados dos Responsáveis</h5>
          <hr />
          <Form.Group>
            <Form.Label style={{ marginRight: 15, marginTop: 15 }} htmlFor="number">Responsáveis</Form.Label>
            <Button onClick={() => setDadosResp(DadosResp.concat({ nome: '', parent: '', tel: '', idade: '', cpf: '' }))}>Adicionar Responsável</Button>
          </Form.Group>
          <Form.Row style={{ height: '90%' }}>
            {DadosResp.map(dados => {
              return (
                <>
                  <Responsavel
                    data={dados}
                    DadosResponsaveis={event => console.log(event)}
                    RespSelect={RespSelect}
                    delListResp={delListResp} />
                </>
              )
            })}
          </Form.Row>

          
          <BtLoad  onClick={editarAluno} state={stateBt} title='Atualizar aluno' />
        </Form>
        <Modal show={show} style={{
          fontSize: 15, display: 'flex',
          justifyContent: 'center', alignItems: 'center'
        }}>
          <Modal.Title><p >Upload de Imagem</p></Modal.Title>
          <Modal.Body>Upload está {Porcentagem.toFixed(2)}% concluido</Modal.Body>
        </Modal>
      </div>
    </>
  );
}
