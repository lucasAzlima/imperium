import React from "react";
import { Button, Col, Form, Card, Row } from "react-bootstrap";

import "./styles.css";

export default class ListAluno extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: '',
      parent: '',
      tel: '',
      idade: '',
      cpf: '',
      color: 'light'
    }
  }
  componentDidMount(){
    var data = this.props.data;
    this.setState({nome:data.nome, parent:data.parent, tel:data.tel, idade:data.idade, cpf:data.cpf});
  }

  render() {
    var dados = {
      nome: this.state.nome, parent: this.state.parent,
      tel: this.state.tel, idade: this.state.idade, cpf: this.state.cpf};
      
    return (
      <>
        <Card bg={this.state.color} style={{ marginLeft: 5, marginBottom: 5, padding: 5 }}>
          <Form.Row >
            <Form.Group as={Col} controlId="idade">
              <Form.Label>Nome</Form.Label>
              <Form.Control value={this.state.nome} 
              onChange={e => this.setState({ nome: e.target.value })} />
            </Form.Group>

            <Form.Group as={Col} controlId="parent">
              <Form.Label>Parentesco</Form.Label>
              <Form.Control value={this.state.parent}
              onChange={e => this.setState({ parent: e.target.value })} />
            </Form.Group>
          </Form.Row>

          <Form.Row >
            <Form.Group as={Col} controlId="idade">
              <Form.Label>Idade</Form.Label>
              <Form.Control type="number" value={this.state.idade}
              onChange={e => this.setState({ idade: e.target.value })} />
            </Form.Group>

            <Form.Group as={Col} controlId="telefone">
              <Form.Label>Telefone</Form.Label>
              <Form.Control type="tel" value={this.state.tel}
              onChange={e => this.setState({ tel: e.target.value })} />
            </Form.Group>
          </Form.Row>
          
          <Form.Label>Cpf do Resposável</Form.Label>
          <Form.Control type="tel" value={this.state.cpf}
          onChange={e => this.setState({ cpf: e.target.value })} />

          <Row style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button style={{ margin: 5 }} variant='primary'
              onClickCapture={() => this.setState({ color: 'success' })}
              onClick={() => this.props.RespSelect(dados)}>
              Atualizar</Button>

            <Button style={{ margin: 5 }} variant='primary'
              onClick={() => this.props.delListResp(dados)}
              onClickCapture={() => this.setState({ color: 'light' })}
            >
              Remover</Button>
          </Row>
          
        </Card>
      </>
    );
  }
}