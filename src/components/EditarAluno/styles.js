
import { Button, Form } from 'react-bootstrap';

import styled from 'styled-components';

export const StyledButton = styled(Button)`
    border: 0;
    border-radius: 2px;
    padding: 5px 20px;
    font-size: 16px;
    font-weight: bold;
    background-color: #fff;
    color: #fff;
    width: 100%;

    cursor: pointer;

`;

export const StyledFormRow = styled(Form.Row)`
    padding: 0px 7px;
    display:flex;
    justify-content: space-around;
    
`;