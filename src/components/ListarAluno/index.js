/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { Button, Form, Table, OverlayTrigger, Tooltip, Modal, ProgressBar } from 'react-bootstrap'
import ReactPaginate from 'react-paginate';
import { BsSearch } from 'react-icons/bs';
import { FaRegUserCircle, FaPencilAlt } from "react-icons/fa";
import './styles.css'
export default function Disciplinas() {
  const [datavalue, setdatavalue] = useState(['']);
  const [turmas, setTurmas] = useState([]);
  const [filter, setfilter] = useState('');
  const [cadastrar, setcadastrar] = useState(true);
  const lowercasedFilter = filter.toLowerCase();
  const [postData, setpostData] = useState('');
  const [offset, setoffset] = useState(0);
  const perPage = 5;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  

  const [isLoading, setisLoading] = useState(true);

  const history = useHistory();
  const filteredData = datavalue.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });

  useEffect(() => {
    GetAluno();
    getTurmas();
  }, []);

  useEffect(() => { receivedData(offset, filter); }, [offset, filter, datavalue])

  function GetAluno() {
    const token = localStorage.getItem("token");
    api
      .get("/getAlunos", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        if(res.data.length !== 0) {
          if(Number(res.data.maxAluno) - Number(res.data.alunos.length) <=0 ) setcadastrar(false)
          setdatavalue(res.data.alunos)
        }
        else {
          setdatavalue([{
            nome: "Nenhum dado cadastrado",
            nivel: "-",
            turma: {
              ano: "-",
            },
            

          }])
        }
        setisLoading(false);
      })
      .catch(e => {
        console.log(e);
        console.log("Erro de autenticação, verifique seu email e senha");
      });

      
  }

  function getTurmas() {
    const token = localStorage.getItem("token");

    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setTurmas(turmas.concat(res.data));
      })
      .catch(e => {
        console.log(e);
      });
  }


  function receivedData(offset, filter) {

    
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(pd =>
    
      
      <tr>
        <td id='bordaArreEsq'>{pd.nome}</td>
        <td>{pd.nivel}</td>
        <td>{pd.turma.ano}</td>
        <td id='bordaArreDir'>
          <OverlayTrigger placement="left"  overlay={<Tooltip>Visualizar</Tooltip>}>
            <Button id="BotaoResp" disabled={(pd.nivel ==='-')} onClick={() => history.push({ pathname: "/VerAluno", state: { data: pd } })}>
              <FaRegUserCircle size="20px" />
            </Button>
          </OverlayTrigger>

          <OverlayTrigger placement="right" overlay={<Tooltip>Editar</Tooltip>}>
            <Button id="BotaoResp" disabled={(pd.nivel ==='-')} onClick={() => history.push({ pathname: "/editarAluno", state: { data: pd } })}>
              <FaPencilAlt size="20px" />
            </Button>
          </OverlayTrigger>
        </td>

      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);
  }

  useEffect(()=>{
    if(filter !== ''){
      setoffset(0)
      setforceNew(0)
      
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  },[filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if(filter === ''){
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };



  return (
    <>
      

      
      
      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Nome ou Turma'}
          />
          
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>


        <Button id="BotaoCadResp" disabled={!cadastrar} onClick={() => history.push('/cadastroDeAlunos')}>Cadastrar Aluno</Button>
      </div>

      <div id='listagem'>
        <Table id="tabelaPadrao" borderless striped responsive="sm">
          <thead>
            <tr id="title-aluno-container">
              <th>Nome</th>
              <th>Grau</th>
              <th>Turma</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            {postData}
          </tbody>
        </Table>

        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"} 
          forcePage={forceNew}/>

      </div>

      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>

    </>
  );
}