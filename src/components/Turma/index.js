/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import {
    Button, Modal, Form,
    OverlayTrigger, Tooltip, InputGroup,
    FormControl, Alert, ProgressBar
} from 'react-bootstrap'
import './styles.css';
import api from "../../services/api";
import { FaPencilAlt, FaTrash } from "react-icons/fa";
import { FaAngleLeft } from 'react-icons/fa';
import { useHistory, Link } from 'react-router-dom'
import { Styledh4 } from './style.js';
import BtLoad from "../ButtonLoading"

function Turma(props) {
    const [turma, setturma] = useState({ alunos: [], materias: [] })
    const [proflist, setprofList] = useState([])
    const [show, setshow] = useState(false);
    const [show2, setshow2] = useState(false);
    const [show3, setshow3] = useState(false);
    const [show4, setshow4] = useState(false);
    const [disciplinas, setdisci] = useState([{ nome: 'Selecionar disciplina' }]);
    const [professores, setprofessores] = useState([{ nome: 'Selecionar professor' }]);
    const [profselec, setprofselec] = useState();
    const [discSelec, setdiscSelec] = useState([]);
    const [profselecEdit, setprofselecEdit] = useState('');
    const [idEdit, setidEdit] = useState('');
    const [delNomeuser, setdelNomeuser] = useState('');
    const [delNome, setdelNome] = useState('');
    const history = useHistory();
    const [nome, setnome] = useState("");
    const [sala, setsala] = useState("");
    const [nivel, setnivel] = useState("");
    const [turno, setturno] = useState("");
    const Turnos = ["Manhã", "Tarde", "Noite"];
    const niveis = ["Primário", "Ensino fundamental", "Ensino médio"];
    const [stateBt, setstateBt] = useState(false);
    const [isLoading, setisLoading] = useState(true);

    useEffect(() => {
        GetTurma()
        GetDisc();
        Getprof();
    }, [])

    function GetTurma() {
        const token = localStorage.getItem("token");
        api
            .put("/getTurma",
                {
                    id: localStorage.getItem('turma')
                },

                {
                    headers: { Authorization: `Bearer ${token}` }
                })
            .then(res => {
                setturma(res.data);
                profInClass(res.data);
                setisLoading(false);
            })
            .catch(e => {
                console.log(e);
                console.log(
                    "Erro de autenticação, verifique seu email e senha"
                );
            });
    }

    function GetDisc() {
        const token = localStorage.getItem("token");
        api
            .get("/getDisciplina", {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                setdisci(disciplinas.concat(res.data));
            })
            .catch(e => {
                console.log(e);
                console.log(
                    "Erro de autenticação, verifique seu email e senha"
                );
            });
    }

    function Getprof() {
        const token = localStorage.getItem("token");
        api
            .get("/getProfessor", {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                setprofessores(professores.concat(res.data));

            })
            .catch(e => {
                console.log(e);
                console.log(
                    "Erro de autenticação, verifique seu email e senha"
                );
            });
    }

    function profInClass(trm) {
        var prf = [];
        trm.materias.forEach(disc => {
            if (prf.length !== 0) {
                if (!prf.includes(disc.professor)) prf.push(disc.professor);
            } else {
                prf.push(disc.professor)
            }
        })
        console.log(prf)
        setprofList(prf)
    }

    function addDisc() {
        setstateBt(true)
        var professor = professores.filter(Doc => Doc.nome === profselec)[0]
        var disciplina = disciplinas.filter(Doc => Doc.nome === discSelec)[0]
        let trm = turma;
        var alns = []
        trm.alunos.forEach(aluno => {
            alns.push({ nome: aluno.nome, id: aluno.id, notas: Array(Number(trm.EstrutEstudo)).fill(0) })
        })
        trm.materias.push({
            alunos: alns,
            nome: disciplina.nome,
            id: disciplina.id,
            professor: professor.nome,
            idProf: professor.id
        })

        var notasAlunos = [];
        trm.alunos.forEach((aluno, index) => {
            notasAlunos.push({ id: aluno.id, notas: [] });
            var nt = [];
            trm.materias.forEach(materia => {
                materia.alunos.forEach(aln => {
                    if (aln.id === aluno.id) {
                        nt.push({
                            materia: materia.nome,
                            notas: aln.notas,
                            professor: materia.professor
                        }); return
                    }
                })
            })
            Object.assign(notasAlunos[index].notas, nt);
        })
        const token = localStorage.getItem("token");
        api
            .put(
                "addNotas",
                {
                    materias: trm.materias,
                    id: trm.id,
                    notas: notasAlunos
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                setstateBt(false)
                setturma(trm)
                setshow(false)
                GetTurma()

            })
            .catch(e => {
                console.log(e);
            });
    }

    function upDisc() {
        var professor = professores.filter(Doc => Doc.nome === profselecEdit)[0]
        let trm = turma;
        var materias = trm.materias.filter(Doc => Doc.id !== idEdit);
        var materiasEd = trm.materias.filter(Doc => Doc.id === idEdit)[0];
        materiasEd.professor = professor.nome;
        materiasEd.idProf = professor.id;
        materias.push(materiasEd);
        trm.materias = materias;
        const token = localStorage.getItem("token");
        api
            .put(
                "addDisciplina",
                {
                    materias: trm.materias,
                    id: trm.id,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                setturma(trm)
                profInClass(trm)
                setshow2(false)
                GetTurma()
            })
            .catch(e => {
                console.log(e);
            });
    }

    function delDic() {

        var mat = turma.materias.filter(Doc => Doc.id !== idEdit);
        var notasAl = [];
        turma.alunos.forEach((aluno, index) => {
            notasAl.push({ id: aluno.id, notas: [] });
            var nt = [];
            mat.forEach(materia => {
                materia.alunos.forEach(aln => {
                    if (aln.id === aluno.id) { nt.push({ materia: materia.nome, notas: aln.notas }); return }
                })
            })
            Object.assign(notasAl[index].notas, nt);
        })
        updatemat(mat, notasAl)
    }

    function updatemat(materias, notasAlunos) {
        const token = localStorage.getItem("token");
        api
            .put(
                "addNotas",
                {
                    materias: materias,
                    id: turma.id,
                    notas: notasAlunos,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                setshow3(false)
                GetTurma()
            })
            .catch(e => {
                console.log(e);
            });
    }

    function editarTurma() {
        const token = localStorage.getItem("token");

        api
            .post(
                "editarTurma",
                {
                    ano: nome,
                    turno: turno,
                    sala: sala,
                    id: turma.id,
                    nivel: nivel
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div id='prfl-turma-box'>
            <div id='prfl-turma-header'>
                <div id='titulo-turma'>
                    <Link id="link-return-listurma" to="/turmas">
                        <FaAngleLeft size={23}></FaAngleLeft>
                    </Link>
                    <p id='nome'>{`${turma.ano}`}</p>
                    <p>{`· ${turma.nivel}`}</p>
                    <p>{`· Sala ${turma.sala}`}</p>
                    <p>{`· Turno da ${turma.turno}`}</p>
                </div>
                <Button id="button-edit-turma"
                    onClick={() => {
                        setnome(turma.ano)
                        setnivel(turma.nivel)
                        setsala(turma.sala)
                        setturno(turma.turno)
                        setshow4(true)
                    }}>Editar</Button>
            </div>

            <div id='prfl-turma'>
                <div id='prfl-disciplinas'>
                    <div id='header-disciplina'>
                        <h6>Disciplinas</h6>
                        <Button onClick={() => setshow(true)}>Adicionar</Button>
                    </div>
                    <div id='scroll'>
                        {turma.materias.map(disc =>
                            <table>
                                <tr>
                                    <td>
                                        <p id="disciplina-turma-p">{disc.nome}</p>
                                        <p>{disc.professor}</p>
                                    </td>
                                    <td><Button id='button-gerenciar-notas'
                                        onClick={() => history.push({ pathname: '/NotasDisciplina', state: { disciplina: disc, turma } })}>Gerenciar Notas</Button></td>
                                    <td>
                                        <OverlayTrigger placement="top" overlay={<Tooltip>Editar</Tooltip>}>
                                            <Button id="BotaoResp">
                                                <FaPencilAlt size='21' id='icon'
                                                    onClick={() => {
                                                        setshow2(true);
                                                        setprofselecEdit(disc.professor);
                                                        setidEdit(disc.id)
                                                    }} />
                                            </Button>
                                        </OverlayTrigger >
                                        <OverlayTrigger placement="top" overlay={<Tooltip>Excluir</Tooltip>}>
                                            <Button id="BotaoResp">
                                                <FaTrash size='21' id='icon'
                                                    onClick={() => {
                                                        setshow3(true);
                                                        setidEdit(disc.id);
                                                        setdelNome(disc.nome)
                                                    }} />
                                            </Button>
                                        </OverlayTrigger>
                                    </td>
                                </tr>
                            </table>

                        )}
                    </div>
                </div>
                <div id='prfl-aluno'>
                    <div id='header-disciplina'>
                        <h6>Alunos</h6>
                        <Button
                            onClick={() => history.push({ pathname: '/Alunosturma', state: { alunos: turma.alunos, turma } })}>Ver Todos</Button>
                    </div>
                    <span>
                        <p id='numeroAl'>{turma.alunos.length}</p>
                        <p>Matriculados</p>
                    </span>
                </div>
                <div id='prfl-professor'>
                    <h6>Professores</h6>
                    <div id='scroll-prof'>
                        {proflist.map(disc =>
                            <div id='item-list'>
                                <p>{disc}</p>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <Modal show={show} onHide={() => setshow(false)} centered >
                <Modal.Header closeButton id="modalHeader">
                    <Modal.Title >Adicionar disciplina</Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">

                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Disciplina</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control as="select"
                            value={discSelec}
                            onChange={event => { setdiscSelec(event.target.value); }}>
                            {disciplinas.map(disc => (
                                <option>{disc.nome}</option>))}
                        </Form.Control>


                    </InputGroup>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Professor</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control as="select"
                            value={profselec}
                            onChange={event => { setprofselec(event.target.value); }}>
                            {professores.map(prof => (
                                <option>{prof.nome}</option>))}
                        </Form.Control>
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <BtLoad onClick={() => addDisc()} state={stateBt} title='Adicionar' id="BotaoEditS" />
                </Modal.Footer>
            </Modal>

            <Modal show={show2} onHide={() => setshow2(false)} centered size="lg">
                <Modal.Header id="modalHeader">
                    <Modal.Title >Alterar professor</Modal.Title>
                </Modal.Header>

                <Modal.Body id="modalBody">
                    <Form.Label htmlFor="number" id="form-label-turma">Professor </Form.Label>
                    <Form.Control as="select"
                        value={profselecEdit}
                        onChange={event => { setprofselecEdit(event.target.value); }}>
                        {professores.map(prof => (
                            <option>{prof.nome}</option>))}
                    </Form.Control>
                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <Styledh4>Deseja salvar a alteração?</Styledh4>
                    <Button id="BotaoEditS" onClick={() => upDisc()}>Sim</Button>
                    <Button id="BotaoEditN" onClick={() => setshow2(false)} >Não</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={show3} onHide={() => setshow3(false)} >
                <Modal.Header id="modalHeader">
                    <Modal.Title>Deseja deletar a disciplina?</Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">
                    <p>Se essa disciplina for deletada, todo o histórico e notas dos alunos
                         cadastrados nela serão excluídos de forma permanente e não poderão ser recuperados. </p>
                    <Alert variant='danger'>
                        <Form.Label htmlFor="text">Para deletar a disciplina, por favor digite
                        <strong>{delNome}</strong> para confirmar</Form.Label>
                    </Alert>
                    <Form.Control
                        value={delNomeuser}
                        onChange={event => { setdelNomeuser(event.target.value); }} />
                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <Button id="BotaoEditN" onClick={() => {
                        if (delNome === delNomeuser) delDic()
                        else {
                            setshow3(false)
                            setTimeout(() => { setshow3(true) }, 1000);
                        }
                    }}>Deletar</Button>
                    <Button id="BotaoEditS" onClick={() => setshow3(false)}>Cancelar</Button>
                </Modal.Footer>
            </Modal>


            <Modal show={show4} onHide={() => setshow4(false)} centered size="lg">
                <Modal.Header id="modalHeader">
                    <Modal.Title>Editar turma</Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Nome</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={nome}
                            onChange={event => setnome(event.target.value)}
                        />
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor" >Nível</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control as="select"
                            value={nivel}
                            onChange={event => { setnivel(event.target.value); }}>
                            {niveis.map(nivel => <option>{nivel}</option>)}
                        </Form.Control>
                    </InputGroup>

                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor" >Turno</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control as="select"
                            value={turno}
                            onChange={event => { setturno(event.target.value); }}>
                            {Turnos.map(tuno => <option>{tuno}</option>)}
                        </Form.Control>

                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor" >Sala</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={sala}
                            onChange={event => setsala(event.target.value)} />
                    </InputGroup>

                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <Styledh4>Deseja salvar a alteração?</Styledh4>
                    <Button id="BotaoEditS" onClick={() => editarTurma()}>Sim</Button>
                    <Button id="BotaoEditN" onClick={() => setshow4(false)}>Não</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={isLoading} size='lg' centered animation>
                <Modal.Body>
                    <ProgressBar animated now={100} />
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default Turma;