/* eslint-disable array-callback-return */
/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { Link } from "react-router-dom";
import firebase from "../../services/firebase";
import {  Button, Form, Modal, OverlayTrigger, Tooltip, Table, ProgressBar } from 'react-bootstrap'
import ReactPaginate from 'react-paginate';
import './styles.css'
import { FaRegUserCircle, FaPencilAlt, FaTrash } from 'react-icons/fa';
import { BsSearch } from 'react-icons/bs';
export default function Cameras() {
  const [notificacoes, setnotificacoes] = useState([]);
  const [instituicao, setinstituicao] = useState("");
  const [filter, setfilter] = useState('');
  const [turmas, setturmas] = useState([]);
  const [corpo, setcorpo] = useState('');
  const [title, settitle] = useState('');
  const [file, setfile] = useState('');
  const [postData, setpostData] = useState([]);
  const [offset, setoffset] = useState(0);
  const [confirmNot, setconfirmNot] = useState(0);
  const [editor, seteditor] = useState(false);
  const [editor1, seteditor1] = useState(false);
  const perPage = 6;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const [isLoading, setisLoading] = useState(true);
  

  useEffect(() => {
    Getnot();
    getTurmas();
  }, []);

  useEffect(() => { receivedData(offset, filter); }, [offset, filter, notificacoes])


  function getTurmas() {
    const token = localStorage.getItem("token");

    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = [];
        res.data.map(data => trm.push({ nome: data.ano, id: data.id, state: false, alunos: data.alunos }));
        setturmas(trm);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function Getnot() {
    const token = localStorage.getItem("token");
    api
      .get("/getNotificacoes", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {

        if (res.data.length !== 0) {
          setnotificacoes(res.data.data);
          setinstituicao(res.data.id);
        }
        else {
          setnotificacoes([{
              titulo: "Nenhum dado cadastrado",
              corpo: "-"
              }])
          
        }
        setisLoading(false);

        

      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function deleteNotify(event) {
    const token = localStorage.getItem("token");
    api.put("/deleteNotificacoes",
      {
        id: event
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        firebase.storage().ref().child(res.data.path).delete();
        Getnot();
        event.history.push("/comunicados");
      })
      .catch(e => {
        console.log(e);
      });

  }


  const lowercasedFilter = filter.toLowerCase();
  const filteredData = notificacoes.filter(item => {
    return Object.keys(item).some(key => {
      if (key !== 'key')
        return item[key].toLowerCase().includes(lowercasedFilter)
    }
    );
  });

  function receivedData(offset, filter) {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(pd =>
      <tr>
        <td id='bordaArreEsq'>{pd.titulo}</td>
        <td id='card-text-limite-notificação'>{pd.corpo}...</td>
        <td id='bordaArreDir'>
          <OverlayTrigger placement="left" overlay={<Tooltip>Visualizar</Tooltip>}>
            <Button id="BotaoResp" disabled={(pd.corpo === '-')} onClick={() => { seteditor(true); setcorpo(pd.corpo); setfile(pd.filename); settitle(pd.titulo) }}>
              <FaRegUserCircle size="20px" />
            </Button>
          </OverlayTrigger>
          <OverlayTrigger placement="top" overlay={<Tooltip>Editar</Tooltip>}>
            <Link to={{ pathname: "/editarNotificacao", state: { data: pd}}} >
              <Button disabled={(pd.corpo === '-')} id="BotaoResp">
                <FaPencilAlt size="20px" />
              </Button>
            </Link>
          </OverlayTrigger>
          <OverlayTrigger placement="right" overlay={<Tooltip>Excluir</Tooltip>}>
            <Button id="BotaoResp2" disabled={(pd.corpo === '-')} onClick={e => { seteditor1(true); setconfirmNot(pd.id) }}>
              <FaTrash size="20px" />
            </Button>
          </OverlayTrigger>

        </td>
      </tr>
    );


    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);

  }

  useEffect(() => {
    if (filter !== '') {
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  }, [filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if (filter === '') {
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };



  return (
    <>

      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Notificação'}

          />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>

        <Link to={{ pathname: "/cadnotify", state: { id: instituicao, turmas: turmas } }} >
          <Button id="BotaoCadResp" >Cadastrar Notificação</Button>
        </Link>

      </div>

      <div id="listagem">
        <Table id="tabelaPadrao" borderless striped responsive="sm">
          <thead>
            <tr>
              <th>Título</th>
              <th>Corpo da Mensagem</th>
              <th id="ajust-lisnotificação-ações">Ações</th>

            </tr>
          </thead>
          <tbody >
            {postData}
          </tbody>
        </Table>
        <ReactPaginate

          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          forcePage={forceNew} />
      </div>
      <Modal className="container-modal-notificação"  show={editor} onHide={() => seteditor(false)}>
        <Modal.Header id="modalHeader" close>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body id="modalBody">
        <strong>Título:</strong> {corpo}
        <br/>
        <strong>Nome do Arquivo: </strong> {file}</Modal.Body>
        
      </Modal>

      <Modal show={editor1} onHide={() => seteditor(false)}>
        <Modal.Header id="modal-header-notificação">
          <Modal.Title>Ação do Administrador</Modal.Title>
        </Modal.Header>
        <Modal.Body id="modal-body-notificação">Tem certeza que deseja excluir essa notificação ?</Modal.Body>
        <Modal.Footer id="modal-footer-notificação">
          <Button id="button-ConfirmDeletenotificação-modal"  onClick={() => { seteditor1(false); deleteNotify(confirmNot) }}>Sim</Button>
          <Button id="button-NoConfirmDeletenotificação-modal"  onClick={() => seteditor1(false)}>Não</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>

    </>
  );
}
