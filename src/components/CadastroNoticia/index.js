import React, { useState, useEffect } from "react";
import { Modal, Form, OverlayTrigger, Tooltip} from "react-bootstrap"
import "./styles.css";
import api from "../../services/api";
import firebase from "../../services/firebase";
import BtLoad from "../ButtonLoading"
import { MdCancel } from "react-icons/md";


export default function CadastroNoticia({ history }) {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");
  const [Porcentagem, setPorcentagem] = useState(0);
  const [Link, setLink] = useState("");
  const [filename, setfilename] = useState("");
  const [stateBt, setstateBt] = useState(false);
  const [show, setshow] = useState(false);
  var curr = new Date();
  curr.setDate(curr.getDate());
  var date = curr.toISOString().substr(0, 10);
  const [datePublication, setDatePublication] = useState(date);
  const [usersT, setusersT] = useState([]);


  useEffect(() => {
    GetListaUsers();
  },[])

  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
        cropImage(pictureConverter.result, 1 / 1));
         
      }
      catch (e) {
        console.log("Formato não suportado");
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {
        
        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };

  async function cadastrarNoticia(event) {
    setstateBt(true)

    event.preventDefault();
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarNoticia",
        {
          titulo: title,
          corpo: body,
          imagem: picture,
          link: Link
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {

        usersT.forEach(element => {
          element.expoTokens.forEach(token => sendPushNotification(token))
        });


        if(picture!==undefined){
        setshow(true);
        firebase.storage().ref().child(res.data.path).put(picture, {contentType: 'image/jpeg'})
          .on('state_changed', function (snapshot) {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setPorcentagem(progress);
            if(progress===100)setTimeout(()=>{setshow(false);},1000)
          });}
    setstateBt(false)

          history.push('/Noticias')
      })
      .catch(e => {
        console.log(e);
      });
  }

  function GetListaUsers() {
    const token = localStorage.getItem("token");
    api
      .get("/getListUsers", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setusersT(res.data)
      })
      .catch(e => {
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  return (
    <div id='box-cadNot'>
      <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0}} alt="" src={ImageShow} />
        <div id='input-wrapper-cadNot'>
          <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
            Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
            <div className="input-wrapper-cadNot">
              <label for='input-file-cadNot'>Selecionar um arquivo</label>
              <input type="file" onChange={(event) => setPic(event)} id='input-file-cadNot' />
              <span id='file-name'>{filename}</span>
            </div>
            </OverlayTrigger>
            {(picture !== undefined) ?
            <MdCancel onClick={() => {
              setImage('')
              setfilename('')
              setPicture(undefined)
              document.getElementById("input-file-cadNot").value = ''
            }} id='icon-cancel-ca' /> : <div />}
        </div>
      <Form id='form-cadNot'>
        <Form.Label htmlFor="name">TÍTULO *</Form.Label>
        <Form.Control
          type="text"
          id="title"
          value={title}
          onChange={event => setTitle(event.target.value)}
        />
        <Form.Label htmlFor="name">CORPO * </Form.Label>
        <Form.Control
          as="textarea"
          id="name"
          value={body}
          onChange={event => setBody(event.target.value)}
          rows="5"
        />
        <br />
        <Form.Label htmlFor="name">LINK *</Form.Label>
        <Form.Control
          type="text"
          id="link"
          value={Link}
          onChange={event => setLink(event.target.value)}
        />
        <br />
        <Form.Label htmlFor="date">DATA DA PUBLICAÇÃO *</Form.Label>
        <Form.Control
          type="date"
          id="date"
          value={datePublication}
          onChange={event => setDatePublication(event.target.value)}
        />

      <BtLoad onClick={cadastrarNoticia} state={stateBt} title='Cadastrar'/>
      </Form>
      <Modal show={show} style={{
        fontSize: 15, display: 'flex',
        justifyContent: 'center', alignItems: 'center'
      }}>
        <Modal.Title>
          <p >Upload de Imagem</p></Modal.Title>
        <Modal.Body>
          Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
      </Modal>
    </div>
  );
}

async function sendPushNotification(destin) {

  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    mode: 'no-cors',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': true,
    },
    body: JSON.stringify({
      to: destin,
      sound: 'default',
      body: 'Nova notícia da instuição!',
      data: { data: 'goes here' },
    }),
  });
}
