import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { Modal, FormControl, InputGroup, Button, Form, Table, Alert, ProgressBar } from 'react-bootstrap'
import ReactPaginate from 'react-paginate';
import BtLoad from "../ButtonLoading"

import { BsSearch } from 'react-icons/bs';

import './style.css';
import { Styledh4 } from './style.js';

export default function Disciplinas() {
  const [datavalue, setdatavalue] = useState([]);
  const [turmas, setturmas] = useState([]);
  const [disc, setdisc] = useState('');
  const [filter, setfilter] = useState('');
  const [nomeEdit, setnomeEdit] = useState('');
  const [atualizar, setAtualizar] = useState(false);
  const [cadastrar, setCadastrar] = useState(false);
  const [excluirDisciplina, setexcluirDisciplina] = useState(false);
  const [idEdit, setIdEdit] = useState('');
  const [security, setsecurity] = useState('');
  const [confirmSecurity, setconfirmSecurity] = useState('');
  const [idDisciplina, setidDisciplina] = useState('');
  const perPage = 5;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const [offset, setoffset] = useState(0);
  const [isLoading, setisLoading] = useState(true);
  const [stateBt, setstateBt] = useState(false);
  

  useEffect(() => {
    GetDisc();
    Turmas();
  }, []);

  function GetDisc() {
    const token = localStorage.getItem("token");
    api
      .get("/getDisciplina", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        if (res.data.length !== 0)
          setdatavalue(res.data);
        else
          setdatavalue([{
                nome: "Nenhum dado cadastrado",
            }])
        setisLoading(false);


        setdatavalue(res.data);
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function cadastDisc() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarDisciplina",
        {
          nome: disc,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        setCadastrar(false);
        setdisc('');
        GetDisc();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function editDisc() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .put(
        "editarDisciplina",
        {
          nome: nomeEdit,
          id: idEdit,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        setAtualizar(false);
        GetDisc();
      })
      .catch(e => {
        console.log(e);
      });
  }

  async function deleteDisc(event) {
    setstateBt(true)
    
    const token = localStorage.getItem("token");
    api.put("/deleteDisciplina",
      {
        id: idDisciplina
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setstateBt(false)
        turmas.forEach(async (trm, idx) => await delDic(trm, idx))
        window.location.reload();
      })
      .catch(e => {
        console.log(e);
      });
  }

  async function delDic(turma, idx) {
    var mat = turma.materias.filter(Doc => Doc.id !== idDisciplina);
    var notasAl = [];
    turma.alunos.forEach((aluno, index) => {
        notasAl.push({ id: aluno.id, notas: [] });
        var nt = [];
        mat.forEach(materia => {
            materia.alunos.forEach(aln => {
                if (aln.id === aluno.id) { nt.push({ materia: materia.nome, notas: aln.notas }); return }
            })
        })
        Object.assign(notasAl[index].notas, nt);
    })
    await updateTurmas(mat, notasAl, turma.id)
}

  function Turmas() {
    const token = localStorage.getItem("token");
    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setturmas(res.data)
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  async function updateTurmas(materias, notasAlunos, id) {
    const token = localStorage.getItem("token");
    await api
        .put(
            "addNotas",
            {
                materias: materias,
                id: id,
                notas: notasAlunos,
            },
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        )
        .then(res => {
        })
        .catch(e => {
            console.log(e);
        });
}


  const lowercasedFilter = filter.toLowerCase();
  const filteredData = datavalue.filter(item => {
    return Object.keys(item).some(key =>
      item[key].toLowerCase().includes(lowercasedFilter)
    );
  });

  useEffect(() => {
    if (filter !== '') {
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  }, [filter, setoffset, forceOld, offsetOld]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if (filter === '') {
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  function ReceivedData() {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(pd =>
      <tr >

        <td id='bordaArreEsq'>{pd.nome}</td>
        <td id='bordaArreDir2'>

          <Button
            disabled={(pd.nome === 'Nenhum dado cadastrado')}
            id="button"
            variant="outline-primary"
            onClick={() => { setAtualizar(true) }}
            onClickCapture={event => { setIdEdit(pd.id); setnomeEdit(pd.nome) }}
            style={{ marginRight: 5 }}> Editar </Button>

          <Button
            disabled={(pd.nome === 'Nenhum dado cadastrado')} 
            id="button2" 
            variant="outline-primary" 
            value={pd.id}
            onClick={() => {
              setexcluirDisciplina(true);
              setidDisciplina(pd.id);
              setsecurity(pd.nome)
            }}
          >Excluir</Button>

        </td>
      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    return postData1;
  }


  return (
    <>
      <div id="tagBarraPesquisa" >

        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Disciplina'}
          />

          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>

        <Button id="BotaoCadDisc" onClick={() => { setCadastrar(true) }} >Cadastrar Disciplina</Button>
      </div>

      <div id="listagem">

        <Table id="tabelaDisc" borderless striped responsive="sm" >
          <thead>
            <tr>
              <th>Disciplina</th>
              <th>Ações</th>
            </tr>
          </thead>

          <tbody>
            {<ReceivedData/>}

          </tbody>
        </Table>

        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          forcePage={forceNew} />

        <Modal show={atualizar} onHide={() => setAtualizar(false)} centered size="lg">
          <Modal.Header id="modalHeader">
            <Modal.Title >Edição de Disciplina</Modal.Title>
          </Modal.Header>
          <Modal.Body id="modalBody">

            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="inputColor">Disciplina</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl

                value={nomeEdit}
                onChange={event => setnomeEdit(event.target.value)}
              />
            </InputGroup>

          </Modal.Body>
          <Modal.Footer id="modalBody">
            <Styledh4>Deseja salvar a alteração?</Styledh4>
            <BtLoad id="BotaoEditS" onClick={editDisc} state={stateBt} title='Sim' />
            <Button id="BotaoEditN" onClick={() => setAtualizar(false)} >Não</Button>
          </Modal.Footer>
        </Modal>

        <Modal show={cadastrar} onHide={() => setCadastrar(false)} centered size="lg">
          <Modal.Header closeButton id="modalHeader">
            <Modal.Title >Cadastro de Disciplina</Modal.Title>
          </Modal.Header >
          <Modal.Body id="modalBody">

            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="inputColor">Disciplina</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl

                value={disc}
                onChange={event => setdisc(event.target.value)}
              />
            </InputGroup>

          </Modal.Body>
          <Modal.Footer id="modalBody">
          <BtLoad id="BotaoEditS" onClick={cadastDisc} state={stateBt} title='Cadastrar' />
          </Modal.Footer>
        </Modal>

        <Modal show={excluirDisciplina} onHide={() => setexcluirDisciplina(false)} >
          <Modal.Header id="modalHeader">
            <Modal.Title>Deseja Excluir a disciplina? </Modal.Title>
          </Modal.Header>
          <Modal.Body id="modalBody">

            <p>Se essa disciplina for deletada, todo o histórico e notas dos alunos cadastrados nela serão excluídos de forma permanente e não poderão ser recuperados. </p>

            <Alert variant='danger'>
              <Form.Label htmlFor="text">Para deletar a disciplina, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
            </Alert>

            <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />


          </Modal.Body>
          <Modal.Footer id="modalBody">
            <BtLoad id="BotaoEditS" onClick={event => {
              if (security === confirmSecurity) deleteDisc(event.target.value)
              else {
                setexcluirDisciplina(false)

                setTimeout(() => { setexcluirDisciplina(true) }, 1000);
              }
            }} state={stateBt} title='Sim' />
            <Button id="BotaoEditN" onClick={() => setexcluirDisciplina(false)} >Não</Button>
          </Modal.Footer>
        </Modal>
      </div>

      <Modal show={isLoading} size='lg' centered animation>
        <Modal.Body>
            <ProgressBar animated now={100} />
        </Modal.Body>
    </Modal>
    </>
  );
}
