import React from 'react';
import { Spinner, Button } from 'react-bootstrap'
function ButtonLoading({id, state, title, onClick, ...props}) {
    return (
      <Button
      id={id}
        type='submit'
        variant="primary"
        disabled={state}
        onClick={(event)=>onClick(event)}
        {...props}
      >
        {state ? <Spinner
      as="span"
      animation="border"
      size="sm"
      role="status"
      aria-hidden="true"
    /> : title}
      </Button>
    );
  }

export default ButtonLoading;