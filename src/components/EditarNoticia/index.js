/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Modal, Form, OverlayTrigger, Tooltip } from "react-bootstrap"
import api from "../../services/api";
import firebase from "../../services/firebase";
import { MdCancel } from "react-icons/md";
import './styles.css'
import BtLoad from "../ButtonLoading"

export default function CadastroNoticia({ history }) {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [filename, setfilename] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");
  const [Porcentagem, setPorcentagem] = useState(0);
  const [Link, setLink] = useState("");
  const [id, setid] = useState("");
  const [show, setshow] = useState(false);
  const [stateBt, setstateBt] = useState(false);

  var curr = new Date();
  curr.setDate(curr.getDate());
  var date = curr.toISOString().substr(0, 10);
  const [datePublication, setDatePublication] = useState(date);

  useEffect(() => {
    setVariables(history.location.state.data)
  }, [])

  function setVariables(data){
    setImage(data.imagem);
    setTitle(data.titulo);
    setLink(data.link);
    setBody(data.corpo);
    setid(data.id);
  }
  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
        cropImage(pictureConverter.result, 1 / 1));
         
      }
      catch (e) {
        console.log("Formato não suportado");
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {
        
        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };

  function editNotice(event) {
    setstateBt(true)

    const token = localStorage.getItem("token");
    api
      .put(
        "editarNoticia",
        {
          corpo: body,
          link: Link,
          titulo: title,
          id: id
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {

        if (picture !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(picture, {contentType: 'image/jpeg'})
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) setTimeout(() => { setshow(false); }, 1000)
            });
        }
    setstateBt(false)

        history.push('/Noticias')
      })
      .catch(e => {
        console.log(e);
      });
  }

  return (
    <div id='box-EdN'>
      <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0}} alt="" src={ImageShow} />
        <div id='input-wrapper-EdN'>
          <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
            Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
            <div className="input-wrapper-EdN">
              <label for='input-file-EdN'>Alterar arquivo</label>
              <input type="file" onChange={(event) => setPic(event)} id='input-file-EdN' />
              <span id='file-name'>{filename}</span>
            </div>
            </OverlayTrigger>
            {(picture !== undefined) ?
            <MdCancel onClick={() => {
              setImage('')
              setfilename('')
              setPicture(undefined)
              document.getElementById("input-file-EdN").value = ''
            }} id='icon-cancel-ca' /> : <div />}
        </div>
      <Form  id='form-EdN'>
        <Form.Label htmlFor="name">Título</Form.Label>
        <Form.Control
          type="text"
          id="title"
          value={title}
          onChange={event => setTitle(event.target.value)}
        />
        <Form.Label htmlFor="name">Corpo</Form.Label>
        <Form.Control
          as="textarea"
          id="name"
          value={body}
          onChange={event => setBody(event.target.value)}
          rows="5"
        />
        <br />
        <Form.Label htmlFor="name">Link</Form.Label>
        <Form.Control
          type="text"
          id="link"
          value={Link}
          onChange={event => setLink(event.target.value)}
        />
        <br />
        <Form.Label htmlFor="date">Data da publicação</Form.Label>
        <Form.Control
          type="date"
          id="date"
          value={datePublication}
          onChange={event => setDatePublication(event.target.value)}
        />

      <BtLoad onClick={editNotice} state={stateBt} title='Atualizar' id='bt-login'/>
      </Form>
      <Modal show={show} style={{
        fontSize: 15, display: 'flex',
        justifyContent: 'center', alignItems: 'center'
      }}>
        <Modal.Title>
          <p >Upload de Imagem</p></Modal.Title>
        <Modal.Body>
          Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
      </Modal>
    </div>
  );
}
