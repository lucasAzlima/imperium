/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom'
import api from "../../services/api";
import firebase from "../../services/firebase";
import './style.css';
import {Table, Card, Button, Modal, Alert, Form} from 'react-bootstrap';
import Inot from '../../assets/imgnot.png';

export default function PerfilAluno(props) {
    const [turmas, setTurmas] = useState([]);
    const [aluno, setaluno] = useState([])
    const [foto, setfoto] = useState('')
    const [turma, setturma] = useState('')
    const [nascimento, setnascimento] = useState('');
    const [editor, seteditor] = useState(false);
    const history = useHistory();
  const [confirmSecurity, setconfirmSecurity] = useState('');
  const [security, setsecurity] = useState('');

    useEffect(() => {
        /* setaluno(history.location.state.data) */
        setaluno(props.history.location.state.data)
        setnascimento(props.history.location.state.data.nascimento.split('-'))
        getFoto(props.history.location.state.data.foto)
        setturma(props.history.location.state.data.turma)
        getTurmas();
    }, []);

    async function getFoto(){
        firebase.storage().ref().child(props.history.location.state.data.foto).getDownloadURL().then(function (url) {
            setfoto(url);
          }).catch(()=>{setfoto('NImage')});
    }

    function getTurmas() {
      const token = localStorage.getItem("token");
  
      api
        .get("/getTurmas", {
          headers: { Authorization: `Bearer ${token}` }
        })
        .then(res => {
          setTurmas(turmas.concat(res.data));
        })
        .catch(e => {
          console.log(e);
        });
    }


    function deleteAluno() {
      if(turma.idTurma!==''){
        var turmaUpdate = turmas.filter(Doc => Doc.id === aluno.turma.idTurma);
      turmaUpdate[0].materias.forEach(materia=>{
        materia.alunos = materia.alunos.filter(Doc => Doc.id !== aluno.id);
      })}
      const token = localStorage.getItem("token");
      api.put("deleteAluno",
        {
          id: aluno.id,
          instituicao:localStorage.getItem('instituicao'),
          idTurma: turma.idTurma,
          alunos: turmaUpdate[0].alunos,
          matricula: aluno.matricula,
          materias: turmaUpdate[0].materias,
          contas: aluno.contas
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res => {
          firebase.storage().ref().child(res.data.path).delete();
          history.push("/Alunos")
        })
        .catch(e => {
          console.log(e);
        });
    }
   
    return (
        <>
        <div className='container-perfil-aluno'>
          <Card id="card-perfilaluno">
            <Card.Img src={(foto!=='NImage')?foto:Inot} id="foto-perfil-aluno"></Card.Img>
            <Card.Body>
                  <Table id="table-viewaluno" responsive="sm" borderless>
                    <tbody border="none">
                      <tr id="tr-conteudo-listaluno">
                        <td id="style-td-perfilaluno">Nome</td>
                        <td >{aluno.nome}</td>
                      </tr>
                      <tr  id="tr-conteudo-listaluno">
                        <td id="style-td-perfilaluno" >Turma</td>
                        <td  >{turma.ano}</td>
                      </tr>
                      <tr  id="tr-conteudo-listaluno">
                        <td id="style-td-perfilaluno" >Nível</td>
                        <td >{aluno.nivel}</td>
                      </tr>
                      <tr  id="tr-conteudo-listaluno">
                        <td  id="style-td-perfilaluno">Data de Nascimento</td>
                        <td >{`${nascimento[2]}/${nascimento[1]}/${nascimento[0]}`}</td>
                      </tr>
                    </tbody>
                  </Table>
                  <div id="button-container-vieewaluno">
                    <Button 
                      id="button-editer"
                        onClick={()=>history.push({pathname:"/editarAluno", state : {data: aluno}})}
                        >Editar
                    </Button>
           
                    <Button 
                        id="button-excluir"
                        onClick={()=>{seteditor(true); setsecurity(aluno.nome)}}
                    >Excluir</Button>
                  </div>
                </Card.Body>
             </Card>
              
              <Modal show={editor} onHide={() => seteditor(false)} >
          <Modal.Header id="modalHeader">
            <Modal.Title>Deseja Excluir o alhno? </Modal.Title>
          </Modal.Header>
          <Modal.Body id="modalBody">

            <p>Se esse aluno for deletado, todo o histórico e notas dele cadastrados serão excluídos de forma permanente e não poderão ser recuperados. </p>

            <Alert variant='danger'>
              <Form.Label htmlFor="text">Para deletar o aluno, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
            </Alert>

            <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />


          </Modal.Body>
          <Modal.Footer id="modalBody">
            <Button id="BotaoEditS" onClick={event => {
              if (security === confirmSecurity) deleteAluno(event.target.value)
              else {seteditor(false)}
            }}>Sim</Button>
            <Button id="BotaoEditN" onClick={() => seteditor(false)} >Não</Button>
          </Modal.Footer>
        </Modal>
          </div>
        </>
    );
} 
