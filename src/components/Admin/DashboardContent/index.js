import React from "react";
import './styles.css';
import { CardColumns, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import CadastroDeAdmin from "../../../assets/images-dashAdmin/cadastrodeInst.svg";
import ListadeInst from "../../../assets/images-dashAdmin/listaInstituição.svg";
import CadInst from "../../../assets/images-dashAdmin/cadastro de administrador.svg";
import ListAdmin from "../../../assets/images-dashAdmin/listadeAdmin.svg";

export default function DashboardContent() {
  function RepeterStyle(props) {
    return (
      <Card id="card-dashBoardAdmin">
        <Card.Subtitle id="text-title-dashAdmin">{props.name}</Card.Subtitle>
        <Link to={props.path} id="card-dashBoard-link">
          <img src={props.img} alt={props.name} />
        </Link>
      </Card>
    );
  }
  return (
    <>
      <CardColumns id="cardColumns-dashAdmin">
        <RepeterStyle
          name="Lista de Instituições"
          path="/ListInstituicoes"
          img={ListadeInst}
        />
        <RepeterStyle
          name="Cadastrar Instituições"
          path="/novaInst"
          img={CadInst}
        />
        <RepeterStyle
          name="Administradores"
          path="/ListAdmins"
          img={ListAdmin}
        />
        <RepeterStyle
          name="Cadastro de Administrador"
          path="/CadAdmins"
          img={CadastroDeAdmin}
        />
      </CardColumns>
    </>
  );
}