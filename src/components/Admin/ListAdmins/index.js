/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import {
    Button, Form, Modal,
    OverlayTrigger, Table, Tooltip,
    ProgressBar,
    Alert
} from 'react-bootstrap'
import { FaTrash, FaEdit } from "react-icons/fa";
import './styles.css'
import { BsSearch } from 'react-icons/bs';
import ReactPaginate from 'react-paginate';
import api from "../../../services/api";
import BtLoad from "../../ButtonLoading"

function Turma({ history }) {
  const [stateBt, setstateBt] = useState(false);
    const [admins, setadmins] = useState([])
    const [confirm, setconfirm] = useState(false);
    const [confirmSecurity, setconfirmSecurity] = useState('');
    const [security, setsecurity] = useState('');
    const [idDelete, setIdDelete] = useState('');
    const [filter, setfilter] = useState('');
    const lowercasedFilter = filter.toLowerCase();
    const [postData, setpostData] = useState('');
    const [offset, setoffset] = useState(0);
    const [isLoading, setisLoading] = useState(true);
    const perPage = 5;
    const [pagecount, setpagecount] = useState(0);
    const [offsetOld, setoffsetOld] = useState(0);
    const [forceNew, setforceNew] = useState(0);
    const [forceOld, setforceOld] = useState(0);
    const filteredData = admins.filter(item => {
        return Object.keys(item).some(key => {
            return (key === 'nome' || key === 'email' || key === 'telefone') ?
                item[key].toLowerCase().includes(lowercasedFilter) :
                item['nome'].toLowerCase().includes(lowercasedFilter)
        });
    });

    useEffect(() => {
        data();
    }, [])

    function data() {
        const token = localStorage.getItem("token");
        api
            .get(
                "/getAdmins",
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                if (res.data.length !== 0)
                    setadmins(res.data);
                else
                    setadmins([{
                        admin: true,
                        cpf: "-",
                        email: "-",
                        gestor: true,
                        id: "",
                        nome: "Nenhum dado cadastrado",
                        telefone: "-"
                    }])
                setisLoading(false);
            })
            .catch(e => {
                console.log(e);
            });
    }

    useEffect(() => { receivedData(offset, filter); }, [offset, filter, admins, isLoading])

    function receivedData(offset) {
        const slice = filteredData.slice(offset, offset + perPage)
        const postData1 = slice.map(data =>
            <tr>
                <td>{data.nome}</td>
                <td>{data.email}</td>
                <td>{data.telefone}</td>
                <td>
                    <div style={{flexDirection:'row'}}>
                    <OverlayTrigger placement="auto" overlay={<Tooltip>Editar</Tooltip>}>
                        <Button
                            disabled={(data.email === '-')}
                            style={{ backgroundColor: 'transparent', border: 'none' }}
                            onClick={() => history.push({ pathname: '/EditAdmin', state: { data: data } })}>
                            <FaEdit color="gray" size={20} />
                        </Button>
                    </OverlayTrigger>
                    <OverlayTrigger placement="auto" overlay={<Tooltip>Remover</Tooltip>}>
                        <Button
                            disabled={(data.email === '-')}
                            style={{ backgroundColor: 'transparent', border: 'none' }}
                            onClick={() => {setconfirm(true); setIdDelete(data.uid); setsecurity(data.nome)}}>
                            <FaTrash color="gray" size={20} />
                        </Button>
                    </OverlayTrigger>
                </div>
                </td>
            </tr>
        )
        setpagecount(Math.ceil(filteredData.length / perPage))
        setpostData(postData1);
    }

    useEffect(() => {
        if (filter !== '') {
            setoffset(0)
            setforceNew(0)
        }
        else {
            setoffset(offsetOld)
            setforceNew(forceOld)
        }
    }, [filter, forceOld, offsetOld]);

    function handlePageClick(e) {
        const selectedPage = e.selected;
        setoffset(selectedPage * perPage);
        setforceNew(selectedPage)
        if (filter === '') {
            setforceOld(selectedPage)
            setoffsetOld(selectedPage * perPage)
        }
    };

    function deleteInst() {
        const token = localStorage.getItem("token");
        setstateBt(true)
        api
            .put(
                "/deleteAdmin",
                {
                    id: idDelete,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(() => {
                setconfirm(false)
                data();
                setstateBt(false)
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div>
            <div id="tagBarraPesquisa">
                <span className="barraDePesquisa">
                    <Form.Control
                        id="barraPesquisa"
                        value={filter}
                        onChange={event => setfilter(event.target.value)}
                        placeholder={'Pesquisar por nome'}
                    />
                    <span id="iconPesquisa"  >
                        <BsSearch size='18px' />
                    </span>
                </span>
                <Button id="BotaoCadResp"
                    onClick={() => history.push('/CadAdmins')}>Cadastrar Admin</Button>
            </div>
            <div id='listagemLA'>
                <Table id="tabelaPadrao" borderless striped responsive="sm">
                    <thead>
                        <tr id="title-aluno-container">
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Telefone</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {postData}
                    </tbody>
                </Table>

                <ReactPaginate
                    previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pagecount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                    forcePage={forceNew} />

            </div>
            <Modal show={isLoading} size='xl' centered animation>
                <Modal.Body>
                    <ProgressBar animated now={100} />
                </Modal.Body>
            </Modal>
            <Modal show={confirm} onHide={() => setconfirm(false)} >
                <Modal.Header id="modalHeader">
                    <Modal.Title>Deseja Excluir a instituição? </Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">
                    <p>Se esse admin for deletado, todo o histórico e dados cadastrados serão excluídos de forma permanente e não poderão ser recuperados. </p>
                    <Alert variant='danger'>
                        <Form.Label htmlFor="text">Para deletar, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
                    </Alert>
                    <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />
                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <BtLoad id="BotaoEditS" onClick={() => {
                        if (security === confirmSecurity) deleteInst()
                        else {
                            setconfirm(false)
                            setTimeout(() => { setconfirm(true) }, 1000);
                         }
                    }} title="sim" state={stateBt}/>
                    <Button id="BotaoEditN" onClick={() => setconfirm(false)} >Não</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Turma;