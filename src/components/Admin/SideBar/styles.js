import styled from 'styled-components';

export const Contain = styled.div`  
    
    margin-top:20px;
    display:flex;
    flex-direction: column;
    
        li {
            margin-left: 0;
            display: flex;
            margin: 0px 1px 5px;
            padding: 0px;
            height:40px;
            justify-content:center;
            align-items: center;
            width: 100%

        svg {
            margin-left:15px;
            margin-right: 10px;
            color:white;
            height: 20px;
            width: 100%;
            max-width:21px;
        }

        a{
            width: 100%;
            margin: 0 1px;
            color: white;
            text-decoration: none;
            font-size: 15px;
            font-weight:bold;
        }

        :hover{
            
            color: #0F4C81;
            background-color: white;
            border-radius: 3px;

            svg{
                color:#0F4C81;
            }

            a{
                color:#0F4C81;
                
            }
        }

`;

export const Container = styled.div`
  @media only screen and (min-width: 1px) {
        width: ${props => (props.grid ? props.grid / 12 * 100 : '8:33')}%;
    }

    justify-content: start;
    display: flex;
    flex-direction: column;
    background-color: #185296;
    border-right: solid 1px #f0f0f5;
    border-radius: 0px 30px 30px 0px;
    min-height: 100vh;
    width: auto;
    position: fixed;
    align-items: stretch;

`;