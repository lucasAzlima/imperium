import React from 'react';
import { Link } from 'react-router-dom'
import { Contain, Container } from './styles'
import { TiGroup } from 'react-icons/ti';
import { FaChalkboardTeacher, FaListAlt } from 'react-icons/fa';
import { MdDashboard } from 'react-icons/md';
import logo from '../../../assets/logoImp.png'
import './styles.css'

export default function SideBar() {
    return (
        <Container grid={2}>
            <figure id='logo-sidebar'>
                <img src={logo} alt="Logo da empresa" width='190px' height='190px' />
            </figure>

            <Contain>
                <ul>
                    <li>
                        <Link to='/DashAdmin'>
                            <MdDashboard />
                                Dashboard
                            </Link>
                    </li>
                    <li>
                        <Link to='/ListAdmins'>
                            <TiGroup />
                                Admins
                            </Link>
                    </li>

                    <li>
                        <Link to='/ListInstituicoes'>
                            <FaChalkboardTeacher />
                                Instituicoes
                            </Link>
                    </li>
                    <li>
                        <Link to='/Planos'>
                            <FaListAlt />
                                Planos
                            </Link>
                    </li>
                </ul>
            </Contain>
        </Container>

    );
}