import styled from 'styled-components';
import {  ButtonToolbar } from 'react-bootstrap'

export const ButtonToolbarStyled = styled(ButtonToolbar)`
    justify-content: "space-between"; 
`;