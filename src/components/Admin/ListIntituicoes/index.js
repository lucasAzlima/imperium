/* eslint-disable array-callback-return */
/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import {
  Form, Button, ButtonGroup, Modal,
  ButtonToolbar, OverlayTrigger, Tooltip,
  Table, Alert, ProgressBar
} from 'react-bootstrap'
import BtLoad from '../../ButtonLoading'
import { FaTrash, FaAddressCard, FaEdit } from "react-icons/fa";
import { BsSearch } from 'react-icons/bs';
import './styles.css'
import api from "../../../services/api";
import ReactPaginate from "react-paginate";

export default function ListInst({ history }) {
  const [stateBt, setstateBt] = useState(false);
  const [instituicoes, setinstituicoes] = useState([]);
  const [confirm, setconfirm] = useState(false);
  const [idInst, setidInst] = useState('');
  const [idGestor, setidGestor] = useState('');
  const [uid, setuid] = useState('');
  const [stateInst, setstateInst] = useState('');
  const [confirmdes, setconfirmdes] = useState(false);
  const [confirmtit, setconfirmtit] = useState('');
  const [security, setsecurity] = useState('');
  const [confirmSecurity, setconfirmSecurity] = useState('');
  const [filter, setfilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const [postData, setpostData] = useState('');
  const [offset, setoffset] = useState(0);
  const perPage = 5;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const filteredData = instituicoes.filter(item => {
    return Object.keys(item).some(key => {
      if (key === 'nomeFantasia') return item[key].toLowerCase().includes(lowercasedFilter)
    });
  });
  const [isLoading, setisLoading] = useState(true);




  useEffect(() => {
    listInst();
  }, []);

  function listInst() {
    const token = localStorage.getItem("token");
    api
      .get(
        "/getInstituicoes",
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setisLoading(false);
        if (res.data.length !== 0)
          setinstituicoes(res.data);
        else
          setinstituicoes([{
            nome: "-",
            status: true,
            id: "",
            nomeFantasia: "Nenhum dado cadastrado",
            gestor: { id: '' }
          }])
      })
      .catch(e => {
        console.log(e);
      });
  }

  function deleteInst() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "/deleteInstituicao",
        {
          id: idInst,
          idGestor: idGestor
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setconfirm(false)
        setstateBt(false)

        listInst()
      })
      .catch(e => {
        console.log(e);
      });
  }

  function desableInst() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .put("/changeStateInstituicao",
        {
          stateInst: !stateInst, instituicao: idInst, uid: uid
        },
        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        setconfirmdes(false);
        setstateBt(false)
        setconfirm(false);
        listInst();
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );

      })
  }

  useEffect(() => { receivedData(offset, filter); }, [offset, filter, instituicoes])

  function receivedData(offset) {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(inst =>
      <tr>
        <td id='bordaArreEsq' >{inst.nomeFantasia}</td>
        <td >
          <ButtonToolbar id='buttonTB'>
            <ButtonGroup id='btgroup' >
              <OverlayTrigger overlay={<Tooltip id="tooltip">Ver instituição</Tooltip>}>
                <Button
                  disabled={(inst.nome === '-')}
                  id="BotaoResp"
                  onClick={() => history.push({ pathname: "/instituicao", state: { instituicao: inst } })}
                ><FaAddressCard size={24} /></Button>
              </OverlayTrigger>
              <OverlayTrigger overlay={<Tooltip id="tooltip">Editar instituição</Tooltip>}>
                <Button
                  disabled={(inst.nome === '-')}
                  id="BotaoResp"
                  onClick={() => history.push({ pathname: "/EditInstituicao", state: { instituicao: inst } })}
                ><FaEdit size={20} /></Button>
              </OverlayTrigger>
              <OverlayTrigger overlay={<Tooltip id="tooltip">Deletar instituição</Tooltip>}>
                <Button
                  disabled={(inst.nome === '-')}
                  id="BotaoResp"
                  onClick={() => {
                    setconfirm(true);
                    setidInst(inst.id);
                    setidGestor(inst.gestor.id);
                    setsecurity(inst.nomeFantasia)
                  }}
                ><FaTrash size={20} /></Button>
              </OverlayTrigger>

            </ButtonGroup>
          </ButtonToolbar>
        </td>
        <td id='bordaArreDir2'>
          <OverlayTrigger placement="left" overlay={<Tooltip id="tooltip">Alterar situação</Tooltip>}>
            {(inst.status ?
              <Button
                disabled={(inst.nome === '-')}
                id="situacao-escola"
                onClick={() => {
                  setconfirmtit("Desativar instituição?");
                  setconfirmdes(true)
                  setidInst(inst.id);
                  setuid(inst.gestor.id);
                  setstateInst(inst.status)
                }}

              >Ativa</Button> :

              <Button
                id="situacao-escola"
                onClick={() => {
                  setconfirmtit("Reativar instituição?");
                  setconfirmdes(true);
                  setidInst(inst.id);
                  setuid(inst.gestor.id);
                  setstateInst(inst.status)
                }}
              >Desativa</Button>
            )}
          </OverlayTrigger>
        </td>
      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);
  }

  useEffect(() => {
    if (filter !== '') {
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  }, [filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if (filter === '') {
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  return (
    < >
      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por nome'}
          />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>

        <Button id="BotaoCadResp" onClick={() => history.push("/novaInst")}>
          Cadastrar instituição
          </Button>
      </div>
      <div id='listagem'>
        <Table id="tabelaCameras" borderless striped responsive='sm'>
          <thead>
            <tr>
              <th >Nome</th>
              <th >Ações</th>
              <th>Situação</th>
            </tr>
          </thead>
          <tbody >
            {postData}
          </tbody>
        </Table>
        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          forcePage={forceNew} />
      </div>
      <Modal show={isLoading} size='xl' centered animation>
        <Modal.Body>
          <ProgressBar animated now={100} />
        </Modal.Body>
      </Modal>

      <Modal show={confirmdes} onHide={() => setconfirmdes(false)}>
        <Modal.Header id="modal-admin-header">
          <Modal.Title>{confirmtit}</Modal.Title>
        </Modal.Header>
        <Modal.Footer  >
          <BtLoad id="BotaoEditS" onClick={desableInst} state={stateBt} title='Sim' />
          <Button
            id="BotaoEditN"
            onClick={() => setconfirmdes(false)}
          >Não</Button>
        </Modal.Footer>
      </Modal>
      <Modal show={confirm} onHide={() => setconfirm(false)} >
        <Modal.Header id="modalHeader">
          <Modal.Title>Deseja Excluir a instituição? </Modal.Title>
        </Modal.Header>
        <Modal.Body id="modalBody">

          <p>Se essa instituição for deletada, todo o histórico e dados cadastrados nela serão excluídos de forma permanente e não poderão ser recuperados. </p>

          <Alert variant='danger'>
            <Form.Label htmlFor="text">Para deletar a instituição, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
          </Alert>

          <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />


        </Modal.Body>
        <Modal.Footer id="modalBody">
          <BtLoad id="BotaoEditS" onClick={() => {
            if (security === confirmSecurity) deleteInst()
            else {
              setconfirm(false)

              setTimeout(() => { setconfirm(true) }, 1000);
            }
          }} title="sim" state={stateBt} />
          <Button id="BotaoEditN" onClick={() => setconfirm(false)} >Não</Button>
        </Modal.Footer>
      </Modal>

    </ >
  );
}
