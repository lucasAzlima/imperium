import React, { useState } from "react";
import { Modal, Form, Col, Tooltip, OverlayTrigger } from "react-bootstrap"
import api from "../../../services/api";
import firebase from "../../../services/firebase";
import BtLoad from "../../ButtonLoading"
import './style.css'
import {useHistory} from 'react-router-dom'
import { MdCancel } from "react-icons/md";
import { Link } from "react-router-dom";
import { TiArrowLeft } from "react-icons/ti";

export default function CadastroNoticia() {
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");
  const [Porcentagem, setPorcentagem] = useState(0);
  const [nome, setname] = useState("");
  const [filename, setfilename] = useState("");
  const [cpf, setcpf] = useState("");
  const [email, setemail] = useState("");
  const [senha, setsenha] = useState("");
  const [tel, settel] = useState("");
  const [show, setshow] = useState(false);
  const [stateBt, setstateBt] = useState(false);
const history = useHistory();
  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
          cropImage(pictureConverter.result, 1 / 1));

      }
      catch (e) {
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {

        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };

  function CadAdmin() {
    setstateBt(true)

    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarAdmin",
        {
          email,
          senha,
          instituicao: '',
          nome,
          cpf,
          telefone: tel,
          Gestor: true,
          Admin: true
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        if (picture !== undefined) {
          setshow(true);
          firebase.storage().ref().child(`Admins/${res.data.id}`).put(picture, { contentType: 'image/jpeg' })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) setTimeout(() => { setshow(false); history.push('/ListAdmins') }, 1000)
            });
        }
        // history.push('/ListAdmins')
      })
      .catch(e => {
        console.log(e);
      });
  }
  return (
    <div id='fundo-cadAd'>

      <Form id='img-cadAd'>
        <hr />
        <div style={{ alignItems: 'center' }}>

          <Link id="bt-return" to="/ListAdmins">
            <OverlayTrigger placement="right" overlay={<Tooltip>Retornar</Tooltip>}>
              <TiArrowLeft size={30} color="gray"></TiArrowLeft>
            </OverlayTrigger>
          </Link>
          <h5 style={{ marginLeft: '40%' }}>Cadastrar administrador</h5>
        </div>

        <hr />
        <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0
        }} alt="" src={ImageShow} />
        <div id='input-wrapper-1'>
          <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
            Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
            <div className="input-wrapper">

              <label for='input-file'>Selecionar um arquivo</label>
              <input type="file" onChange={(event) => setPic(event)} id='input-file' />
              <span id='file-name'>{filename}</span>
            </div>
          </OverlayTrigger>
          {(picture !== undefined) ?
            <MdCancel onClick={() => {
              setImage('')
              setfilename('')
              setPicture(undefined)
              document.getElementById("input-file").value = ''
            }} id='icon-cancel-ca' /> : <div />}
        </div>
        <Form.Row id='form-cadAd'>
          <Form.Group as={Col} >
            <Form.Label>Nome</Form.Label>
            <Form.Control
              type="text"
              placeholder="Nome"
              value={nome}
              onChange={e => setname(e.target.value)} />
          </Form.Group>
          <Form.Group >
            <Form.Label>CPF</Form.Label>
            <Form.Control
              placeholder="cpf"
              value={cpf}
              onChange={e => setcpf(e.target.value)} />
          </Form.Group>
          <Form.Group as={Col} >
            <Form.Label>Telefone</Form.Label>
            <Form.Control
              type="tel"
              placeholder="Telefone"
              value={tel}
              onChange={e => settel(e.target.value)} />
          </Form.Group>
        </Form.Row>
        <Form.Row id='form-cadAd'>

          <Form.Group as={Col}>
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Email"
              value={email}
              onChange={e => setemail(e.target.value)} />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>Senha</Form.Label>
            <Form.Control
              type="text"
              placeholder="senha"
              value={senha}
              onChange={e => setsenha(e.target.value)} />
          </Form.Group>
        </Form.Row>
        <BtLoad onClick={CadAdmin} state={stateBt} title='Cadastrar' />
      </Form>
      <Modal show={show} style={{
        fontSize: 15, display: 'flex',
        justifyContent: 'center', alignItems: 'center'
      }}

      >
        <Modal.Title>
          <p >Upload de Imagem</p></Modal.Title>
        <Modal.Body>
          Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
      </Modal>
    </div>
  );
}
