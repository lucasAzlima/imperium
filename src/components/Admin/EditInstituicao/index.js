/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom'
import { Button, Form, Col, Tooltip, OverlayTrigger } from 'react-bootstrap'
import { FaChevronLeft } from 'react-icons/fa'
import BtLoad from "../../ButtonLoading";
import api from "../../../services/api";
import './styles.css';

export default function EditInstituicao({ history }) {

    const [gestorBt, setgestorBt] = useState(false);
    const [instBt, setinstBt] = useState(false);
    const [update, setupdate] = useState(false);
    const [cep, setcep] = useState("");
    const [gstr, setgestor] = useState([]);
    const [instit, setinstituicao] = useState([]);
    const [idInst, setidInst] = useState("");
    const [cnpj, setcnpj] = useState("");
    const [email, setemail] = useState("");
    const [nomeEmpresarial, setnomeEmpresarial] = useState("");
    const [nomeFantasia, setnomeFantasia] = useState("");
    const [telefone, settelefone] = useState("");
    const [telefoneg, settelefoneg] = useState("");
    const [nomeg, setnomeg] = useState("");
    const [cpf, setcpf] = useState("");
    const [emailg, setemailg] = useState("");
    const [emailOld, setemailgOld] = useState("");
    const [stateBt, setstateBt] = useState(false);
    const [planos, setplanos] = useState([{nome:'Selecionar plano', value:0, alunos:0}]);
    const [plano, setplano] = useState("");
    const [PlanoInst, setPlanoInst] = useState("");

    useEffect(() => {
        variaveis(history.location.state.instituicao);
        getPlanos();
    }, []);

    async function updateInstituicao() {
        setstateBt(true)
        const token = localStorage.getItem("token");
        let gestor = {
            telefone: telefoneg,
            nome: nomeg,
            cpf: cpf,
            email: emailg
        }
        await api
            .post("/updateInstituicao",
                {
                    cep: cep,
                    cnpj: cnpj,
                    email: email,
                    emailOld: emailOld,
                    nomeEmpresarial: nomeEmpresarial,
                    nomeFantasia: nomeFantasia,
                    gestor: gestor,
                    plano: PlanoInst,
                    telefone: telefone,
                    id: idInst,
                    UpdateGestor:false
                },
                {
                    headers: { Authorization: `Bearer ${token}` }
                })
            .then(res => {
                setstateBt(false)

            })
            .catch(e => {
                console.log(e);
                console.log(
                    "Erro de autenticação, verifique seu email e senha"
                );
                setstateBt(false)
            });
    }

    function variaveis(instituicao) {
        const inst = {
            cnpj: instituicao.cnpj,
            cep: instituicao.cep,
            email: instituicao.email,
            nomeFantasia: instituicao.nomeFantasia,
            nomeEmpresarial: instituicao.nomeEmpresarial
        }
        setinstituicao(inst);

        setidInst(instituicao.id)
        setcep(instituicao.cep)
        setcnpj(instituicao.cnpj)
        setemail(instituicao.email)
        setnomeFantasia(instituicao.nomeFantasia)
        setnomeEmpresarial(instituicao.nomeEmpresarial)
        settelefone(instituicao.telefone)
        setplano(instituicao.plano.nome)
        var gestor = instituicao.gestor;
        setgestor(gestor);

        setemailg(gestor.email)
        setemailgOld(gestor.email)
        setcpf(gestor.cpf)
        setnomeg(gestor.nome)
        settelefoneg(gestor.telefone)
    }

    function getPlanos() {
        const token = localStorage.getItem("token");
    
        api
          .get("/getPlanos", {
            headers: { Authorization: `Bearer ${token}` }
          })
          .then(res => {
            setplanos(planos.concat(res.data))
          })
          .catch(e => {
            console.log(e);
          });
      }

    function resetInst() {
        setcep(instit.cep)
        setcnpj(instit.cnpj)
        setemail(instit.email)
        setnomeFantasia(instit.nomeFantasia)
        setnomeEmpresarial(instit.nomeEmpresarial)

        setemailg(gstr.email)
        setemailgOld(gstr.email)
        setcpf(gstr.cpf)
        setnomeg(gstr.nome)
        settelefoneg(gstr.telefone)
    }

    return (
        <div id="form-edicao">
            <OverlayTrigger overlay={<Tooltip>Voltar</Tooltip>}>

                <Button id='button-back'>
                    <Link to='Listinstituicoes'>
                        <FaChevronLeft />
                    </Link>
                </Button>

            </OverlayTrigger>
            
            <header id="header-form-edicao">
                <h4>Edição da instituição</h4>
                <p><strong>Formulário para edição de uma instituição</strong></p>
            </header>
            <hr />
            <Form >
                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">Nome da instituição</Form.Label>
                        <Form.Control required
                            onChange={(e) => (setnomeEmpresarial(e.target.value), setupdate(true))}
                            value={nomeEmpresarial} disabled={instBt}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">Email da instituição</Form.Label>
                        <Form.Control required
                            onChange={(e) => (setemail(e.target.value), setupdate(true))}
                            value={email} disabled={instBt}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Quantidades de Alunos Máxima</Form.Label>
              <Form.Control as="select"
                            value={plano}
                            onChange={event => { 
                              setplano(event.target.value);
                              setPlanoInst(planos.filter(Doc=>Doc.nome===event.target.value)[0]) }}>
                            {planos.map(plano => <option>{plano.nome}</option>)}
                        </Form.Control>
            </Form.Group>
                </Form.Row>

                <Form.Row >
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">Telefone da instituição</Form.Label>
                        <Form.Control 
                            required
                            type="number"
                            onChange={(e) => (settelefone(e.target.value), setupdate(true))}
                            value={telefone} disabled={instBt}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">CNPJ da instituição</Form.Label>
                        <Form.Control 
                            required
                            type="number"
                            onChange={(e) => (setcnpj(e.target.value), setupdate(true))}
                            value={cnpj} disabled={instBt}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">CEP da instituição</Form.Label>
                        <Form.Control 
                            required
                            type="number"
                            onChange={(e) => (setcep(e.target.value), setupdate(true))}
                            value={cep} disabled={instBt}
                        />
                    </Form.Group>
                </Form.Row>
                <hr />
                <section id="header-form-edicao">
                    <h4>Edição do gestor</h4>
                    <p><strong>Formulário para edição do gestor </strong></p>

                </section>
                <hr />
                <Form.Row >
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">Nome</Form.Label>
                        <Form.Control required
                            onChange={(e) => (setnomeg(e.target.value), setupdate(true))}
                            value={nomeg} disabled={gestorBt}
                        />
                    </Form.Group>

                    <OverlayTrigger overlay={<Tooltip id="tooltip">
                        Ao mudar o email, caso queira mudar a senha,
                        basta ir na tela de login e recuperar a senha
                            com o email para qual foi mudado</Tooltip>}>
                        <Form.Group as={Col}>
                            <Form.Label id="label-form">Email</Form.Label>
                            <Form.Control required
                                type='email'
                                onChange={(e) => (setemailg(e.target.value), setupdate(true))}
                                value={emailg} disabled={gestorBt}
                            />
                        </Form.Group>
                    </OverlayTrigger>
                </Form.Row>
                <Form.Row >
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">CPF</Form.Label>
                        <Form.Control 
                            required
                            type="number"
                            onChange={(e) => (setcpf(e.target.value), setupdate(true))}
                            value={cpf} disabled={gestorBt}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label id="label-form">Telefone</Form.Label>
                        <Form.Control required
                            type="number"
                            onChange={(e) => (settelefoneg(e.target.value), setupdate(true))}
                            value={telefoneg} disabled={gestorBt}
                        />
                    </Form.Group>
                </Form.Row>
            </Form>
            <div id="buttons-edit">
                <BtLoad  onClick={updateInstituicao} state={stateBt} id="button-edit" title='Atualizar informações' />
                {(update) ? <Button  id="discard-edit" onClick={() => resetInst()} >Descartar</Button> : <div />}
            </div>
        </div>
    );
}