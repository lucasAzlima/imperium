/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { OverlayTrigger, Popover, ListGroup, Modal, Button, Form, InputGroup, FormControl, Alert } from "react-bootstrap";
import firebase from "../../../services/firebase"
import api from "../../../services/api"
import { useHistory } from 'react-router-dom'
import school from '../../../assets/school.svg'


export default function NavBar() {
  const [admin, setadmin] = useState({ nome: '', email: '', telefone: '' })
  const [imagem, setimagem] = useState(school)
  const [novasenha, setnovasenha] = useState('')
  const [senhaatual, setsenhaatual] = useState('')
  const history = useHistory();
  const [confirmdes, setconfirmdes] = useState(false);
  const [passAlert, setpassAlert] = useState(false);

  function handleClick() {
    firebase.auth().signOut().then(() => {
      localStorage.setItem("token", '');
      history.push('/login')
    });
  }

  useEffect(() => {
    
    if (!JSON.parse(localStorage.getItem('adminG'))) {
      Getuid();
    } else {
      setadmin(JSON.parse(localStorage.getItem('adminG')));
      setimagem(localStorage.getItem('imgpflA'))

    }
    if (localStorage.getItem("token") === '') {
      alert("desconectado, entre novamente no sistema");
      history.push('/login')
    }
  }, []);

  function Getuid() {
    const token = localStorage.getItem("token");
    api.get("/getuid", {
      headers: { Authorization: `Bearer ${token}` }
    })
      .then(res => {
        firebase.firestore().collection('admins').doc(res.data.uid).get().then(doc => {
          setadmin(doc.data())
          localStorage.setItem('adminG', JSON.stringify(doc.data()))
          firebase.storage().ref().child(`Admins/${doc.data().id}`).getDownloadURL().then(function (url) {
            setimagem(url)
            localStorage.setItem('imgpflA', url)
          });
        })
      })
      .catch(e => {
        console.log(e);
      });
  }

  function newPass() {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        setpassAlert(false)
        user.updatePassword(novasenha).then(function () {
          const credential = firebase.auth.EmailAuthProvider.credential(user.email, novasenha);
          user.reauthenticateWithCredential(credential).then(function () {
            window.location.reload();
          }).catch(function (error) {
            window.location.reload()
          });
        }).catch(function (error) {
          setpassAlert(true)
        });
      } else {
        setpassAlert(true)
      }
    });
  }

  const popover = (
    <Popover id="popover-basic-G" >
      <Popover.Title as="h3" id='title-NB-G'>{admin.nome}</Popover.Title>
      <Popover.Content>
        <ListGroup id='LG-NB-G'>
          <ListGroup.Item id='item-NB-G'>Telefone: {admin.telefone}</ListGroup.Item>
          <ListGroup.Item id='item-NB-G'>Email: {admin.email}</ListGroup.Item>
          <ListGroup.Item id='item-NB-sair-G' onClick={() => setconfirmdes(true)}>Mudar senha</ListGroup.Item>
          <ListGroup.Item id='item-NB-sair-G' onClick={() => handleClick()}>Sair</ListGroup.Item>
        </ListGroup>
      </Popover.Content>
    </Popover>
  );


  return (
    <div id="container-nav">
      <div className='position-nav'>
        <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
          <div id="container-nav2">
            <span>Olá, {admin.nome}</span>

            <img src={imagem} alt="Admin" className="avatar" style={{
              width: imagem ? '10vh' : 0,
              height: imagem ? '10vh' : 0
            }} />
          </div>
        </OverlayTrigger>

      </div>
      <Modal show={confirmdes} onHide={() => setconfirmdes(false)}>
        <Modal.Header id="modal-admin-header">
          <Modal.Title>Alterar senha</Modal.Title>
        </Modal.Header>
        <Modal.Body id="modalBody">
          {passAlert ?
            <Alert variant='danger'>
              <Form.Label htmlFor="text">
                Algo deu errado, por favor verificar se os dados estão corretos.
              </Form.Label>
            </Alert> : <div />}
          <InputGroup id="editInput">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Senha atual</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              type="password"
              value={senhaatual}
              onChange={event => setsenhaatual(event.target.value)}
            />
          </InputGroup>
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor" >Nova senha</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              type="password"
              value={novasenha}
              onChange={event => { setnovasenha(event.target.value); }} />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer id="modalBody">
          <Button id="BotaoEditS" onClick={()=>newPass()}>Alterar</Button>
          <Button
            id="BotaoEditN"
            onClick={() => setconfirmdes(false)}
          >Não</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
