/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import api from "../../../services/api";
import { Form, Col, OverlayTrigger, Tooltip } from 'react-bootstrap'
import BtLoad from "../../ButtonLoading"
import './styles.css';
import { Link } from 'react-router-dom';
import { TiArrowLeft } from 'react-icons/ti';

export default function ListInst({ history }) {
  const [cep, setcep] = useState("213123");
  const [planos, setplanos] = useState([{nome:'Selecionar plano', value:0, alunos:0}]);
  const [plano, setplano] = useState("");
  const [cnpj, setcnpj] = useState("123123");
  const [email, setemail] = useState("sdasd@");
  const [nomeEmpresarial, setnomeEmpresarial] = useState("teste");
  const [nomeFantasia, setnomeFantasia] = useState("teste");
  const [telefone, settelefone] = useState("123123");
  const [telefoneg, settelefoneg] = useState("123123");
  const [nomeg, setnomeg] = useState("weqwqwe");
  const [cpf, setcpf] = useState("123123");
  const [emailg, setemailg] = useState("teste@gmail.com");
  const [PlanoInst, setPlanoInst] = useState("");
  const [senha, setsenha] = useState("123123");
  const [EstrutEstudo, setEstrutEstudo] = useState("4");
  const [stateBt, setstateBt] = useState(false);

  useEffect(() => { getPlanos() }, [])

  function cadInstituicao() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "/cadastrarInstituicao",
        {
          cep: cep,
          cnpj: cnpj,
          email: email,
          nomeEmpresarial: nomeEmpresarial,
          nomeFantasia: nomeFantasia,
          plano: PlanoInst,
          EstruturaCur: EstrutEstudo,
          gestor: {
            nome: nomeg,
            cpf: cpf,
            email: emailg,
            telefone: telefoneg,
            senha: senha
          },
          telefone: telefone
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        // history.push("/ListInstituicoes")
      })
      .catch(e => {
        console.log(e);
      });
  }

  function getPlanos() {
    const token = localStorage.getItem("token");

    api
      .get("/getPlanos", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setplanos(planos.concat(res.data))
      })
      .catch(e => {
        console.log(e);
      });
  }
  return (
    <div id="formni">
      <Link id="linkCadINs" to="/ListInstituicoes">
          <OverlayTrigger placement="right" overlay={<Tooltip>Retornar</Tooltip>}>
                <TiArrowLeft size={30}></TiArrowLeft>
          </OverlayTrigger>
      </Link>
      <div id="header-formCadInst">
          <strong>Formulário da Instituição</strong>
          <p>No formulário abaixo você preencherá com algumas informações sobre a instituição</p>
      </div>
      <Form>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label id="label-CadInst">Nome da Instituição</Form.Label>
            <Form.Control
              value={nomeEmpresarial}
              type="text"
              onChange={e => setnomeEmpresarial(e.target.value)}
            >
            </Form.Control>
          </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Telefone</Form.Label>
              <Form.Control
                
                value={telefone}
                onChange={e => settelefone(e.target.value)} 
              >
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Sigla Institucional</Form.Label>
              <Form.Control
                type="text"
                
                value={nomeFantasia}
                onChange={e => setnomeFantasia(e.target.value)} 
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Quantidades de Alunos Máxima</Form.Label>
              <Form.Control as="select"
                            value={plano}
                            onChange={event => { 
                              setplano(event.target.value);
                              setPlanoInst(planos.filter(Doc=>Doc.nome===event.target.value)[0]) }}>
                            {planos.map(plano => <option>{plano.nome}</option>)}
                        </Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Estrutura Curricular</Form.Label>
              <Form.Control
                type="number"
                placeholder="Ex: 4"
                value={EstrutEstudo}
                onChange={e => setEstrutEstudo(e.target.value)}
              >
              </Form.Control>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">CNPJ</Form.Label>
              <Form.Control
                
                value={cnpj}
                onChange={e => setcnpj(e.target.value)}
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">CEP</Form.Label>
              <Form.Control
                value={cep}
                onChange={e => setcep(e.target.value)}
              ></Form.Control>
            </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Email</Form.Label>
              <Form.Control
                
                value={email}
                onChange={e => setemail(e.target.value)}
              ></Form.Control>
          </Form.Group>
        </Form.Row>
      </Form>
      <div id="header-formCadInst">
        <strong>Informações sobre o Gestor</strong>
        <p>A partir dessas informações abaixo que ele terá acesso ao sistema da escola dele</p>
      </div>
      <Form>
        <Form.Row>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Nome</Form.Label>
              <Form.Control
                value={nomeg}
                onChange={(e) => setnomeg(e.target.value)}
                />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">CPF</Form.Label>
              <Form.Control
                value={cpf}
                onChange={(e) => setcpf(e.target.value)}
                 />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Telefone</Form.Label>
              <Form.Control
                value={telefoneg}
                onChange={(e) => settelefoneg(e.target.value)}
                type="tel"
                 />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Senha</Form.Label>
              <Form.Control
                value={senha}
                onChange={(e) => setsenha(e.target.value)}
                type="password"
                 />
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col}>
              <Form.Label id="label-CadInst">Email</Form.Label>
              <Form.Control
                value={emailg}
                onChange={(e) => setemailg(e.target.value)}
                type="email"
                 />
            </Form.Group>
          </Form.Row>
        </Form>
        <BtLoad  onClick={cadInstituicao} state={stateBt} title='Cadastrar Instituição' id='btni'/>
    </div >
  );
}
