/* eslint-disable array-callback-return */
/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import {
    Alert, Button, Form, FormControl,
    InputGroup, Modal, OverlayTrigger,
    Table, Tooltip, ProgressBar
} from 'react-bootstrap';
import { AiOutlineEdit } from 'react-icons/ai';
import { BsSearch } from 'react-icons/bs';
import { FiTrash2 } from 'react-icons/fi';
import ReactPaginate from 'react-paginate';
import './styles.css'
import api from "../../../services/api";
import BtLoad from "../../ButtonLoading"
function Planos() {
    const [cadPlan, setcadPlan] = useState(false);
    const [edPlan, setedPlan] = useState(false);
    const [delPlan, setdelPlan] = useState(false);
    const [stateBt, setstateBt] = useState(false);
    const [filter, setfilter] = useState('');
    const [dados, setdados] = useState([]);
    const [planoatual, setplanoatual] = useState({});
    const [nome, setnome] = useState('');
    const [security, setsecurity] = useState('');
    const [confirmSecurity, setconfirmSecurity] = useState('');
    const [alunos, setalunos] = useState('');
    const [valor, setvalor] = useState('');
    const [nomeEdit, setnomeEdit] = useState('');
    const [alunosEdit, setalunosEdit] = useState('');
    const [valorEdit, setvalorEdit] = useState('');
    const [id, setid] = useState('');
    const lowercasedFilter = filter.toLowerCase();
    const [postData, setpostData] = useState('');
    const [offset, setoffset] = useState(0);
    const perPage = 5;
    const [pagecount, setpagecount] = useState(0);
    const [offsetOld, setoffsetOld] = useState(0);
    const [forceNew, setforceNew] = useState(0);
    const [forceOld, setforceOld] = useState(0);
    const filteredData = dados.filter(item =>
        Object.keys(item).some(key =>{
             if(key!== 'nome')return item[key].toLowerCase().includes(lowercasedFilter)
        }));
        const [isLoading, setisLoading] = useState(true);
        
        
        

    useEffect(() => { getPlanos() }, [])

    useEffect(() => { receivedData(offset, filter) }, [offset, filter, dados])

    function receivedData(offset) {
        const slice = filteredData.slice(offset, offset + perPage)
        const postData1 = slice.map(plano =>
            <tr>
                <td>{plano.nome}</td>
                <td>{plano.alunos}</td>
                <td>{plano.valor}</td>
                <td>
                    <OverlayTrigger placement="auto" overlay={<Tooltip>Editar</Tooltip>}>
                        <Button
                            disabled={(plano.valor==='-')}
                            style={{ backgroundColor: 'transparent', border: 'none' }}
                            onClick={() => {
                                setedPlan(true);
                                setid(plano.id);
                                setnomeEdit(plano.nome);
                                setalunosEdit(plano.alunos);
                                setvalorEdit(plano.valor)
                                setplanoatual(plano);
                            }}>
                            <AiOutlineEdit color="gray" size={25} />
                        </Button>
                    </OverlayTrigger>
                    <OverlayTrigger placement="auto" overlay={<Tooltip>Remover</Tooltip>}>
                        <Button
                            disabled={(plano.valor==='-')}
                            style={{ backgroundColor: 'transparent', border: 'none' }}
                            onClick={() => { setdelPlan(true); setid(plano.id); setsecurity(plano.nome) }}>
                            <FiTrash2 color="gray" size={25} />
                        </Button>
                    </OverlayTrigger>
                </td>
            </tr>
        )
        setpagecount(Math.ceil(filteredData.length / perPage))
        setpostData(postData1);
    }

    useEffect(() => {
        if (filter !== '') {
            setoffset(0)
            setforceNew(0)
        }
        else {
            setoffset(offsetOld)
            setforceNew(forceOld)
        }
    }, [filter, forceOld, offsetOld]);

    function handlePageClick(e) {
        const selectedPage = e.selected;
        setoffset(selectedPage * perPage);
        setforceNew(selectedPage)
        if (filter === '') {
            setforceOld(selectedPage)
            setoffsetOld(selectedPage * perPage)
        }
    };


    function getPlanos() {
        const token = localStorage.getItem("token");

        api
            .get("/getPlanos", {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                if (res.data.length !== 0)
                    setdados(res.data);
                else 
                setdados([{
                    id: "",
                    nome: "Nenhum plano cadastrado",
                    alunos: "-",
                    valor: "-",
                }])
                setisLoading(false);
            })
            .catch(e => {
                console.log(e);
            });
    }

    function cadastrarPlanos() {
        const token = localStorage.getItem("token");
        setstateBt(true)
        api
            .post(
                "cadastrarPlanos",
                {
                    nome: nome,
                    alunos: alunos,
                    valor: valor
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(() => {
                setcadPlan(false)
                setstateBt(true)
                getPlanos();
            })
            .catch(e => {
                console.log(e);
            });
    }

    function editPlanos() {
        setstateBt(true)

        const token = localStorage.getItem("token");
        api
            .put(
                "editarPlanos",
                {
                    nome: nomeEdit,
                    alunos: alunosEdit,
                    valor: valorEdit,
                    id: id,
                    planoAtual: planoatual
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
            .then((res) => {
                setedPlan(false)
                setstateBt(true)

                getPlanos();
            })
            .catch(e => {
                console.log(e);
            });
    }

    function deletePlanos() {
        const token = localStorage.getItem("token");
        setstateBt(true)

        api.put("/deletePlanos",
            {
                id: id,
            },
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(() => {
                setdelPlan(false)
                setstateBt(false)

                getPlanos();
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div>
            <div id="tagBarraPesquisa">
                <span className="barraDePesquisa">
                    <Form.Control
                        id="barraPesquisa"
                        value={filter}
                        onChange={event => setfilter(event.target.value)}
                        placeholder={'Pesquisar por Nome'}
                    />
                    <span id="iconPesquisa"  >
                        <BsSearch size='18px' />
                    </span>
                </span>
                <Button id="BotaoCadResp"
                    onClick={() => setcadPlan(true)}>Cadastrar Plano</Button>
            </div>
            <div id="fundoPlano">
                <Table
                    id="tabelaPadrao" borderless striped responsive="sm">
                    <thead>
                        <tr id="title-aluno-container">
                            <th>Nome</th>
                            <th>Nº de alunos</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {postData}
                    </tbody>
                </Table>
                <ReactPaginate
                    previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pagecount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                    forcePage={forceNew} />
            </div>
            <Modal show={isLoading} size='xl' centered animation>
            <Modal.Body>
                <ProgressBar animated now={100} />
            </Modal.Body>
        </Modal>
            <Modal show={cadPlan} onHide={() => setcadPlan(false)}>
                <Modal.Header id="modal-admin-header">
                    <Modal.Title>Cadastrar Plano</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Nome</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={nome}
                            onChange={event => setnome(event.target.value)}
                        />
                    </InputGroup>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Nº de alunos</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={alunos}
                            onChange={event => setalunos(event.target.value)}
                        />
                    </InputGroup>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Valor</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={valor}
                            onChange={event => setvalor(event.target.value)}
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer  >
                    <BtLoad onClick={() => cadastrarPlanos()} state={stateBt} title='Cadastrar' id="BotaoEditS" />
                    <Button
                        id="BotaoEditN"
                        onClick={() => { setedPlan(false) }}
                    >Não</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={edPlan} onHide={() => setedPlan(false)}>
                <Modal.Header id="modal-admin-header">
                    <Modal.Title>Cadastrar Plano</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Nome</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={nomeEdit}
                            onChange={event => setnomeEdit(event.target.value)}
                        />
                    </InputGroup>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Nº de alunos</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={alunosEdit}
                            onChange={event => setalunosEdit(event.target.value)}
                        />
                    </InputGroup>
                    <InputGroup id="editInput">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputColor">Valor</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            value={valorEdit}
                            onChange={event => setvalorEdit(event.target.value)}
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer  >
                    <BtLoad onClick={() => editPlanos()} state={stateBt} title='Atualizar' id="BotaoEditS" />
                    <Button
                        id="BotaoEditN"
                        onClick={() => { setedPlan(false) }}
                    >Não</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={delPlan} onHide={() => setdelPlan(false)} >
                <Modal.Header id="modalHeader">
                    <Modal.Title>Deseja Excluir a instituição? </Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">
                    <p>Se esse plano for deletado, todo os dados cadastrados nela serão excluídos de forma permanente e não poderão ser recuperados. </p>

                    <Alert variant='danger'>
                        <Form.Label htmlFor="text">Para deletar a instituição, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
                    </Alert>

                    <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />
                </Modal.Body>
                <Modal.Footer id="modalBody">
                    <BtLoad onClick={() => {
                        if (security === confirmSecurity) deletePlanos()
                        else {
                            setdelPlan(false)

                            setTimeout(() => { setdelPlan(true) }, 1000);
                        }
                    }} state={stateBt} title='Sim' id="BotaoEditS" />
                    <Button id="BotaoEditN" onClick={() => setdelPlan(false)} >Não</Button>
                </Modal.Footer>
            </Modal>
        </div >);
}

export default Planos;