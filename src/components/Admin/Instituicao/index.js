/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import './styles.css';
import { Link } from 'react-router-dom';
import { FaSchool, FaPhoneAlt, FaUser, FaUserCircle } from 'react-icons/fa';
import { TiDocumentText, TiLocation, TiArrowLeft } from 'react-icons/ti';
import { MdSchool, MdEmail } from 'react-icons/md';


export default function ListInst({ history }) {
    const [cep, setcep] = useState("");
    const [cnpj, setcnpj] = useState("");
    const [email, setemail] = useState("");
    const [nomeEmpresarial, setnomeEmpresarial] = useState("");
    const [nomeFantasia, setnomeFantasia] = useState("");
    const [telefone, settelefone] = useState("");
    const [telefoneg, settelefoneg] = useState("");
    const [nomeg, setnomeg] = useState("");
    const [cpf, setcpf] = useState("");
    const [emailg, setemailg] = useState("");


    useEffect(() => {
        variaveis(history.location.state.instituicao);
    }, []);

    function variaveis(instituicao) {
        setcep(instituicao.cep)
        setcnpj(instituicao.cnpj)
        setemail(instituicao.email)
        setnomeFantasia(instituicao.nomeFantasia)
        setnomeEmpresarial(instituicao.nomeEmpresarial)
        settelefone(instituicao.telefone)
        var gestor = instituicao.gestor;
        setemailg(gestor.email)
        setcpf(gestor.cpf)
        setnomeg(gestor.nome)
        settelefoneg(gestor.telefone)
    }


    return (
        <>
            <div id='fundo-I'>
                <Link id="linkCadINs" to="/ListInstituicoes">
                    <OverlayTrigger placement="right" overlay={<Tooltip>Retornar</Tooltip>}>
                        <TiArrowLeft size={30}></TiArrowLeft>
                    </OverlayTrigger>
                </Link>
                <div id="instiContainerPerfil">
                    <div id="header-ContaPerfilIn">
                        <h5>{nomeFantasia}</h5>
                        <FaSchool color={'gray'} size={30} style={{ marginBottom: 10 }} />
                    </div>
                    <div id="body-PerfilInsta">
                        <div id="conteudo-BodyPerfil">
                            <MdSchool color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Nome da Instituição: </strong>
                            <p>{nomeEmpresarial}</p>

                        </div>
                        <div id="conteudo-BodyPerfil">
                            <MdEmail color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Email:</strong>
                            <p>{email}</p>
                            
                        </div>
                        <div id="conteudo-BodyPerfil">
                            <FaPhoneAlt color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Telefone :</strong>
                            <p>{telefone}</p>
                            
                        </div>
                        <div id="conteudo-BodyPerfil">
                            <TiDocumentText color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>CNPJ: </strong>
                            <p>{cnpj}</p>
                            
                        </div >
                        <div id="conteudo-BodyPerfil">
                            <TiLocation color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>CEP :</strong>
                            <p>{cep}</p>
                            
                        </div>
                    </div>
                </div>
                <hr></hr>
                <div id="instiContainerPerfil">
                    <div id="header-ContaPerfilIn">
                        <h5>{nomeg}</h5>
                        <FaUserCircle color={'gray'} size={30} style={{ marginBottom: 10 }} />
                    </div>
                    <div id="body-PerfilInsta">
                        <div id="conteudo-BodyPerfil">
                            <FaUser color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Cargo: </strong>
                            <p>Gestor Administrador da Escola</p>
                        </div>
                        <div id="conteudo-BodyPerfil">
                            <MdEmail color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Email: </strong>
                            <p>{emailg}</p>
                            
                        </div>
                        <div id="conteudo-BodyPerfil">
                            <TiDocumentText color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>CPF: </strong>
                            <p>{cpf}</p>
                            
                        </div>
                        <div id="conteudo-BodyPerfil">
                            <FaPhoneAlt color={'black'} size={23} style={{ marginRight: 10 }} />
                            <strong>Telefone: </strong>
                            <p>{telefoneg}</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
