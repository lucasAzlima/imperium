import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { FaChevronCircleRight, FaTrash } from "react-icons/fa";
import {
  Button, Form, Table,
  OverlayTrigger, Tooltip, Modal,
  Alert
}
  from 'react-bootstrap'
import ReactPaginate from 'react-paginate';
import { BsSearch } from 'react-icons/bs';
import { TiArrowLeft } from 'react-icons/ti'
import { Styledh4 } from './styles.js'
import './styles.css'
export default function Disciplinas(props) {
  const [datavalue, setdatavalue] = useState([]);
  const [materias, setmaterias] = useState([]);
  const [show, setshow] = useState(false);
  const [filter, setfilter] = useState('');
  const [security, setsecurity] = useState('');
  const [nomedel, setnomedel] = useState('');
  const [idDel, setidDel] = useState('');
  const [idturma, setidturma] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const [offset, setoffset] = useState(0);
  const perPage = 5;
  const [turma, setturma] = useState('');
  const [pagecount, setpagecount] = useState(0);
  const history = useHistory();
  const [offsetOld, setoffsetOld] = useState(0);
  const [wrn, setwrn] = useState(false);

  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const filteredData = datavalue.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });

  useEffect(() => {
    setdatavalue(props.history.location.state.alunos);
    setmaterias(props.history.location.state.turma.materias);
    setturma(props.history.location.state.turma);
    setidturma(props.history.location.state.turma.id)
  }, [
    props.history.location.state.alunos, 
    props.history.location.state.turma.materias,
    props.history.location.state.turma.id,
    props.history.location.state.turma]);

  function ReceivedData() {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(pd =>
      <tr>
        <td id='bordaArreEsq'>{pd.nome}</td>
        <td id='bordaArreDir'>
          <OverlayTrigger placement="left" overlay={<Tooltip>Visualizar Notas</Tooltip>}>
            <Button onClick={() => history.push({ pathname: 'NotasAluno', state: { materias: materias, id: pd.id, aluno: datavalue, turma: turma } })} id="BotaoResp" >
              <FaChevronCircleRight size="20px" />
            </Button>
          </OverlayTrigger>

          <OverlayTrigger placement="right" overlay={<Tooltip>Excluir da turma</Tooltip>}>
            <Button id="BotaoResp" >
              <FaTrash size="20px" onClick={() => { setidDel(pd.id); setnomedel(pd.nome); setshow(true) }} />
            </Button>
          </OverlayTrigger>
        </td>
      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    return (postData1);
  }

  useEffect(() => {
    if (filter !== '') {
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  }, [filter, setoffset, offsetOld, forceOld]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if (filter === '') {
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  function deletealuno() {
    var alns = datavalue.filter(Doc => Doc.id !== idDel);
    var alunoSelect = datavalue.filter(Doc => Doc.id === idDel);
    materias.forEach(materia => {
      materia.alunos = materia.alunos.filter(Doc => Doc.id !== idDel);
    })

    var trm = { id: '', ano: '', nivel: '', turno: '' }
    const token = localStorage.getItem("token");
    api
      .put(
        "delAlunoTurma",
        {
          materias: materias,
          alunos: alns,
          id: idturma,
          turma: trm,
          alunosadd: alunoSelect
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        history.push('turma')
      })
      .catch(e => {
        console.log(e);
      });
  }


  return (
    <>
      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Nome ou Turma'}
          />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>
        <Button id="BotaoCadResp"
          onClick={() => history.push({ pathname: '/AddAluno', state: { alunos: datavalue, turma: props.history.location.state.turma } })}>Cadastrar Aluno</Button>
      </div>

      <div id='listagem'>
        <div id="listarALunoturma-title">
          <OverlayTrigger placement="right" overlay={<Tooltip>Retornar</Tooltip>}>
            <Button id="BotaoResp" onClick={() => history.push("/turma")}>
              <TiArrowLeft size="35" color={'gray'}></TiArrowLeft>
            </Button>
          </OverlayTrigger>
          <p>Alunos da Turma</p>
        </div>
        <Table id="tabelaPadrao" borderless striped responsive="sm">
          <thead>
            <tr id="title-aluno-container">
              <th>Nome</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            {<ReceivedData/>}
          </tbody>
        </Table>

        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          forcePage={forceNew} />


      </div>

      <Modal show={show} onHide={() => setshow(false)} centered size="lg">
        <Modal.Header id="modalHeader">
          <Modal.Title >Deletar aluno</Modal.Title>
        </Modal.Header>

        <Modal.Body id="modalBody">
          <p>Se esse aluno for deletado, todo o histórico e notas cadastrados nele serão excluídos de forma permanente e não poderão ser recuperados. </p>
          <Alert variant='danger'>
            <Form.Label htmlFor="text">Para deletar o aluno, por favor digite <strong>{nomedel}</strong> para confirmar</Form.Label>
          </Alert>
          {(wrn) ? <p style={{ color: 'red' }}>falha, verifique se digitou corretamente!</p> : <div />}
          <Form.Control

            value={security}
            onChange={event => { setsecurity(event.target.value); }} />

        </Modal.Body>
        <Modal.Footer id="modalBody">
          <Styledh4>Deseja salvar a alteração?</Styledh4>
          <Button id="BotaoEditS" onClick={() => {
            if (security === nomedel) { setwrn(false); deletealuno() }
            else {
              setshow(false)
              setTimeout(() => { setwrn(true); setshow(true) }, 1000);
            }
          }}>Sim</Button>
          <Button id="BotaoEditN" onClick={() => setshow(false)} >Não</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

