import styled from 'styled-components';

export const Container = styled.div`
    width:100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color:#e5e5e5;

    form {
        display:flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 415px;
        height: 400px;
        box-shadow: 0 0 100px rgba(0, 0, 0, 0.2);
        border-radius: 8px;
        background: #fff;

        h1{
            margin-top: 34px;
            margin-bottom:68px;
        }

        .input {
            display: flex;
            align-items:center;
            width: 100%;
            max-width:360px;
            background:white;
            border-radius: 10px;
            height: 50px;
            padding-left:34px;
            border: 2px solid;
            margin-bottom: 10px;
            margin-top: 10px;

            input {
                margin-left: 16px;
                max-width:80%;
            }
        }

        .forgot{
            font-size:14px;
            max-width: 360px;
            padding-left: 220px;
            margin-bottom:25px;
        }

        .acessButtom {
            border: 2px solid;
            height: 50px;
            max-width: 360px;
            width: 100%;
            border-radius: 10px;
            padding:10px 108px;
            background-color: midnight#323bb4;
            margin-bottom:10px;


            a{
            text-decoration: none;
            color: #fff;
            font-size:18px;
            }
        }

        .cadastro{
            font-size:14px;
            max-width: 415px;
            padding: 0 52px;
            margin-bottom: 60px;
            

            a{
                text-align: right; 
            }

        }
        
    }
`;
