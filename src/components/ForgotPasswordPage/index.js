import React, { useState } from 'react';
import { Container } from './styles';
import { Form, Button } from 'react-bootstrap'
import { MdMail } from 'react-icons/md';
import './styles.css'
import firebase from "../../services/firebase";
import { useHistory } from 'react-router-dom'


export default function LoginPage() {
    const [emailrc, setEmailrc] = useState("");
    const history = useHistory();

    function RecuperarSenha() {
        firebase.auth().sendPasswordResetEmail(emailrc).then(function () {
            history.push('/login')
        }).catch(function (error) {
            console.log(error)
        });
    }

    return (
        <Container id='fundofp'>
            <form>
                <h1> iMPERIUM Educ</h1>
                <div className="text-fp">Digite seu email para recuperar sua senha</div>
                <div id='box-input-forgot'>
                    <MdMail id='icon-forgot'/>
                    <Form.Control
                        type="email"
                        placeholder="Email"
                        id="input-forgot"
                        value={emailrc}
                        onChange={e => setEmailrc(e.target.value)} />
                </div>
                <Button onClick={() => RecuperarSenha()}>Recuperar Acesso</Button>
                <Button onClick={() => history.push('/login')}>Fazer login</Button>
            </form>


        </Container>
    );
}