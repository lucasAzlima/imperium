import React, { useState } from "react";
import { Form } from "react-bootstrap";
import firebase from "../../services/firebase";
import api from "../../services/api";
import BtLoad from "../ButtonLoading"
import { MdEmail, MdLock } from "react-icons/md";

import './style.css'

export default function Login({ history }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [stateBt, setstateBt] = useState(false);

  async function handleSubmit(event) {
    setstateBt(true)
    event.preventDefault();

    firebase
      .auth()
      .setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(function () {
        firebase.auth().signInWithEmailAndPassword(email, password)
          .then(() => {
            firebase
              .auth()
              .currentUser.getIdToken()
              .then(
                function (data) {
                  localStorage.setItem("token", data);
                  api
                    .get("/verificarAdmin", {
                      headers: { Authorization: `Bearer ${String(data)}` }
                    })
                    .then(res => {

                      const { auth, instituicao, admin, gestor, ativo } = res.data;

                      if (auth && ativo) {
                        localStorage.setItem("instituicao", instituicao);
                        localStorage.setItem("login", JSON.stringify({ email: email, senha: password }));
                        setstateBt(false)
                        localStorage.setItem("Permissions",
                          JSON.stringify({ admin: admin, gestor: gestor }));
                        if ((admin === true) && (gestor === true)) {
                          setstateBt(false);
                          history.push("/DashAdmin")
                        };
                        if ((admin === false) && (gestor === true)) DataInst(instituicao);

                      } else {
                        alert("Erro de autenticação, usuário não cadastrado ou em foi desativado")
                        console.log("Erro de autenticação, usuário não cadastrado");
                      }
                    })
                    .catch(e => {
                      console.log(e);
                      console.log(
                        "Erro de autenticação, verifique seu email e senha"
                      );
                    });
                });
          }).catch(() => {
            setstateBt(false);
            alert(`Houve um problema ao se autenticar, por favor, verificar se os dados estão corretos.`)
          });
      });
  }

  function DataInst(instituicao) {
    const token = localStorage.getItem("token");
    api
      .put("/getDataInst",
        {
          instituicao: instituicao
        }, {
        headers: { Authorization: `Bearer ${token}` }

      })
      .then(res => {
        setstateBt(false)
        localStorage.setItem("EstrutCurric", res.data.escola.EC);
        history.push("/dashboard");
      })
      .catch(e => {
        console.log(e);
      });
  }


  return (
    <div id='fundologin'>
      <span id='logo-login'>iMPERIUM Educ</span>
      <Form onSubmit={handleSubmit}>
        <div id='box-input-login'>
          <MdEmail id='icon-login' />
          <Form.Control
            type="email"
            id="input-login"
            placeholder="Seu e-mail"
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </div>
        <div id='box-input-login'>
          <MdLock id='icon-login' />
          <Form.Control
            type="password"
            placeholder="Senha"
            id="input-login"
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </div>

        <span id='recvr-login' onClick={() => history.push('/forgotpassword')}>Esqueci a senha</span>
        <div id='button-login'>
          <BtLoad onClick={handleSubmit} state={stateBt} title='Acessar' id='bt-login' />
          <span id='contato-login'>Ainda não é associado?<span onClick={() => history.push('/form')}> Entre em contato</span></span>
        </div></Form>
    </div>
  );
}
