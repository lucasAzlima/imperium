import React, { useEffect, useState } from "react";
import api from "../../services/api";
import './style.css';
import { Modal, Button, Form } from 'react-bootstrap'
import Popup from "reactjs-popup";
import { useHistory } from 'react-router-dom'

export default function ResponsavelView(props) {
  const [responsaveis, setresponsaveis] = useState([])
  const [nomecp, setnomecp] = useState('')
  const [nomedl, setnomedl] = useState('')
  const [parentcp, setparentcp] = useState('')
  const [tel1cp, settel1cp] = useState('')
  const [tel2cp, settel2cp] = useState('')
  const [idadecp, setidadecp] = useState('')
  const [instituicao, setinstituicao] = useState('')
  const [aluno, setaluno] = useState('')
  const [editor, seteditor] = useState(false)
  const [close, setclose] = useState(false)
  const history = useHistory();

  const [nwnome, setnwnome] = useState('')
  const [nwparent, setnwparent] = useState('')
  const [nwtel1, setnwtel1] = useState('')
  const [nwtel2, setnwtel2] = useState('')
  const [nwidade, setnwidade] = useState('')

  useEffect(() => {
    const token = localStorage.getItem("token");
    api
      .get("/getNotificacoes", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setinstituicao(res.data.id);
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
    setresponsaveis(props.history.location.state.data.responsaveis);
    setaluno(props.history.location.state.data.id)

  },[
    props.history.location.state.data.responsaveis,
    props.history.location.state.data.id
  ])

  function copia(resp) {
    setnomecp(resp.nome)
    setnomedl(resp.nome)
    setparentcp(resp.parent)
    settel1cp(resp.tel1)
    settel2cp(resp.tel2)
    setidadecp(resp.idade)
  }

  function AddResp(){
    responsaveis.push({ nome: nwnome, idade: nwidade, tel1: nwtel1, tel2: nwtel2, parent: nwparent })
    updateResp()
  }

  function updateResp() {
    var resp = responsaveis.slice();
    resp = resp.filter(Doc => Doc.nome !== nomedl);
    resp.push({ nome: nomecp, idade: idadecp, tel1: tel1cp, tel2: tel2cp, parent: parentcp })
    setresponsaveis(resp)
    AtualizarResp(resp)    
  }

  function deleteResp() {
    var resp = responsaveis.filter(Doc => Doc.nome !== nomedl);
    setresponsaveis(resp);
    AtualizarResp(resp);
  }

  function AtualizarResp(resp) {
    const token = localStorage.getItem("token");
    api.put("editResp",
      {
        responsaveis: resp,
        instituicao: instituicao,
        aluno: aluno
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setclose(false)
        history.push("/VerAluno")
      })
      .catch(e => {
        console.log(e);
      });
  }
  return (
    <div className="container-responsavel-view">
      {(responsaveis.length < 2) ? <Popup className="popup-style"
        trigger={
          <Button id='resp-bt-cad'> Cadastrar </Button>}
        modal
        closeOnDocumentClick>
        <div className="header-form-responsavel">
          <h2>
            Cadastro de Responsável
          </h2>
        </div>
        
        <Form className="form-responsavel">
          <Form.Group>
            <Form.Label htmlFor="name">Nome</Form.Label>
            <Form.Control
              type="text"
              id="title"
              value={nwnome}
              onChange={event => setnwnome(event.target.value)}
            />
            <br />
            <Form.Label htmlFor="name">Parentesco</Form.Label>
            <Form.Control
              type="text"
              id="link"
              value={nwparent}
              onChange={event => setnwparent(event.target.value)}
            />
            <br />
            <Form.Label htmlFor="name">Telefone 1</Form.Label>
            <Form.Control
              type="number"
              id="link"
              value={nwtel1}
              onChange={event => setnwtel1(event.target.value)}
            />
            <br />
            <Form.Label htmlFor="name">Telefone 2</Form.Label>
            <Form.Control
              type="number"
              id="link"
              value={nwtel2}
              onChange={event => setnwtel2(event.target.value)}
            />
            <br /> <Form.Label htmlFor="name">Idade</Form.Label>
            <Form.Control
              type="number"
              id="link"
              value={nwidade}
              onChange={event => setnwidade(event.target.value)}
            />
            <br />
            <Button id="BotaoFormResp" onClick={() => AddResp()}>Adicionar</Button>
          </Form.Group>
        </Form>
        
      </Popup> : <div />
       
      }
      {responsaveis.map((resp, index) =>
        <div id="elements-responsavelView">
            <div id="elements-responsavelView-title">
              <p>Nome: <text>{resp.nome}</text></p>
              <p>Relação: <text>{resp.parent}</text></p>
              <p>Telefone 1: <text>{resp.tel1}</text></p>
              <p>Telefone 2: <text>{resp.tel2}</text></p>
              <p>Idade: <text>{resp.idade}</text></p>
            </div>
            <div id='responsavelView-bt'>

            <Popup
              trigger={
                <Button id='resp-bt-ed' onClickCapture={() => copia(resp)}
                  style={{ marginRight: 5 }}> Editar </Button>}
              modal
              open={close}
              onClose={() =>setclose(false)}
              closeOnDocumentClick>
              <span>
                <Form.Label htmlFor="name">nome</Form.Label>
                <Form.Control
                  type="text"
                  id="title"
                  value={nomecp}
                  onChange={event => setnomecp(event.target.value)}
                />
                <br />
                <Form.Label htmlFor="name">Telefone 1</Form.Label>
                <Form.Control
                  type="text"
                  id="link"
                  value={parentcp}
                  onChange={event => setparentcp(event.target.value)}
                />
                <br />
                <Form.Label htmlFor="name">Telefone 1</Form.Label>
                <Form.Control
                  type="text"
                  id="link"
                  value={tel1cp}
                  onChange={event => settel1cp(event.target.value)}
                />
                <br />
                <Form.Label htmlFor="name">Telefone 2</Form.Label>
                <Form.Control
                  type="text"
                  id="link"
                  value={tel2cp}
                  onChange={event => settel2cp(event.target.value)}
                />
                <br /> <Form.Label htmlFor="name">Idade</Form.Label>
                <Form.Control
                  type="text"
                  id="link"
                  value={idadecp}
                  onChange={event => setidadecp(event.target.value)}
                />
                <br />
                <Button BotaoFormResp onClick={() => updateResp()}>Atualizar</Button>
              </span>
            </Popup>

            <button id='resp-bt-ex' onClick={() => { setnomedl(resp.nome); seteditor(true) }}>Excluir</button>
          </div>
        </div>
      )}
      <Modal show={editor} onHide={() => seteditor(false)}>
        <Modal.Header>
          <Modal.Title>Confirmação do Administrador</Modal.Title>
        </Modal.Header>
        <Modal.Body>Você está tentando excluir um Responsável!! Deseja confirmar a ação ?</Modal.Body>
        <Modal.Footer>
        <Button variant="primary" onClick={() => {deleteResp(); seteditor(false)}}>Sim</Button>
          <Button variant="secondary" onClick={() => seteditor(false)}>Não</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
