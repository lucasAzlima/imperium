import React,{ useState, useEffect} from "react";
import api from "../../services/api";
import { Card, FormControl, InputGroup, Button, Table, OverlayTrigger, Tooltip, Accordion, Modal, Alert, Form, ProgressBar } from 'react-bootstrap'

import {  FaPencilAlt, FaRegUserCircle, FaTrash } from "react-icons/fa";
import { useHistory } from "react-router-dom";
import "./style.css";
import { Styledh4 } from './styles.js';
import BtLoad from "../ButtonLoading"

export default function Professor()  {
  const [datavalue, setdatavalue] = useState([]);
  const [turmas, setturmas] = useState([]);
  const [nome, setnome] = useState("");
  const [telefone, settelefone] = useState("");
  const [email, setemail] = useState("");
  const [matricula, setmatricula] = useState("");
  const [nomeEdit, setnomeEdit] = useState("");
  const [matriculaEdit, setmatriculaEdit] = useState("");
  const [telefoneEdit, settelefoneEdit] = useState("");
  const [emailEdit, setemailEdit] = useState("");
  const [idEdit, setIdEdit] = useState('');
  const [atualizar, setAtualizar] = useState(false);
  const history = useHistory();
  const [security, setsecurity] = useState('');
  const [confirmSecurity, setconfirmSecurity] = useState('');
  const [excluirProf, setexcluirProf] = useState(false);
  const [isLoading, setisLoading] = useState(true);
  const [stateBt, setstateBt] = useState(false);
  

  
  useEffect(() => {
    Getprof();
    Turmas();
  },[]);

  async function Getprof () {
    const token = localStorage.getItem("token");
    await api
      .get("/getProfessor", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        if (res.data.length !== 0) setdatavalue(res.data);
        else {
          setdatavalue([{
                nome: "Nenhum dado cadastrado",
                matricula: "-",
                email: "-",
                telefone: "-"
            }])
        }
            
        setisLoading(false);
        
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function cadastProf() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    const MAT = Number(matricula);
    api
      .post(
        "cadastrarProfessor",
        {
          nome: nome,
          matricula: MAT,
          telefone: telefone,
          email: email
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setnome('');
        setmatricula('');
        settelefone('')
        setemail('')
        Getprof();
        setstateBt(false)
      })
      .catch(e => {
        console.log(e);
      });
  }

  function editProf(event){
    const token = localStorage.getItem("token");
    const MAT = Number(matriculaEdit);
    api
      .put(
        "editarProf",
        {
          nome: nomeEdit,
          matricula: MAT,
          telefone: telefoneEdit,
          email: emailEdit,
          id:idEdit
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setAtualizar(false);
        Getprof();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function deleteP (){ 
    var ModfTurmas = []
    turmas.forEach( (trm, idx) =>{

      trm.materias.forEach(mat =>{
        if(mat.idProf === idEdit){
          mat.idProf = '';
          mat.professor ='';
        }
      })
    ModfTurmas.push({id:trm.id, materias:trm.materias})
    })
    const token = localStorage.getItem("token");
    api.put("deleteProf",
      {
        id: idEdit,
        modificacoes:ModfTurmas
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setexcluirProf(false)
        Getprof();
      })
      .catch(e => {
        console.log(e);
      });
    
  }


  function Turmas() {
    const token = localStorage.getItem("token");
    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setturmas(res.data)
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });

      
  }


  function copia (event) {
    var data = event.split("|");
    setnomeEdit(data[0]);
    setmatriculaEdit(data[1]);
    setemailEdit(data[2]);
    settelefoneEdit(data[3]);
  }

    return (
      <>
        <div id='listagem'>
        <Table id="tabelaProfessor" borderless striped responsive="sm" >
                    <thead >
                      <tr >
                        <th id="nome">Nome</th>
                        <th >Matrícula</th>
                        <th >Email</th>
                        <th >Telefone</th>
                        <th id="acao">Ações</th>
                      </tr>
                    </thead>
                    <tbody >
          
              {datavalue.map((data, _id) => {
                return (
                    <tr disabled key={_id}>
                      <td id='bordaArreEsq' >{data.nome}</td>
                      <td>{data.matricula}</td>
                      <td>{data.email}</td>
                      <td>{data.telefone}</td>
                      <td id='bordaArreDir'>

                      <OverlayTrigger placement="left" overlay={<Tooltip>Visualizar</Tooltip>}>
                          <Button disabled={(data.matricula === '-')} id="BotaoResp" >
                            <FaRegUserCircle size={20} onClick={() => history.push({ pathname: "/Perfil", state: { data: data } }) }/>
                          </Button>
                      </OverlayTrigger>

                      <OverlayTrigger key="top" placement="top" overlay={<Tooltip>Editar</Tooltip>}>
                      <Button disabled={(data.matricula === '-')} id="BotaoResp">
                        <FaPencilAlt size={20} onClick={event =>{setIdEdit(data.id); copia(`${data.nome}|${data.matricula}|${data.email}|${data.telefone}`);setAtualizar(true)}} /> 
                      </Button>
                     </OverlayTrigger>
                        
                        <OverlayTrigger placement="right" overlay={<Tooltip>Excluir</Tooltip>}>
                          <Button disabled={(data.matricula === '-')} id="BotaoResp2" >
                            <FaTrash  size={20} value={data.id} onClick={() => {setsecurity(data.nome);setIdEdit(data.id);setexcluirProf(true)}}/>
                          </Button>
                        </OverlayTrigger>
                      </td>
                    </tr>
                );
                
              })
              }
            </tbody>
              </Table>

              <Accordion>
                <Card >
                  <Card.Header id="cardCabecalho" >
                    <Accordion.Toggle as={Button} id="BotaoAccordion" variant="link" eventKey="0">
                      Cadastrar novo professor(a)
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                          <InputGroup.Text >Nome</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          value={nome}
                          onChange={event => setnome(event.target.value) }
                        />
                        <InputGroup.Prepend>
                          <InputGroup.Text >Matrícula</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          value={matricula}
                          onChange={event => setmatricula( event.target.value )}
                        />
                      </InputGroup>
                      
                      <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                          <InputGroup.Text >Email</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          value={email}
                          onChange={event => setemail(event.target.value) }
                        />
                        <InputGroup.Prepend>
                          <InputGroup.Text >Telefone</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          value={telefone}
                          onChange={event => settelefone( event.target.value )}
                        />
                      </InputGroup>
                      
                      <BtLoad  onClick={cadastProf} state={stateBt} title='Cadastrar' id="BotaoAccordion"/>

                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            
          
        </div>

        <Modal show={atualizar} onHide={() => setAtualizar(false)}  centered size="lg">
                <Modal.Header id="modalHeader">
                  <Modal.Title >Edição de Disciplina</Modal.Title>
                </Modal.Header>
                <Modal.Body id="modalBody">
                <span>
                  <InputGroup id="editInput">
                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputColor">Nome</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      value={nomeEdit}
                      onChange={event => setnomeEdit(event.target.value )}
                    />
                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputColor">Matrícula</InputGroup.Text>
                    </InputGroup.Prepend>
            
                    <FormControl
                      
                      
                      value={matriculaEdit}
                      onChange={event => setmatriculaEdit(event.target.value)}
                    />

                    

                  </InputGroup>
                  <InputGroup id="editInput">
                  <InputGroup.Prepend>
                      <InputGroup.Text id="inputColor">Email</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      
                      
                      value={emailEdit}
                      onChange={event => setemailEdit(event.target.value )}
                    />

                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputColor">Telefone</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      
                      
                      value={telefoneEdit}
                      onChange={event => settelefoneEdit(event.target.value )}
                    />
                  </InputGroup>
                </span>

                </Modal.Body>
                <Modal.Footer id="modalBody">
                  <Styledh4>Deseja salvar a alteração?</Styledh4>
                  <Button id="BotaoEditS" onClick={event => editProf(event.target.value)} >Sim</Button>
                  
                  
                  
                  <Button id="BotaoEditN" onClick={() => setAtualizar(false)} >Não</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={excluirProf} onHide={() => setexcluirProf(false)} >
          <Modal.Header id="modalHeader">
            <Modal.Title>Deseja Excluir o professor? </Modal.Title>
          </Modal.Header>
          <Modal.Body id="modalBody">

            <p>Se esse professor for deletado, todos os dados cadastrados serão excluídos de forma permanente e não poderão ser recuperados. </p>

            <Alert variant='danger'>
              <Form.Label htmlFor="text">Para deletar o professor, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
            </Alert>

            <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />


          </Modal.Body>
          <Modal.Footer id="modalBody">
            <Button id="BotaoEditS" onClick={event => {
              if (security === confirmSecurity) deleteP(event.target.value)
              else {
                setexcluirProf(false)

                setTimeout(() => {  setexcluirProf(true) }, 1000);
              }
            }}>Deletar</Button>
            <Button id="BotaoEditN" onClick={() => setexcluirProf(false)} >Descartar</Button>
          </Modal.Footer>
        </Modal>

        <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
        </Modal>
      </>
    );
          
  }
