import React, { useState, useEffect } from "react";
import './style.css'
import { Card, Form, Button, Modal } from 'react-bootstrap'
import { FaUpload } from 'react-icons/fa';
import { useHistory } from 'react-router-dom'
import api from "../../services/api";
import firebase from "../../services/firebase";
import BtLoad from "../ButtonLoading"
import Destinatarios from "../Destinatarios"

export default function EditarNotificacoes({data}) {
  const [file, setfile] = useState(undefined);
  const [filename, setfilename] = useState("");
  const [titulo, settitulo] = useState("");
  const [corpo, setcorpo] = useState("");
  const [idEdit, setidEdit] = useState("");
  const [destin, setdestin] = useState(false);
  const history = useHistory();
  const [alunos, setalunos] = useState([]);
  const [turmas, setturmas] = useState([]);
  const [keys, setkeys] = useState([]);
  const [selectView, setselectView] = useState([]);
  const [stateBt, setstateBt] = useState(false);
  const [show, setshow] = useState(false);
  const [Porcentagem, setPorcentagem] = useState(0);

  useEffect(() => {
    const {titulo, corpo, key, filename, id} = data;
    setkeys(key)
    settitulo(titulo)
    setfilename(filename)
    setcorpo(corpo)
    setidEdit(id)


    const token = localStorage.getItem("token");
    api
      .get("/getAlunos", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = []
        res.data.alunos.map(data =>{
          if(key.includes(data.id))selectView.push({type: 'Aluno', nome: data.nome, id: data.id})
          return trm.push({
            nome: data.nome,
            id: data.id,
            state: (key.includes(data.id))?true:false,
            turma: data.turma
          })});
        
        console.log(trm)

        setalunos(trm);
      })
      .catch(e => {
        console.log(e);
      });

      api
        .get("/getTurmas", {
          headers: { Authorization: `Bearer ${token}` }
        })
        .then(res => {
          var trm = []
          res.data.map(data =>{
            if(key.includes(data.id))selectView.push({type: 'Turma', nome: data.ano, id: data.id})
            return trm.push({
              ano: data.ano,
              id: data.id,
              state: (key.includes(data.id))?true:false,
              turno: data.turno,
              sala: data.sala
            })});
          setturmas(trm);
        })
        .catch(e => {
          console.log(e);
        });
      
  }, [ data, selectView])

  function fileinput(file) {
    if (file.target.files[0] !== undefined)
      setfilename(file.target.files[0].name)
    setfile(file.target.files[0])
  }

  function cadasnotificacao(event) {
    event.preventDefault();
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarNotificacoes",
        {
          titulo: titulo,
          corpo: corpo,
          key: keys,
          filename: filename,
          id: idEdit,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        if (file !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(file, { contentType: 'application/pdf', })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) {
                setTimeout(() => { setshow(false); history.push("/Comunicados"); }, 1000);
              }
            });
          setfile("");
        }
        settitulo("");
        setcorpo("");
        setkeys([]);
        if (file === undefined) history.push("/Comunicados");
      })
      .catch(e => {
        console.log(e);
      });
  }

  return <div id='cadNotificacao'>
    <Card id='CardcadNot'>
      <Card.Header id="modalHeader"><Card.Title id='CardcadNot-title'>Cadastrar Comunidado</Card.Title></Card.Header>
      <Card.Body>
        <Form>
          <Form.Group>
            <Form.Label>Título do Comunicado</Form.Label>
            <Form.Control
              value={titulo}
              onChange={(event) => settitulo(event.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Corpo do Comunicado</Form.Label>
            <Form.Control
              as='textarea'
              value={corpo}
              onChange={(event) => setcorpo(event.target.value)}
            />
          </Form.Group>
          <Form.Row id='imput-button'>
            <Form.Group>
              <Form.Label>Enviar arquivo(pdf)</Form.Label>
              <div id='imput-group'>
                <label for="container-cadnotificação-input" id="style-upload-cadnotificação">
                  Upload do Arquivo
                </label>
                <FaUpload style={{ color: 'white' }} />
                <input id="container-cadnotificação-input" type="file" onChange={(event) => fileinput(event)}></input>
              </div>
            </Form.Group>
            <Button onClick={() => setdestin(true)}>Selecionar Destinatários</Button>
          </Form.Row>
        </Form>
      </Card.Body>
      <Card.Footer>
        <BtLoad
          onClick={cadasnotificacao}
          state={stateBt}
          title='Cadastrar'
          id="button-cadastro-notificação"
        />
      </Card.Footer>
    </Card>
    <Modal show={destin} onHide={() => setdestin(false)} id='ModalDist' size='xl'>
      <Modal.Header closeButton id="modalHeader">
        <Modal.Title >Selecionar destinatários</Modal.Title>
      </Modal.Header>
      <Modal.Body id="modalBody">
        <Destinatarios
          alunos={alunos}
          turmas={turmas}
          keys={(event) => setkeys(event)}
          selecionados={keys}
          setselecionados={(event) => setkeys(event)}
          selectView={selectView}
          setselectView={(event) => setselectView(event)}
          closeModal={() => {
            setdestin(false);
            setselectView([{ type: 'Instituição', nome: 'Toda a nstituição', id: '' }]);
          }}
        />
      </Modal.Body>
    </Modal>
    <Modal show={show}>
      <Modal.Header id="modalHeader">
        <Modal.Title>
          <p id="container-cadnotificação-label">Upload de arquivo</p></Modal.Title></Modal.Header>
      <Modal.Body>
        Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
    </Modal>
  </div>;
}
