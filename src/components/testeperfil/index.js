import React from 'react';
import { Button, Tab, Tabs } from 'react-bootstrap'
import Perfil from "../PerfilAluno"
import Responsaveis from "../ResponsavelView"
import Comunicados from "../ComunicadoAluno"

export default function Example ( props){

    return (
      <Tabs defaultActiveKey="Perfil" id="uncontrolled-tab-example">
  <Tab eventKey="Perfil" title="Perfil">
    <Perfil history={props.history}/>
  </Tab>
  <Tab eventKey="Resp" title="Responáceis">
    <Responsaveis history={props.history}/>
  </Tab>
  <Tab eventKey="Comunidados" title="Comunidados">
    <Comunicados history={props.history}/>
  </Tab>
</Tabs>
    );
  }
