import React, { useState, useEffect } from "react";
import api from "../../services/api";
import './styles.css'

import { Card, Table, FormControl, InputGroup, Button, Modal,Accordion, ProgressBar  } from 'react-bootstrap'
import Popup from "reactjs-popup";
import BtLoad from "../ButtonLoading"

export default function Cameras() {
  const [cameras, setcameras] = useState([]);
  const [nomeEdit, setnomeEdit] = useState("");
  const [linkEdit, setlinkEdit] = useState("");
  const [nome, setnome] = useState("");
  const [show, setshow] = useState(false);
  const [link, setlink] = useState("");
  const [message, setmessage] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [stateBt, setstateBt] = useState(false);
  

  useEffect(() => {
    Getcamera();
  }, []);

  function Getcamera() {
    const token = localStorage.getItem("token");

    api
      .get("/getCameras", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        if (res.data.length !== 0)
          setcameras(res.data);
        else
          setcameras([{
                nome: "Nenhum dado cadastrado",
                link: "-"
            }])
        setisLoading(false);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function cadasCamera() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarCameras",
        {
          nome: nome,
          link: link
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
        setcameras([]);
        setlink('');
        setnome('');
        Getcamera();
        setstateBt(false)
      })
      .catch(e => {
        console.log(e);
      });
  }

  function editCamera(event) {
    
    const token = localStorage.getItem("token");
    var cameraSet = cameras.slice();
    cameraSet = cameraSet.filter(obj => obj.nome !== event)
    cameraSet.push({ nome: nomeEdit, link:linkEdit });
    api
      .put(
        "editCameras",
        {

          cameras: cameraSet
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
        setcameras([]);
        setlinkEdit('');
        setnomeEdit('');

        Getcamera();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function deleteCamera(event) {
    const token = localStorage.getItem("token");
    api.put("/deleteCameras",
      {
        objDel: event,
        cameras: cameras
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
        Getcamera();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function copia(event) {
    var data = event.split("|");
    setlinkEdit(data[1]);
    setnomeEdit(data[0]);
  }

  return (
    <>

      <div id='listagem'>
        <Table id="tabelaCameras" borderless striped responsive='sm'>
              <thead >
                <tr >
                  <th >Câmeras</th>
                  <th>Link</th>
                  <th >Ações</th>
                  
                </tr>
              </thead>
              <tbody >
          
            {cameras.map(data => {
              return (
                <>
                  <tr >
                    <td id='bordaArreEsq' >{data.nome}</td>
                    <td id="card-text-limite-notificação"><a href={data.link}>{data.link}</a></td>
                    <td id='bordaArreDir2'>
                      <Popup
                        trigger={<Button id="button" disabled={(data.link === '-')} variant="outline-primary"
                          value={`${data.nome}|${data.link}`}
                          
                          onClickCapture={event => copia(event.target.value)}
                          style={{ marginRight: 5 }}> Editar </Button>}
                        modal
                        closeOnDocumentClick
                        >
                        <span>
                          <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                              <InputGroup.Text id="basic-addon1">Nome</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                              
                              aria-label="Nome"
                              aria-describedby="basic-addon1"
                              value={nomeEdit}
                              onChange={event => setnomeEdit(event.target.value)}
                            />
                            <InputGroup.Prepend>
                              <InputGroup.Text id="basic-addon1">Link</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                              
                              aria-label="Nome"
                              aria-describedby="basic-addon1"
                              value={linkEdit}
                              onChange={event => setlinkEdit(event.target.value)}
                            />
                          </InputGroup>
                          <Button id="BotaoAccordion" onClick={event => editCamera(data.nome)}>Atualizar</Button>
                        </span>
                      </Popup>
                      <Button disabled={(data.link === '-')}  variant="outline-primary" id="button2" value={data.nome} onClick={event => deleteCamera(event.target.value)}>Excluir</Button>
                    </td>
                  </tr>
                </>
              );
            })
            }
            </tbody>
          </Table>
          

        {/* Cadastro de cameras */}
        <Accordion >
          <Card >
            <Card.Header id="cardCabecalho">
              <Accordion.Toggle as={Button} id="BotaoAccordion" variant="link" eventKey="0">
                Cadastrar nova Câmera
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <InputGroup className="mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">Nome</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    
                    aria-label="Nome"
                    aria-describedby="basic-addon1"
                    value={nome}
                    onChange={event => setnome(event.target.value)}
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">Link</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    
                    aria-label="Nome"
                    aria-describedby="basic-addon1"
                    value={link}
                    onChange={event => setlink(event.target.value)}
                  />
                </InputGroup>
                
                <BtLoad  onClick={cadasCamera} state={stateBt} title='Cadastrar' id="BotaoAccordion"/>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>

        <Modal show={show}>
          <Modal.Body>
            <Modal.Title>
              <p style={{
                fontSize: 15, display: 'flex',
                justifyContent: 'center', alignItems: 'center'
              }}>{message}</p></Modal.Title>
          </Modal.Body>
        </Modal>
      </div>

      <Modal show={isLoading} size='lg' centered animation>
        <Modal.Body>
            <ProgressBar animated now={100} />
        </Modal.Body>
      </Modal>
    </>
  );
}
