import React, { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Link} from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi'
import firebase from "../../services/firebase";
import "./styles.css";
import api from "../../services/api";
import BtLoad from "../ButtonLoading"
const firestore = firebase.firestore();

export default function Register(props) {
  const [member, setmember] = useState(false);

  function Render() {
    
    switch (member) {
      
      case true:
        return (
          <>
            <div id='FormResp'>
              <Logado history={props.history}/>
              
            </div>
          </>
        );

      case false:
        return (
          <>
            <div id='FormResp'>
              <NLogado history={props.history} />
              <Button id="BotaoSemEstilo"  onClick={() => setmember(true)}> Já sou membro </Button>
            </div>
          </>
        )
      default:
        break;
    }
  }
  return (
    <>
      <Form >
        <Render />
      </Form>
    </>
  );
}

function NLogado(props) {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [cpf, setcpf] = useState("");
  const [address, setaddress] = useState("");
  const [password, setPassword] = useState("");
  const [stateBt, setstateBt] = useState(false);

  function cadastResp() {
    setstateBt(true)

    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarResponsavel",
        {
          email: email,
          endereco: address,
          cpf: cpf,
          nome: name,
          senha: password
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        sendEmail();
        props.history.push({
          pathname: '/alunoDependente',
          state: { instituicao: localStorage.getItem('instituicao'), uid: res.data.uid }
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  function sendEmail() {

    var titulo= 'Bem-vindo ao iMPERIUM Educ'
    var body = 'Para acessar o sistema pela primeira vez, vá até o site ... e clique em recuperar senha'+
                ', depois disso, digite o email que forneceu para o login do App e clique em recuperar'+
                ' assim, poderá escolher uma senha própria'
    const token = localStorage.getItem("token");
    api
        .post(
            "sendEmail",
            {
                emailFrom: 'lucas313lima@gmail.com',
                emailTo: String(email),
                titulo: String(titulo),
                corpo: String(body),
                senha: 'luc@slim@313'
            },
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        )
        .then(res => {
        })
        .catch(e => {
            console.log(e);
        });
}

  return (
    <>
      <div id='formHead'>
      
        <Container fluid>
            <Row>
              <Col sm={2}>
                <Link id="bt-return" to="/responsaveis">
                  <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                  Retornar
                </Link>
              </Col>
              <Col sm={8}><h5>Cadastro de Responsáveis</h5></Col>
            </Row>
          </Container>
        <hr />
      </div>
      <Form id='formBody'>

        <Form.Label htmlFor="name">NOME COMPLETO </Form.Label>
        <Form.Control
          id="name"
          value={name}
          onChange={event => setName(event.target.value)}
        />
        <Form.Label htmlFor="name">Endereço</Form.Label>
        <Form.Control
          id="address"
          type='address'
          value={address}
          onChange={event => setaddress(event.target.value)}
        />
        <Form.Label htmlFor="name">CPF </Form.Label>
        <Form.Control
          id="cpf"
          
          value={cpf}
          onChange={event => setcpf(event.target.value)}
        />
        <Form.Label htmlFor="email">E-MAIL </Form.Label>
        <Form.Control
          id="email"
          
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <Form.Label>SENHA </Form.Label>
        <Form.Control
          type="password"
          
          id="password"
          value={password}
          onChange={event => setPassword(event.target.value)}
        />
        <BtLoad id="BotaoFormResp" onClick={cadastResp} state={stateBt} title='Cadastrar Responsável'/>

      </Form>
    </>
  )
}

function Logado(props) {
  const [stateBt, setstateBt] = useState(false);
  const [email, setEmail] = useState("");

  function verificarUser() {
    setstateBt(true)
    const uid = firebase.auth().currentUser.uid;
    firestore.collection("responsaveis")
      .get()
      .then(querySnapshot => {
        var id = ''
        var data = querySnapshot.docs.map(doc => { return { data: doc.data(), id: doc.id } });
        data.forEach(element => {
          if (email.toLocaleLowerCase() === element.data.email.toLocaleLowerCase())
            id = element.id;
        });
        firestore.collection("admins")
          .doc(uid)
          .get()
          .then(doc => {
            setstateBt(false)
            const { instituicao } = doc.data();
            props.history.push({
              pathname: '/alunoDependente',
              state: { instituicao: instituicao, uid: id }
            });
          });
      }).catch(function (error) {
        console.log(error)
      })

  }
  return (
    <>
      <div id='formHead'>
      <Container fluid>
            <Row>
              <Col sm={2}>
                <Link id="bt-return" to="/responsaveis">
                  <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                  Retornar
                </Link>
              </Col>
              <Col sm={8}><h5>Logar no Sistema</h5></Col>
            </Row>
          </Container>
          <hr />
          <div id='formBody'>
            <Form.Label htmlFor="email">E-MAIL</Form.Label>
            <Form.Control
              type="email"
              id="email"
              placeholder="Seu e-mail"
              value={email}
              onChange={event => setEmail(event.target.value)}
            />
            <BtLoad id="BotaoFormResp"   onClick={verificarUser} state={stateBt} title='Entrar'/>
        </div>
      </div>
    </>
  )
}
