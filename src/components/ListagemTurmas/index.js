/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";

import { useHistory } from 'react-router-dom'
import ReactPaginate from 'react-paginate';
import { BsSearch } from 'react-icons/bs';
import { FaChevronCircleRight, FaTrash } from "react-icons/fa";
import BtLoad from "../ButtonLoading"



import { Modal, OverlayTrigger, Tooltip, Button, Form, Table, FormControl, InputGroup, Alert, ProgressBar } from 'react-bootstrap';
import './styles.css';
import ButtonLoading from "../ButtonLoading";

export default function ListagemTurmas() {

  const history = useHistory();
  const [cadastrar, setCadastrar] = useState(false);
  const [exluirTurma, setexluirTurma] = useState(false);
  const [security, setsecurity] = useState('');
  const [confirmSecurity, setconfirmSecurity] = useState('');
  const [idturma, setidturma] = useState('');
  const [nome, setnome] = useState("");
  const [sala, setsala] = useState("");
  const [nivel, setnivel] = useState("");
  const [turno, setturno] = useState("");
  const [datavalue, setdatavalue] = useState([]);
  const [filter, setfilter] = useState('');
  const Turnos = ["Manhã", "Tarde", "Noite"];
  const niveis = ["Primário", "Ensino fundamental", "Ensino médio"];
  const perPage = 5;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const [offset, setoffset] = useState(0);
  const [postData, setpostData] = useState('');
  const [stateTurma, setstateTurma] = useState('');
  const [confirmdes, setconfirmdes] = useState(false);
  const [confirmtit, setconfirmtit] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const [stateBt, setstateBt] = useState(false);
  const [isLoading, setisLoading] = useState(true);

  const filteredData = datavalue.filter(item => {
    return Object.keys(item).some(key => item['ano'].toLowerCase().includes(lowercasedFilter));
  });
  useEffect(() => {
    Turmas();
  }, []);

  function deleteTurma() {
    setstateBt(true)
    var alunos = datavalue.filter(Doc => Doc.id === idturma)[0].alunos
    const token = localStorage.getItem("token");
    api.put("/deleteTurma",
      {
        id: idturma,
        alunos: alunos
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setstateBt(false)
        history.push("turmas");
        window.location.reload();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function desableTurma() {
    setstateBt(true)
    var turma = datavalue.filter(Doc => Doc.id === idturma)[0]
    var notasAlunos = [];
    turma.alunos.forEach((aluno, index) => {
      notasAlunos.push({ id: aluno.id, notas: [] });
      var nt = [];
      turma.materias.forEach(materia => {
        materia.alunos.forEach(aln => {
          if (aln.id === aluno.id) {
            nt.push({
              materia: materia.nome,
              notas: aln.notas,
              professor: materia.professor
            }); return
          }
        })
      })
      Object.assign(notasAlunos[index].notas, nt);
    })
    const token = localStorage.getItem("token");
    api.put("/desableTurma",
      {
        id: idturma,
        alunos: turma.alunos,
        status: stateTurma,
        notas: notasAlunos
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setstateBt(false)

        window.location.reload();
      })
      .catch(e => {
        console.log(e);
      });
  }

  function Turmas() {
    const token = localStorage.getItem("token");
    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setisLoading(false);
        if (res.data.length !== 0)
          setdatavalue(res.data);
        else
          setdatavalue([{
            ano: "Nenhum dado cadastrado",
            turno: "-"
          }])
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function cadastrarTurma() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarTurma",
        {
          ano: nome,
          turno: turno,
          sala: sala,
          nivel: nivel
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        localStorage.setItem('turma', res.data.id)
        history.push("turma");
        setstateBt(false)
      })
      .catch(e => {
        console.log(e);
      });
  }



  useEffect(() => { receivedData(offset, filter); }, [offset, filter, datavalue])
  useEffect(() => {
    if (filter !== '') {
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  }, [filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if (filter === '') {
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  function receivedData(offset, filter) {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(data =>
      <tr>
        <td>{data.ano}</td>
        <td>{data.turno}</td>
        <td>
          <OverlayTrigger placement="left" overlay={<Tooltip>Perfil de turma</Tooltip>}>
          <Button id="BotaoResp2" disabled={data.turno === "-"} 
          onClick={() => {
            localStorage.setItem('turma', data.id); history.push("/turma");}}>
          <FaChevronCircleRight 
          size={18}  />
          </Button>
          </OverlayTrigger>

          <OverlayTrigger placement="right" overlay={<Tooltip>Excluir</Tooltip>}>
            <Button id="BotaoResp2" disabled={data.turno === "-"}
            onClick={() => { setexluirTurma(true); setidturma(data.id); setsecurity(data.ano) }}>
              <FaTrash size={18}/>
            </Button>
          </OverlayTrigger>
        </td>
        <td>
          <OverlayTrigger placement="left" overlay={<Tooltip id="tooltip">Alterar situação</Tooltip>}>
            {(data.status ?
              <Button
                disabled={(data.turno === '-')}
                id="situacao-escola"
                onClick={() => {
                  setconfirmtit("Desativar turma?");
                  setconfirmdes(true)
                  setstateTurma(data.status)
                  setidturma(data.id)
                }}

              >Ativa</Button> :

              <Button
                id="situacao-escola"
                disabled={(data.turno === '-')}
                onClick={() => {
                  setconfirmtit("Reativar turma?");
                  setconfirmdes(true);
                  setstateTurma(data.status)
                  setidturma(data.id)
                }}
              >Desativa</Button>
            )}
          </OverlayTrigger>
        </td>
      </tr>
    )
    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);
  }

  return (
    <>
      <div id="tagBarraPesquisa" >

        <span className="barraDePesquisa">
          <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Turma'}
          />

          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>
        <Button id="BotaoCadDisc" onClick={() => { setCadastrar(true); setturno(Turnos[0]); setnivel(niveis[0]) }} >Cadastrar Turma</Button>
      </div>

      <div id="listagem">
        <Table id="tabelaCameras" borderless striped responsive='sm'>
          <thead>
            <tr>
              <th>Turma</th>
              <th>Turno</th>
              <th>Ações</th>
              <th>Situação</th>
            </tr>
          </thead>
          <tbody>


            {postData}
          </tbody>
        </Table>
        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          forcePage={forceNew} />
      </div>



      <Modal show={exluirTurma} onHide={() => setexluirTurma(false)} >
        <Modal.Header id="modalHeader">
          <Modal.Title>Deseja Excluir a turma? </Modal.Title>
        </Modal.Header>
        <Modal.Body id="modalBody">

          <p>Se essa turma for deletada, todo o histórico e notas dos alunos cadastrados nela serão excluídos de forma permanente e não poderão ser recuperados. </p>

          <Alert variant='danger'>
            <Form.Label htmlFor="text">Para deletar a turma, por favor digite <strong>{security}</strong> para confirmar</Form.Label>
          </Alert>

          <Form.Control value={confirmSecurity} onChange={event => { setconfirmSecurity(event.target.value); }} />


        </Modal.Body>
        <Modal.Footer id="modalBody">
          <ButtonLoading id="BotaoEditS"
            state={stateBt}
            title='Sim'
            onClick={() => {
              if (security === confirmSecurity) deleteTurma()
              else {
                setexluirTurma(false)

                setTimeout(() => { setexluirTurma(true) }, 1000);
              }
            }} />
          <Button id="BotaoEditN" onClick={() => setexluirTurma(false)} >Não</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={cadastrar} onHide={() => setCadastrar(false)} centered size="lg">
        <Modal.Header  id="modalHeader">
          <Modal.Title >Cadastro de Turma</Modal.Title>
          <button onClick={() => setCadastrar(false)} >X</button>
        </Modal.Header >
        <Modal.Body id="modalBody">
          <InputGroup id="editInput">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Turma</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              value={nome}
              onChange={event => setnome(event.target.value)}
            />
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Nível</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control as="select"
              value={nivel}
              onChange={event => { setnivel(event.target.value); }}>
              {niveis.map(nivel => <option>{nivel}</option>)}
            </Form.Control>

          </InputGroup>
          <InputGroup id="editInput">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Turno</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control as="select"
              value={turno}
              onChange={event => { setturno(event.target.value); }}>
              {Turnos.map(tuno => <option>{tuno}</option>)}
            </Form.Control>

            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Sala</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              value={sala}
              onChange={event => setsala(event.target.value)}
            />
          </InputGroup>

        </Modal.Body>
        <Modal.Footer id="modalBody">


          <BtLoad onClick={() => cadastrarTurma()} state={stateBt} title='Cadastrar' id="BotaoEditS" />
        </Modal.Footer>
      </Modal>
      <Modal show={confirmdes} onHide={() => setconfirmdes(false)}>
        <Modal.Header id="modal-admin-header">
          <Modal.Title>{confirmtit}</Modal.Title>
        </Modal.Header>
        <Modal.Footer  >
          <ButtonLoading id="BotaoEditS" onClick={() => desableTurma()} state={stateBt} title='Sim' />
          <Button
            id="BotaoEditN"
            onClick={() => setconfirmdes(false)}
          >Não</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={isLoading} size='lg' centered animation>
        <Modal.Body>
          <ProgressBar animated now={100} />
        </Modal.Body>
      </Modal>


    </>
  );
}
