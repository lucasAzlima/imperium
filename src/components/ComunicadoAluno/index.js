import React, { useEffect, useState } from "react";
import api from "../../services/api";
import { Card, CardColumns} from "react-bootstrap";
import './style.css';

export default function ResponsavelView(props) {
    const [notification, setnotification] = useState([])

    useEffect(() => {
        Getnot(props.history.location.state.data)
    }, [props.history.location.state.data])

    function Getnot(data) {
        const token = localStorage.getItem("token");
        api
            .get("/getNotificacoes", {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {

                var notify = []
                res.data.data.forEach(item => {
                    item.key.forEach(key => {
                        if (key === data.id || key === data.turma.idTurma || key === res.data.id) notify.push(item);
                    })
                })
                setnotification(notify);

            })
            .catch(e => {
                console.log(e);
                console.log(
                    "Erro de autenticação, verifique seu email e senha"
                );
            });
    }

    return (
        <div className="container-comunicadoaluno">
            <CardColumns className='cardcolumns-comunicadoAluno'>
                        {notification.map(data=>(
                        <Card id='comunicado-aluno-view'>
                            <Card.Body>
                                <Card.Title id="style-titleComunicadoAluno">Título</Card.Title>
                                <Card.Subtitle id="style-subtitleComunicadoAluno">{data.titulo}</Card.Subtitle>
                                <Card.Title id="style-title2ComunicadoAluno">Corpo da Mensagem</Card.Title>
                                <LongText text={data.corpo}/>
                                <Card.Title id="style-title2ComunicadoAluno">Data da postagem</Card.Title>
                                <Card.Text id="data-comunicadoAluno">{data.data}</Card.Text>
                            </Card.Body>
                        </Card>
                        ))}
            </CardColumns>
        </div>
    )
}

function LongText(props) {
    const [showAll, setshowAll] = useState(false);
  
    if (props.text.length <= 50) {
      // there is nothing more to show
      return <Card.Text id="ajust-comunicadoAluno">{props.text}</Card.Text>
    }
    if (showAll) {
      // We show the extended text and a link to reduce it
      return <Card.Text id="ajust-comunicadoAluno">
        {props.text}
        <p onClick={() => setshowAll(false)} style={{color:'#323bb4'}}> Ler Menos</p>
      </Card.Text>
    }
    // In the final case, we show a text with ellipsis and a `Read more` button
    const toShow = props.text.substring(0, 50) + "...";
    return <div>
      <Card.Text id="ajust-comunicadoAluno" >{toShow}<span onClick={() => setshowAll(true)} style={{color:'#323bb4'}}> Ler Mais</span></Card.Text>
      </div>
  }
  

