import React from 'react';
import { StyledNavBarHome } from "../IndexHeader/styles";
import desktop from "../../../assets/Desktop1.png";
import {Link} from 'react-router-dom';
import './Systemweb.css';
import {TiArrowRightThick} from 'react-icons/ti'
import alunos from '../../../assets/alunos.png';
import noticias from '../../../assets/noticias.jpg';
import comunicados from '../../../assets/comunicados.png';
import dashbord from '../../../assets/dashboard.png';
import app from '../../../assets/imageApp.svg';
import Footer from '../Footer/footer';

export default function SystemWeb(){

    window.scrollTo(0, 0);

    return(
        <div>
            <StyledNavBarHome/>
            <div className='page-systemweb'>
                <div className='span-systemweb'>
                    <div id='text-systemweb'>
                        <h2>Sistema Web</h2>
                        <p>O sistema Web da iMPERIUM possibilita um controle de toda gestão escolar, controle de dados de todos os alunos, flexibilização da comunicação
                            e muitas outras funcionalidades. 
                        </p>
                    </div>
                    <div id='img-system'>
                        <img alt="" src={desktop}></img>
                    </div>  
                </div>
                <div className='tutorial-systemweb'>
                    <div id='cont-tutorial1'>
                        <h2>Cadastrando os alunos da sua instituição na plataforma</h2>
                        <p>Fica mais fácil buscar informações a respeito dos alunos cadastrados na plataforma, além de ter um maior controle
                            de gestão das informações.
                        </p>
                    </div>
                    <div id='imgs-system'>
                        <img alt="" src={alunos}></img>
                    </div>
                </div>
                <div className='tutorial-systemweb'>
                    <div id='imgs-system'>   
                        <img alt="" src={comunicados}></img>
                    </div>
                    <div id='cont-tutorial1'>
                        <h2>Comunicação agiliza o controle da gestão escolar</h2>
                        <p>Após o cadastrado de alunos da sua instituição na plataforma, fica fácil o fluxo de informação, basta apenas enviar comunicados para alunos
                            ,turmas ou grupos de alunos especifícos, aonde os pais visualizam as mensagens.
                        </p>
                    </div>
                </div>
                <div className='tutorial-systemweb' typeof='sec'>
                    <div id='imgs-system'>
                        <img src={noticias} alt='imgs-real'></img>
                    </div>
                    <div id='cont-tutorial1'>
                        <h2>Notícias mais gerais geram agilidade</h2>
                        <p>Nem sempre os alunos passam a informação corretamente para seus pais, com o cadastro de notícias é possível que os pais 
                            visualizem as informações corretamente para possíveis notícias importantes.
                        </p>
                    </div>
                </div>
                <div className='tutorial-systemweb' typeof='sec'>
                    <div id='cont-tutorial1'>
                        <h2>Mais funcionalidades para sua instituição</h2>
                        <p>Além de comunicados e notícias, temos várias funcionalides disponíveis para sua instituição, desde o cadastramentos dos professores,
                            responsáveis dos alunos, disciplinas existentes e muito mais.
                        </p>
                    </div>
                    <div id='imgs-system'>
                        <img alt="" src={dashbord}></img>
                    </div>
                </div>
                <div className='toApp-systemweb'>
                    <div id='cont-appRun'>
                        <h2>Interesssante né? Agora como os responsáveis tem acesso aos comunicados, notícias e notas do alunos?</h2>
                        <p>Através do aplicativo da iMPERIUM, os responsáveis ficam por dentro de tudo que acontece com o aluno. Clica na imagem ao lado</p>
                        <TiArrowRightThick  
                            size={40}
                            style={{color:'gray'}}
                        />
                    </div>
                    <div id='icon-apprun'>
                        <Link to='/sistemamobo'>
                            <button>
                                <img alt="" src={app}></img>
                            </button>
                        </Link>
                    </div>
                </div>
                <Footer/>
            </div>
        </div>
    );
};
