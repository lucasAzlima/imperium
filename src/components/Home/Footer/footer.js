import React from 'react';
import './footer.css';
import logo_rp from "../../../assets/logo_rp.png";

export default function Footer(){
    return(
        <div className="celula_lp_rodape" style={{ backgroundColor: "rgb(29, 29, 29)" }}>
            <img src={logo_rp} alt="logo" className="image_lp_logo" color="white" />
            <hr style={{ backgroundColor: 'gray', margin: '0 2vh' }} />
            <div style={{ display: 'flex', flexDirection: 'column', margin: '4vh 4vh 0vh 5vh' }}>
                <p>Início</p>
                <p>Como funciona</p>
            </div>
        </div>
    );
}