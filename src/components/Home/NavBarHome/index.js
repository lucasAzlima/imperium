import React from 'react';

import { Nav, Navbar, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import logoImp from "../../../assets/logo.png";
import './NavBarHome.css';

export default function NavBarHome({ history }) {
    
    return (
        <div>
            <Navbar fill variant="tabs" defaultActiveKey="/home" fixed="top" className="justify-content-end"
                style={{
                    height: '12vh', alignItems: 'center',
                    backgroundColor: 'white', justifyContent: 'space-between',
                    fontSize: '16px',
                }}>
                <Link to='/' style={{ marginRight:'auto' }}>
                    <img 
                        src={logoImp} 
                        alt="logo" 
                        width='60px'
                        height='60px' 
                    />
                </Link>
                <Nav.Item>
                    <Nav.Link href="/" >
                        Home
                </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/form" >
                        Solicitar
                </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/login" >
                        <Button id="BotaoCadResp">Login</Button>
                    </Nav.Link>
                </Nav.Item>
            </Navbar>

        </div>
    );
}
