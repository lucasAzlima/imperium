import React from 'react';
import './systemmob.css';
import { StyledNavBarHome } from '../IndexHeader/styles';
import {Link} from 'react-router-dom';
import Footer from '../Footer/footer';
import cell1 from "../../../assets/Cell1.png";
import cell2 from "../../../assets/Cell2.png";
import cell3 from '../../../assets/celular1.png';
import cell4 from '../../../assets/celular2.png';
import cell5 from '../../../assets/celular3.jpg';
import solicitar from '../../../assets/cadastro.svg';


export default function SystemMobile(){

    window.scrollTo(0, 0);

    return(
        <div>
            <StyledNavBarHome/>
            <div className='page-systemob'>
                <div className='sec1-systemob'>
                    <div id='text-systemob'>
                        <h2>Controle da educação na palma de sua mão</h2>
                        <p>O aplicativo da iMPERIUM possibilita aos resposáveis ver notas, comunicados e notícias cadastradas pela escola.</p>
                    </div>
                    <div id='imgs-systemob'>
                        <img src={cell1} alt=""/>
                        <img src={cell2} alt=""/>
                    </div>
                </div>
                <div className='sec2-systemob'>
                    <div id='textsec2-systemmob'>
                        <h2>Aplicação exclusivamente aos resposáveis</h2>
                        <p>Após o cadastro do aluno na plataforma web, o responsável do mesmo consegue ter acesso aos dados do aluno pelo aplicativo.</p>
                    </div>
                    <div id='img-sec2'>
                        <img src={cell3} alt=""/>
                    </div>
                </div>
                <div className='sec2-systemob' style={{background:'white'}}>
                    <div id='img-sec2'>
                        <img src={cell4} alt=""/>
                    </div>
                    <div id='textsec2-systemmob'>
                        <h2>Agora só escolher o vínculo</h2>
                        <p>Após o responsável definir sua senha, ele tem acesso aos alunos cadastrados na instituição, então basta escolher um aluno 
                            e acessar todas as funcionalidades disponíveis no aplicativo.
                        </p>
                    </div>
                </div>
                <div className='sec2-systemob' style={{background:'white'}}>
                    <div id='img-sec2'>
                        <img src={cell5}
                            style={{
                                boxShadow:'1px 1px 3px 3px rgba(0, 0, 0, 0.1)',
                                borderRadius:3
                            }}
                        alt=""/>
                    </div>
                    <div id='textsec2-systemmob'>
                        <h2>Diversas funcionalidades disponíveis para os responsáveis</h2>
                        <p>O aplicativo fornece aos responsáveis desde notícias cadastradas pela escola, comunicados a respeito do aluno, notas dos alunos e muito mais.</p>
                    </div>
                </div>
                <div className='endsec-systemob'>
                    <div id='textend-systemob'>
                        <h2>Solicite já o cadastro na plataforma</h2>
                        <Link to='/form'>  
                        <button type='button' id="BotaoAccordion">
                            Solicitar
                        </button>
                      </Link>
                    </div>
                    <div id='imgend-systemob'>
                        <Link to='/form'>
                            <button>
                                <img src={solicitar} alt=""/>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    );
};