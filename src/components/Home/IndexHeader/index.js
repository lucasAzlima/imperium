import React,{useState} from 'react';
import { StyledNavBarHome } from "./styles.js";
import './styles.css';
import {Link} from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'
import desktop from "../../../assets/Desktop.png";
import cell1 from "../../../assets/Cell1.png";
import tecnologia from '../../../assets/tecnologia.png';
import monitor_icon from "../../../assets/Desktop1.png";
import calc_icon from "../../../assets/icon_palmcontrol.png";
import celular_icon from "../../../assets/duplocelular.png";
import Footer from '../Footer/footer';
import web from '../../../assets/Desktop1.png';


function IndexHeader() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <div>
      <StyledNavBarHome />
      <div id="landin_gpage">
          <div className="celula_lp" style={{ marginTop: '10vh', backgroundColor: "#00249c" }}>
              <div className='text_celula'>
                <h2>iMPERIUM Soluções Digitais</h2>
                <p style={{ color: 'white' }}>Tenha o controle que você precisa ao alance das suas mãos com a tecnologia da iMPERIUM.</p>
              </div>
              <div id='imgcelula_lp'>
                <img src={desktop} alt="devices" />
              </div>
          </div>
          <Carousel 
            activeIndex={index} 
            onSelect={handleSelect} 
            className='carousel-landing'
            controls={true}
            >
            <Carousel.Item interval={500}>
                <div className='cont-itemCarousel'>
                  <div id='imgs-carousel'>
                      <img src={cell1} alt="First slide"></img>
                  </div>
                  <div id='text-carousel'>
                    <h2>iMPERIUM Soluções Digitais</h2>
                    <p>Com as tecnologias da iMPERIUM você é capaz de manter uma gestão aprimorada, promovendo uma melhor educação para seus alunos
                      e um maior desempenho dos mesmos. As plataformas da iMPERIUM, dividida em aplicação Web e Mobile, garante a capacidade de flexbilização
                      educacional.
                    </p>
                    <Link to='/sistemaweb'>
                      <button type='button'>
                          Saiba Mais
                      </button>
                    </Link>
                  </div>
                </div>
              </Carousel.Item>
            <Carousel.Item interval={500}>
                <div className='cont-itemCarousel'>
                    <div id='text-carousel'>
                      <h2>Experimente Nossa Tecnologia</h2>
                      <p>Nossas plataformas oferecem o conforto e rapidez para comunicação entre responsáveis e escola, produzida com um layout 
                        agradável de fácil usuabilidade, faça sua solicitação.
                      </p>
                      <Link to='/form'>  
                        <button type='button'>
                            Solicitar
                        </button>
                      </Link>
                    </div>
                    <div id='imgs-carousel'>
                      <img src={monitor_icon} alt="Third slide"></img>
                    </div>
                </div>
            </Carousel.Item>
            <Carousel.Item interval={500}>
                <div className='cont-itemCarousel'>
                  <div id='imgs-carousel'>
                      <img src={calc_icon} alt="First slide"></img>
                  </div>
                  <div id='text-carousel'>
                    <h2 typeof='help'>Dúvidas De Como Funciona Para Solicitar? Basta navegar ate o fim da tela.</h2>
                  </div>
                </div>
            </Carousel.Item>
          </Carousel>
          <div className='resu-System'>
                <div id='text-resu'>
                    <h2>Pura Tecnologia</h2>
                    <p>As plataformas da iMPERIUM utilizando as atuais tecnologia de desenvolvimento para agradar o máximo ao cliente. Com um visual agradável, a plataforma torna-se simples e de fácil usuabilidade, feita para controle de gestão da escola e pais.
                    </p>
                </div>
                <div id='image-resu'>
                    <img alt="" src={tecnologia}></img>
                </div>
          </div>
          <div className='aplic-landing'>
            <div className='web-aplic'>
              <Link to='/sistemaweb'>
                <button style={{padding:24}}>
                    <div id='imgs-web'>
                      <img src={web} style={{width:'23.5vw', height: '50vh'}} alt=''></img>
                    </div>
                    <div id='text-web'>
                      <h3>Conheça nossa plataforma web</h3>
                    </div>
                </button>
              </Link>
            </div>
            <div className='web-aplic'>
              <Link to='/sistemamobo'>
                <button>
                    <div id='imgs-web'>
                      <img style={{width:'23.5vw', height: '50vh'}} src={celular_icon} alt=''></img>
                    </div>
                    <div id='text-web'>
                      <h3>Conheça nosso aplicativo da iMPERIUM</h3>
                    </div>
                </button>
              </Link>
            </div>
          </div>
        <Footer/>
        </div>
    </div>
    );
}

export default IndexHeader;