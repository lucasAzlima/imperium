import styled from "styled-components";
import { Button, Col } from "react-bootstrap";

import NavBarHome from "../NavBarHome";

function getWidthString(span) {
  if (!span) return;

  let width = (span / 12) * 100;
  return `width: ${width}%;`;
}

function getMarginString(span) {
  if (!span) return;

  let margin = (span / 12) * 100;
  return `margin-top: ${margin}px;`;
}

function getHeightString(span) {
  if (!span) return;

  let height = ((span / 12) * 3000) / 100;
  return `height: ${height}%;`;
}

export const Column = styled.div`
  float: left;
  ${({ xs }) => (xs ? getWidthString(xs) : "width: 100%")}

  @media only screen and (min-width: 768px) {
    ${({ sm }) => sm && getWidthString(sm)};
  }

  @media only screen and (min-width: 992px) {
    ${({ md }) => md && getWidthString(md)};
  }

  @media only screen and (min-width: 1200px) {
    ${({ lg }) => lg && getWidthString(lg)};
  }
`;

export const StyledH1 = styled.h1`
  text-align: center;
  text-size-adjust: 200%;
  font-size: 8vw;
  margin-top: 10%;
  margin-left: 3%;
  font-family: "Patua One", Arial, Helvetica, sans-serif;
  color: #007bff;
  ${({ xs }) => (xs ? getWidthString(xs) : "margin-top: 50px")}

  @media screen and (min-width: 768px) {
    ${({ sm }) => sm && getMarginString(sm)};
  }

  @media screen and (min-width: 992px) {
    ${({ md }) => md && getMarginString(md)};
  }

  @media screen and (min-width: 1200px) {
    ${({ lg }) => lg && getMarginString(lg)};
  }
`;

export const StyledH5 = styled.h1`
  text-align: center;
  font-size: 1.5vw;
  text-size-adjust: 200%;

  margin: auto;
  font-family: "Roboto", Arial, Helvetica, sans-serif;
  color: #686868;
`;

export const StyledH52 = styled.h1`
  text-align: center;
  font-size: 1.6rem;
  text-size-adjust: 100%;
  margin-bottom: 20px;

  font-family: "Patua One", Arial, Helvetica, sans-serif;
  color: #454545;
`;

export const StyledCol = styled.div`
  padding: 0;
`;

export const StyledRow = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 10px;
  margin: 0;
  margin-left: 0;
`;

export const StyledButton = styled(Button)`
  padding: 3px;
  margin: 0px 5px;
  text-size-adjust: auto;
`;

export const StyledDiv = styled.div``;

export const StyledNavBarHome = styled(NavBarHome)`
  background-color: rgba(0, 0, 0, 0) !important;
  background: rgba(0, 0, 0, 0) !important;
`;

export const StyledImg = styled.img`
  ${({ xs }) => (xs ? getHeightString(xs) : "height: 80%")}

  max-height: 50%;
  margin: 0 26%;
  border-radius: 0px;
`;

export const StyledCol2 = styled(Col)`
  display: block;
  margin-left: auto;
  margin-right: auto;
`;
