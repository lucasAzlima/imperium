import React,{useState} from 'react';
import {Tabs, Tab , AppBar} from "@material-ui/core";
import PerfilAluno from '../PerfilAluno';
export default function NavegationTab(){
    const [selectedTab, setSeletectedTab] = React.useState(0);
    const handleChange = (event, newValue) =>{
        setSeletectedTab(newValue);
    }
    return(
        <>
            <Tabs value={selectedTab} onChange={handleChange}>
                <Tab label="Ver Aluno"/>
                <Tab label="Responsáveis"/>
                <Tab label="Comunicado"/>
            </Tabs>
        </>
    );
}