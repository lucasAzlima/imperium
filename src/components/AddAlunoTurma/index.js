/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { ListGroup, Form, ProgressBar, Modal } from 'react-bootstrap'
import { BsSearch } from 'react-icons/bs';
import "./styles.css";
import { useHistory } from 'react-router-dom'
import BtLoad from "../ButtonLoading"

export default function Disciplinas(props) {
  const [alunos, setalunos] = useState([]);
  const [turma, setturma] = useState([]);
  const [alunoSelect, setalunoSelect] = useState([]);
  const [filter, setfilter] = useState('');
  const [isLoading, setisLoading] = useState(true);
  const [stateBt, setstateBt] = useState(false);
  
  const history = useHistory();
  const lowercasedFilter = filter.toLowerCase();
  const filteredData = alunos.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });
  useEffect(() => {
    setturma(props.history.location.state.turma)
    GetAluno(props.history.location.state.alunos)
  }, []);

  function GetAluno(alns) {
    const token = localStorage.getItem("token");
    api
      .get("/getAlunos",

        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        var alnF = res.data.alunos;
        alns.forEach(element => {
          alnF = alnF.filter(Doc => Doc.id !== element.id)
        });
        setalunos(alnF)
        setisLoading(false);
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }



  function select(id, state) {
    if (state) {
      var AS = alunoSelect.slice();
      AS.push(alunos.filter(Doc => Doc.id === id)[0])
      setalunoSelect(AS);
    }
    else {
      setalunoSelect(alunoSelect.filter(Doc => Doc.id !== id));
    }
  }

  function addAlunos() {
    setstateBt(true)
    alunoSelect.forEach(element => {
      turma.alunos.push({ nome: element.nome, matricula: element.matricula, id: element.id })
      turma.materias.forEach(materia => {
        materia.alunos.push({ nome: element.nome, id: element.id, notas: Array(Number(turma.EstrutEstudo)).fill(0) })
      })
    })
    var trm = { idTurma: turma.id, ano: turma.ano, nivel: turma.nivel, turno: turma.turno }
    alunoSelect.forEach((aluno, index) => {
      var nt = [];
      turma.materias.forEach(materia => {
        materia.alunos.forEach(aln => {
          if (aln.id === aluno.id) { nt.push({ 
            materia: materia.nome, 
            notas: aln.notas, 
            professor: materia.professor }); return }
        })
      })
      aluno.notas = nt;
    })

    const token = localStorage.getItem("token");
    api
      .put(
        "addAlunoTurma",
        {
          materias: turma.materias,
          alunos: turma.alunos,
          id: turma.id,
          turma: trm,
          alunosadd: alunoSelect
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        history.push('turma')
      })
      .catch(e => {
        console.log(e);
      });
  }

  return (
    <>
      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control id="barraPesquisa" value={filter} onChange={event => setfilter(event.target.value)} placeholder={'Pesquisar'} />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>
        <BtLoad onClick={() => addAlunos()} state={stateBt} title='Salvar Alterações' id="BotaoCadResp" />
      </div>

      <div id="listaAlunos" >
        <div id="alunos-disp">
          <h5>Lista de Alunos</h5>
          <ListGroup variant="flush" id="list-group">
            {filteredData.map(data =>
              <Check
                nome={data.nome}
                nivel={data.nivel}
                id={data.id}
                Select={select} />
            )}
          </ListGroup>
        </div>
        <div id='alunos-select'>
        <h5>Alunos Selecionados</h5>

          <ListGroup id="list-group">
            {alunoSelect.map(selected =>
              <ListGroup.Item id="item-list1">
                <p>{selected.nome}</p> 
              </ListGroup.Item>)}
          </ListGroup>
        </div>
      </div>
      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>

    </>
  );
}

function Check(props) {
  const [state, setstate] = useState(false);

  return (
    <ListGroup.Item id="item-list2">
      <div id="aluno-check">
        <p>Nome:  {props.nome}</p>
        <p> Nível:  {props.nivel}</p>
      </div>
      <Form.Check onChange={() => { setstate(!state); props.Select(props.id, !state) }} type="checkbox" label="Selecionar" />

    </ListGroup.Item>)
}