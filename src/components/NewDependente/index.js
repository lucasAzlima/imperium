/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { ListGroup, Button, Form, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { FaTrash } from "react-icons/fa";
import { BsSearch } from 'react-icons/bs';
import "./styles.css";

export default function Disciplinas(props) {
  const [datavalue, setdatavalue] = useState([]);
  const [datavalueT, setdatavalueT] = useState([]);
  const [vinculos, setvinculos] = useState([]);
  const [vinculosup, setvinculosup] = useState([]);
  const [alunoSelect, setalunoSelect] = useState([]);
  const [filter, setfilter] = useState('');
  const [uid, setuid] = useState('');
  const [instituicao, setinstituicao] = useState('');

  const lowercasedFilter = filter.toLowerCase();
  const filteredData = datavalue.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });
  useEffect(() => {
    GetVinculos(props.history.location.state.uid);
    setuid(props.history.location.state.uid);
    setinstituicao(props.history.location.state.instituicao)
  }, []);

  function GetAluno(vinc) {
    const token = localStorage.getItem("token");
    api
      .get("/getAlunos",

        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        var alunos = res.data.alunos;
        res.data.alunos.forEach(element => {
          alunos = alunos.filter(Doc => Doc.contasAtivas < 2)
        })
        setdatavalueT(res.data.alunos)
        vinc.forEach(element => {
          alunos = alunos.filter(Doc => Doc.id !== element.idDependente)
        })
        setdatavalue(alunos)

      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }


  function updateVinculos() {

    const token = localStorage.getItem("token");
    api
      .put("/updateVinculos",
        {
          uid: uid,
          alunos: alunoSelect,
          vinculos: vinculosup,
        },
        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        props.history.push("/responsaveis")
      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function GetVinculos(uid) {
    const token = localStorage.getItem("token");
    api
      .post("/getVinculos",
        { uid: uid },
        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        setvinculos(res.data)
        GetAluno(res.data);
        res.data.forEach(data => {
          alunoSelect.push({
            idDependente: data.idDependente,
            idInstituicao: data.idInstituicao,
            contasAtivas: 0,
            contas: data.contas
          })
          vinculosup.push({
            idDependente: data.idDependente,
            idInstituicao: data.idInstituicao,
          })
        })

      })
      .catch(e => {
        console.log(e);
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }


  function select(id, contasAtivas, state) {
    if (state) {
      var perm = true;
      alunoSelect.forEach(elem => {
        if (elem.idDependente === id) { elem.contasAtivas += 1; perm = false; elem.contas.push(uid) }
      })

      if (perm) {
        var cnt = [];
        cnt.push(uid)
        alunoSelect.push({
          idDependente: id,
          idInstituicao: instituicao,
          contasAtivas: contasAtivas + 1,
          contas: cnt
        })
      };
      vinculosup.push({ idDependente: id, idInstituicao: instituicao })
    }
    else {
      setalunoSelect(alunoSelect.filter(Doc => Doc.idDependente !== id));
      setvinculosup(vinculosup.filter(Doc => Doc.idDependente !== id));
    }
  }

  function delet(id) {
    datavalue.push(datavalueT.filter(Doc => Doc.id === id)[0]);
    setvinculos(vinculos.filter(Doc => Doc.idDependente !== id));
    setvinculosup(vinculosup.filter(Doc => Doc.idDependente !== id));
    alunoSelect.forEach(elem => {
      if (elem.idDependente === id) {
        elem.contasAtivas -= 1;
        elem.contas = elem.contas.filter(Doc => Doc !== uid)
      };
    })
  }

  return (
    <>
      <div id="tagBarraPesquisa">
        <span className="barraDePesquisa">
          <Form.Control id="barraPesquisa" value={filter} onChange={event => setfilter(event.target.value)} placeholder={'Pesquisar'} />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>
        </span>
        <Button id="BotaoCadResp" onClick={() => updateVinculos()}>Salvar Alterações</Button>
      </div>

      <div id="listaAlunos-tuto" >
        <div id="alunos-disp-tuto">

          <h5>Lista de Alunos</h5>

          <ListGroup variant="flush" id="list-group-tuto">
            {filteredData.map(data => {
              return (
                <>
                  <Check
                    nome={data.nome}
                    nivel={data.nivel}
                    id={data.id}
                    CA={data.contasAtivas}
                    Select={select} />
                </>
              );
            })
            }
          </ListGroup>
        </div>
        <div id='alunos-select-tuto'>
          <h5>Alunos tutorados</h5>
          <ListGroup id="list-group-tuto">
            {vinculos.map(vinculo => 
                <ListGroup.Item id="item-list1-tuto">
                  <div id="aluno-tutorado">
                    <p>Nome: {vinculo.dependente}</p>
                    <p>Instituição: {vinculo.instituicao}</p>
                  </div>
                  <OverlayTrigger placement="right" overlay={<Tooltip>Excluir</Tooltip>}>
                    <Button onClick={() => delet(vinculo.idDependente)}>
                      <FaTrash size="18px" />
                    </Button>
                  </OverlayTrigger>
                </ListGroup.Item>
              )
            }
          </ListGroup>
        </div>
      </div>


    </>
  );
}
function Check(props) {
  const [state, setstate] = useState(false);

  return (
    <ListGroup.Item id="item-list2-tuto">
      <div id="aluno-check-tuto">
        <p>Nome:  {props.nome}</p>
        <p>Nível:  {props.nivel}</p>
      </div>
      <Form.Check onChange={() => { setstate(!state); props.Select(props.id, props.CA, !state) }} type="checkbox" label="Selecionar" />

    </ListGroup.Item>)
}