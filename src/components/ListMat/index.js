import React from "react";
import { Button, Card, Col, Form } from "react-bootstrap";

import "./styles.css";

export default class ListAluno extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'light',
    }
  }

  colorChange = () =>{
    if( this.props.color === true) this.setState({color: 'light'})
  }

  render() {
    return (
      <>
        <Form.Row >
          {this.colorChange()}
          <Card bg={this.state.color} style={{ borderRadius: "1em", margin: "1em" }}>
            <Card.Header >{this.props.nome}</Card.Header>

            <Form.Control as="select"
              onChange={event => this.props.profSelect(`${this.props.id}|${event.target.value}`)}>
              {this.props.Professores.map(pf => (
                <option>{pf.nome}</option>))}
            </Form.Control>
            <Col style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <Button style={{ margin: 5 }} variant='info' value={this.props.id}
                onClickCapture={() => this.setState({ color: 'success' })}
                onClick={event => this.props.discSelect(event.target.value)}>
                Adicionar</Button>
              <Button style={{ margin: 5 }} variant='info' value={this.props.id}
                onClickCapture={() => this.setState({ color: 'light' })}
                onClick={event => this.props.delListDisc(event.target.value)}>
                Remover</Button>
            </Col>

          </Card>
        </Form.Row >
      </>
    );
  }
}