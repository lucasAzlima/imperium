import React, { useState } from "react";
import { Form, Col, Container, Modal, Row, Button, ListGroup, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FiArrowLeft } from 'react-icons/fi'
import { Link } from 'react-router-dom';
import "./styles.css";
import { ListGroupRow } from "./styles.js";
import api from "../../services/api"
/* import Responsavel from "../ModResponsavel" */
import firebase from "../../services/firebase";
import { AiOutlineDelete } from "react-icons/ai";
import { MdCancel } from "react-icons/md";
import BtLoad from "../ButtonLoading"

export default function CadastroAluno({ history }) {
  const [DadosResp, setDadosResp] = useState([]);
  const [nameStudent, setNameStudent] = useState("");
  const [cpfStudent, setCpfStudent] = useState("");
  const [gender, setGender] = useState(0);
  const [matricula, setmatricula] = useState("");
  const [dateStudent, setDateStudent] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [ImageShow, setImage] = useState("");
  const [show, setshow] = useState(false);
  const [Porcentagem, setPorcentagem] = useState(0);
  const sexo = ["Selecionar sexo", "Masculino", "Feminino"];
  const [filename, setfilename] = useState("");
  const [stateBt, setstateBt] = useState(false);

  function cadastrarAluno() {
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarAluno",
        {
          matricula: matricula,
          nascimento: dateStudent,
          nome: nameStudent,
          responsaveis: DadosResp,
          sexo: gender,
          cpfAluno: cpfStudent,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        if (picture !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(picture, { contentType: 'image/jpeg' })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) setTimeout(() => { setshow(false); }, 1000)
            });
            history.push("/Alunos")
        }
        setstateBt(false)
        setmatricula('');
        setDateStudent('');
        setNameStudent('');
        setDadosResp([]);
        setGender('');
        setCpfStudent('');
        setImage('');
        setPicture('');
        history.push("/Alunos")
      })
      .catch(e => {
        console.log(e);
      });
  }



  function setPic(event) {
    if (event.target.files[0] !== undefined) {
      const pictureConverter = new FileReader();
      setPicture(event.target.files[0]);
      setfilename(event.target.files[0].name)
      try {
        pictureConverter.readAsDataURL(event.target.files[0]);
        pictureConverter.addEventListener("loadend", () =>
        cropImage(pictureConverter.result, 1 / 1));
         
      }
      catch (e) {
        setImage("");
      }
    } else {
      setPicture(undefined);
      setImage('')
      setfilename('')
    }
  }

  function cropImage(url, aspectRatio) {

    return new Promise(resolve => {
      const inputImage = new Image();
      inputImage.crossOrigin = 'anonymous';

      inputImage.onload = () => {
        
        const inputWidth = inputImage.naturalWidth;
        const inputHeight = inputImage.naturalHeight;
        const inputImageAspectRatio = inputWidth / inputHeight;

        let outputWidth = inputWidth;
        let outputHeight = inputHeight;

        if (inputImageAspectRatio > aspectRatio) {
          outputWidth = inputHeight * aspectRatio;
        } else if (inputImageAspectRatio < aspectRatio) {
          outputHeight = inputWidth / aspectRatio;
        }
        const outputX = (outputWidth - inputWidth) * .5;
        const outputY = (outputHeight - inputHeight) * .5;

        const outputImage = document.createElement('canvas');

        outputImage.width = outputWidth;
        outputImage.height = outputHeight;
        outputImage.id = 'crop'

        const ctx = outputImage.getContext('2d');
        ctx.drawImage(inputImage, outputX, outputY);
        console.log(outputImage)
        setImage(outputImage.toDataURL())
        resolve(outputImage);
      };
      inputImage.src = url;
    });
  };

  function RespSelect(event) {
    var data = DadosResp.slice();
    var disc = data.filter(doc => doc.cpf !== event.cpf);
    disc.push(event);
    setDadosResp(disc)

  }

  function delListResp(event) {
    var Disciplina = DadosResp.filter(Disc => Disc.cpf !== event.cpf);
    setDadosResp(Disciplina);
  }


  return (
    <>
      <div id='box-cadAL' >
        <hr />
        <Container fluid>
          <Row>
            <Col md={2}>
              
              <Link id="bt-return" to="/Alunos">
                  <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                      Retornar
              </Link>
            </Col>
            <Col md={8}><h5>Dados do Aluno</h5></Col>
          </Row>
        </Container>
        <hr />
        <img style={{
          borderRadius: 0,
          width: ImageShow ? '20vh' : 0,
          height: ImageShow ? '20vh' : 0}} alt="" src={ImageShow} />
        <div id='input-wrapper-cadNot'>
          <OverlayTrigger delay={800} overlay={<Tooltip id="tooltip">
            Por favor, selecionar somente arquivos que sejam do tipo png ou jpg</Tooltip>}>
            <div className="input-wrapper-cadNot">
              <label for='input-file-cadNot'>Selecionar um arquivo</label>
              <input type="file" onChange={(event) => setPic(event)} id='input-file-cadNot' />
              <span id='file-name'>{filename}</span>
            </div>
          </OverlayTrigger>
          {(picture !== undefined) ?
            <MdCancel onClick={() => {
              setImage('')
              setfilename('')
              setPicture(undefined)
              document.getElementById("input-file-cadNot").value = ''
            }} id='icon-cancel-ca' /> : <div />}
        </div>

        <Form id='form-cadAL'>
          <div >
            
              <Form.Row>
                <Col>
                  <Form.Label htmlFor="name">Nome completo do aluno</Form.Label>
                  <Form.Control 
                    type="text"
                    id="name"
                    value={nameStudent}
                    onChange={event => setNameStudent(event.target.value)}
                  />
                </Col>
                <Col>
                  <Form.Label htmlFor="number">
                    CPF do aluno<span>(somente números)</span>
                  </Form.Label>
                  <Form.Control
                    type="number"
                    id="name"
                    value={cpfStudent}
                    onChange={event => setCpfStudent(event.target.value)}
                  />
                </Col>

              </Form.Row>
              <Form.Row>
                <Col>
                  <Form.Label htmlFor="number">Matrícula do aluno</Form.Label>
                  <Form.Control
                    type="number"
                    id="name"
                    value={matricula}
                    onChange={event => setmatricula(event.target.value)} />
                </Col>

                <Col>
                  <Form.Label htmlFor="date">
                    Data de nascimento do aluno
                  </Form.Label>
                  <Form.Control
                    type="date"
                    id="date"
                    value={dateStudent}
                    onChange={event => setDateStudent(event.target.value)}
                  />
                </Col>

                <Col>
                  <Form.Label htmlFor="name">Sexo</Form.Label>
                  <Form.Control as="select"
                    value={gender}
                    onChange={event => setGender(event.target.value)}>
                    {sexo.map(sexo => (
                      <option>{sexo}</option>))}
                  </Form.Control>
                </Col>
                
              </Form.Row>
          </div>
          <hr />
          <h5>Dados dos Responsáveis</h5>
          <hr />
            <Form.Row>
              <Row style={{ marginLeft: 5, marginBottom: 5 }}>
                <Responsavel
                  RespSelect={RespSelect} />

                <Col>
                  <Form.Label htmlFor="name">Responsáveis salvos</Form.Label>
                  <ListGroup>
                    {DadosResp.map(resp => {
                      return (
                        <ListGroupRow>
                          <Form.Label style={{ marginRight: 15 }}>{resp.nome}</Form.Label>
                          <Button variant='danger'
                            onClick={() => delListResp(resp)}><AiOutlineDelete /></Button>
                        </ListGroupRow>
                      )
                    })}
                  </ListGroup>
                </Col>
              </Row>
            </Form.Row>
          
          <BtLoad  onClick={cadastrarAluno} state={stateBt} title='Cadastrar' id='btni'/>
        </Form>
      </div>
      <Modal show={show} style={{
        fontSize: 15, display: 'flex',
        justifyContent: 'center', alignItems: 'center'
      }}>
        <Modal.Title><p >Upload de Imagem</p></Modal.Title>
        <Modal.Body>Upload está {Porcentagem.toFixed(2)}% concluido</Modal.Body>
      </Modal>
    </>
  );
}






function Responsavel(props) {
  const [nome, setnome] = useState('')
  const [parent, setparent] = useState('')
  const [tel1, settel1] = useState('')
  const [tel2, settel2] = useState('')
  const [idade, setidade] = useState('')
  const [cpf, setcpf] = useState('')

  var dados = {
    nome: nome, parent: parent, tel1: tel1, tel2: tel2, idade: idade, cpf: cpf
  };

  function reset() {
    setparent('');
    setnome('');
    setidade('');
    settel1('');
    settel2('');
    setcpf('');
  }
  return (
    <div>

      <Form.Row >
        <Form.Group as={Col} controlId="idade">
          <Form.Label>Nome</Form.Label>
          <Form.Control onChange={e => setnome(e.target.value)} />
        </Form.Group>

        <Form.Group as={Col} controlId="parent">
          <Form.Label>Parentesco</Form.Label>
          <Form.Control onChange={e => setparent(e.target.value)} />
        </Form.Group>
      </Form.Row>

      <Form.Row >
        <Form.Group as={Col} controlId="idade">
          <Form.Label>Idade</Form.Label>
          <Form.Control type="number" onChange={e => setidade(e.target.value)} />
        </Form.Group>
        <Form.Group as={Col} controlId="cpf">
          <Form.Label>Cpf do Resposável</Form.Label>
          <Form.Control type="tel" onChange={e => setcpf(e.target.value)} />
        </Form.Group>
      </Form.Row>

      <Form.Row>
        <Form.Group as={Col} controlId="idade">
          <Form.Label>Telefone 1</Form.Label>
          <Form.Control type="tel" onChange={e => settel1(e.target.value)} />
        </Form.Group>
        <Form.Group as={Col} controlId="idade">
          <Form.Label>Telefone 2</Form.Label>
          <Form.Control type="tel" onChange={e => settel2(e.target.value)} />
        </Form.Group>
      </Form.Row>


      <Row style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom:15 }}>
        <Button 
          onClick={() => { props.RespSelect(dados); reset() }}>
          Adicionar</Button>
      </Row>
    </div>
  );
}