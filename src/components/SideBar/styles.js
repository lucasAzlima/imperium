import styled from 'styled-components';

export const Container = styled.div`
  @media only screen and (min-width: 10px) {
        width: ${props => (props.grid ? props.grid / 12 * 100 : '8:33')}%;
    }
  height: 100%;
  background: #323bb4;
  padding: 50px 0;
  position: fixed;
  display: flex;
  align-items: flex-start;
  justify-content: start;
  flex-direction: column;
  background: #185296;
  border-right: solid 1px #f0f0f5;
  border-radius: 0px 30px 30px 0px;
  width: auto;
  overflow: auto;
  direction:rtl;

  img {
      max-width:180px;
      align-items: center;
      justify-self:center;
  }



  h4{
        color: white;
        align-self:center;
        margin-top:20;
        font-family: Poiret One;
        font-style: normal;
        font-weight: normal;
        font-size: 25px;
  }

`;

export const Tabs = styled.ul`
    
    width: 100%;
    margin-top:30px;
    display:flex;
    justify-content:center;
    list-style-type: none;
    flex-direction: column;
    
    

        li {
            
            display: flex;
            margin: 0px 1px 5px;
            padding: 0px;
            height:40px;
            align-items: center;
            direction:ltr;
            

        svg {
            margin-left:15px;
            margin-right: 10px;
            color:white;
            height: 20px;
            width: 100%;
            max-width:21px;
        }

        a{
            width: 100%;
            margin: 0 1px;
            color: white;
            text-decoration: none;
            font-size: 15px;
            

        }

        :hover{
            
            color: #0F4C81;
            background-color: white;
            border-radius: 3px;

            svg{
                color:#0F4C81;
            }

            a{
                color:#0F4C81;
                
            }
        }

}
`