import React from 'react';
import { Container, Tabs } from './styles';
import { FaMoneyCheckAlt, FaBookReader, FaGraduationCap, FaCamera, FaChalkboardTeacher } from 'react-icons/fa';
import { BsFillPeopleFill, BsNewspaper } from 'react-icons/bs';
import { MdNotificationsActive, MdGroup, MdSubject, MdDashboard } from 'react-icons/md';
import { Link } from 'react-router-dom';

export default function SideBar() {
    return (
        <Container grid={2}>
            <h4>iMPERIUM Educ</h4>
            <Tabs>
                <li>
                    <Link to='/dashboard'>
                        <MdDashboard />
                        Home
                </Link>
                </li>
                <li>
                    <Link to='/PerfilEscola'>
                        <FaGraduationCap />
                        Perfil Escola
                    </Link>
                </li>
                <li>
                    <Link to='/Alunos'>
                        <BsFillPeopleFill />
                        Alunos
                    </Link>
                </li>
                <li>
                    <Link to='/responsaveis'>
                        <MdGroup />
                        Responsáveis
                    </Link>
                </li>
                <li>
                    <Link to='/Noticias'>
                        <BsNewspaper />
                        Notícias
                    </Link>
                </li>

                <li>
                    <Link to='/comunicados'>
                        <MdNotificationsActive />
                        Comunicados

                </Link>
                </li>
                <li>
                    <Link to='/Professores'>
                        <FaChalkboardTeacher />
                            Professores
                    </Link>
                </li>
                <li>
                    <Link to='/Disciplinas'>
                        <MdSubject />
                            Disciplinas
                    </Link>
                </li>
                <li>
                    <Link to='/turmas'>
                        <FaBookReader />
                            Turmas
                    </Link>
                </li>
                <li>
                    <Link to='_blanck'>
                        <FaMoneyCheckAlt />
                            Financeiro
                    </Link>
                </li>
                <li>
                    <Link to='/Cameras'>
                        <FaCamera />
                            Câmeras
                    </Link>
                </li>
            </Tabs>
        </Container>
    );
}