import React from "react";
import "./styles.css";
import { CardColumns, Card } from "react-bootstrap";
import aluno from '../../assets/icons-dash/alunos.svg'
import turmas from '../../assets/icons-dash/turmas.svg'
import cameras from '../../assets/icons-dash/cameras.svg'
import professor from '../../assets/icons-dash/professor.svg'
import disciplinas from '../../assets/icons-dash/disciplinas.svg'
import responsaveis from '../../assets/icons-dash/responsaveis.svg'
import financeiro from '../../assets/icons-dash/financeiro.svg'
import noticias from '../../assets/icons-dash/noticias.svg'
import comunicados from '../../assets/icons-dash/comunicados.svg'
import { Link } from "react-router-dom";
export default function DashboardContent(props, { history }) {

  return (
    <div id='dashboard-g'>
      <CardColumns id='cardscol' >
        <CardDashboard
          name="Alunos"
          img={aluno}
          path="/Alunos"
        />

        <CardDashboard
          name="Notícias"
          img={noticias}
          path="/Noticias"
        />
        <CardDashboard
          name="Notificações"
          img={comunicados}
          path="/notificacao"
        />
        <CardDashboard
          name="Turmas"
          img={turmas}
          path="/turmas"
        />
        <CardDashboard
          name="Responsáveis"
          img={responsaveis}
          path="/responsaveis"
        />
        <CardDashboard
          name="Financeiro"
          img={financeiro}
          path="/"
        />
        <CardDashboard
          name="Professores"
          img={professor}
          path="/Professores"
        />
        <CardDashboard
          name="Disciplinas"
          img={disciplinas}
          path="/Disciplinas"
        />
        <CardDashboard
          name="Câmeras"
          img={cameras}
          path="/Cameras"
        />
      </CardColumns>
    </div>
  );
}


function CardDashboard(props) {

  return (
    <Card
      bg="light"
      id='cardDash'
    >
      <Card.Body>
          <Card.Subtitle>{props.name}</Card.Subtitle>
          <br />
          <Link to={props.path}>
            <img src={props.img} alt={props.name} id='img-card' />
          </Link>
      </Card.Body>
    </Card>
  );
}