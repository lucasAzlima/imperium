import React, { useState, useEffect } from "react";
import api from "../../services/api";
import firebase from "../../services/firebase";
import { Card, ListGroup, FormControl, InputGroup, Button, Modal } from 'react-bootstrap'
import Popup from "reactjs-popup";
const firestore = firebase.firestore();

export default function Cameras() {
  const [cameras, setcameras] = useState([]);
  const [nomeEdit, setnomeEdit] = useState("");
  const [linkEdit, setlinkEdit] = useState("");
  const [nome, setnome] = useState("");
  const [show, setshow] = useState(false);
  const [link, setlink] = useState("");
  const [message, setmessage] = useState("");


  useEffect(() => {
    Getgestor();
  }, []);

  function Getgestor() {

    firestore.collection('admins').where('admin', '==', true).get()
      .then(snapshot => {
        if (snapshot.empty) {
          return;
        }
        snapshot.forEach(doc => {
        });
      })
      .catch(err => {
        console.log('Error getting documents', err);
      });
  }

  function cadasCamera() {
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarCameras",
        {
          nome: nome,
          link: link
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
        setcameras([]);
        setlink('');
        setnome('');
      })
      .catch(e => {
        console.log(e);
      });
  }

  function editCamera(event) {
    const token = localStorage.getItem("token");
    var cameraSet = cameras.slice();
    cameraSet = cameraSet.filter(obj => obj.nome !== event)
    cameraSet.push({ nome: nomeEdit, linkEdit });

    api
      .put(
        "editCameras",
        {

          cameras: cameraSet
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
        setcameras([]);
        setlinkEdit('');
        setnomeEdit('');

      })
      .catch(e => {
        console.log(e);
      });
  }

  function deleteCamera(event) {
    const token = localStorage.getItem("token");
    api.put("/deleteCameras",
      {
        objDel: event,
        cameras: cameras
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        setshow(true);
        setmessage(res.data.message);
        setTimeout(() => { setshow(false); }, 1500)
      })
      .catch(e => {
        console.log(e);
      });
  }

  function copia(event) {
    var data = event.split("|");
    setlinkEdit(data[1]);
    setnomeEdit(data[0]);
  }

  return (
    <>

      <div >
        <Card >
          <Card.Header style={{ fontWeight: 'bolder' }}>Câmeras Cadastradas</Card.Header>
          <ListGroup variant="flush">
            {cameras.map(data => {
              return (
                <>
                  <ListGroup.Item style={{ display: 'flex', flexDirection: 'row', }}>
                    <Card.Body>
                      <Card.Subtitle>Câmera</Card.Subtitle>
                      <Card.Text>{data.nome}</Card.Text>
                    </Card.Body>

                    <Card.Body>
                      <Popup

                        trigger={<Button
                          value={`${data.nome}|${data.link}`}
                          className="button"
                          onClickCapture={event => copia(event.target.value)}
                          style={{ marginRight: 5 }}> Editar </Button>}
                        modal
                        closeOnDocumentClick
                      >
                        <span>
                          <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                              <InputGroup.Text id="basic-addon1">Nome</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                              placeholder="Nome"
                              aria-label="Nome"
                              aria-describedby="basic-addon1"
                              value={nomeEdit}
                              onChange={event => setnomeEdit(event.target.value)}
                            />
                            <InputGroup.Prepend>
                              <InputGroup.Text id="basic-addon1">Link</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                              placeholder="Nome"
                              aria-label="Nome"
                              aria-describedby="basic-addon1"
                              value={linkEdit}
                              onChange={event => setlinkEdit(event.target.value)}
                            />
                          </InputGroup>
                          <Button value={data.nome} onClick={event => editCamera(event.target.value)}>Atualizar</Button>
                        </span>
                      </Popup>
                      <Button value={data.nome} onClick={event => deleteCamera(event.target.value)}>Excluir</Button>

                    </Card.Body>
                  </ListGroup.Item>
                </>
              );
            })
            }
          </ListGroup>
        </Card>
        <Card style={{ marginTop: 15 }}>
          <Card.Header style={{ fontWeight: 'bolder' }}>Cadastrar nova Câmera</Card.Header>
          <Card.Body>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Nome</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Nome"
                aria-label="Nome"
                aria-describedby="basic-addon1"
                value={nome}
                onChange={event => setnome(event.target.value)}
              />
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Link</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Nome"
                aria-label="Nome"
                aria-describedby="basic-addon1"
                value={link}
                onChange={event => setlink(event.target.value)}
              />
            </InputGroup>
            <Button onClick={cadasCamera}>Cadastrar</Button>
          </Card.Body>
        </Card>
        <Modal show={show}>
          <Modal.Body>
            <Modal.Title>
              <p style={{
                fontSize: 15, display: 'flex',
                justifyContent: 'center', alignItems: 'center'
              }}>{message}</p></Modal.Title>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
}
