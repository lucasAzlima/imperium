import React, { useState, useEffect } from "react";
import './style.css'
import { Card, Form, Button, Modal } from 'react-bootstrap'
import { FaUpload } from 'react-icons/fa';
import { useHistory } from 'react-router-dom'
import api from "../../services/api";
import firebase from "../../services/firebase";
import BtLoad from "../ButtonLoading"
import Destinatarios from "../Destinatarios"

export default function Disciplinas() {
  const [file, setfile] = useState(undefined);
  const [filename, setfilename] = useState("");
  const [titulo, settitulo] = useState("");
  const [corpo, setcorpo] = useState("");
  const [destin, setdestin] = useState(false);
  const [Notify, setNotify] = useState(false);
  const history = useHistory();
  const [alunos, setalunos] = useState([]);
  const [turmas, setturmas] = useState([]);
  const [users, setusers] = useState([]);
  const [keys, setkeys] = useState([]);
  const [selectView, setselectView] = useState([]);
  const [stateBt, setstateBt] = useState(false);
  const instituicao = localStorage.getItem('instituicao');
  const [show, setshow] = useState(false);
  const [Porcentagem, setPorcentagem] = useState(0);

  useEffect(() => {
    GetAluno();
    getTurmas();
    GetListaUsers();
  }, [])

  function GetAluno() {
    const token = localStorage.getItem("token");
    api
      .get("/getAlunos", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = []
        res.data.alunos.map(data =>
          trm.push({
            nome: data.nome,
            id: data.id,
            state: false,
            turma: data.turma
          }));

        setalunos(trm);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function getTurmas() {
    const token = localStorage.getItem("token");
    api
      .get("/getTurmas", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        var trm = []
        res.data.map(data =>
          trm.push({
            ano: data.ano,
            id: data.id,
            state: false,
            turno: data.turno,
            sala: data.sala
          }));
        setturmas(trm);
      })
      .catch(e => {
        console.log(e);
      });
  }

  function GetListaUsers() {
    const token = localStorage.getItem("token");
    api
      .get("/getListUsers", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setusers(res.data)
      })
      .catch(e => {
        console.log(
          "Erro de autenticação, verifique seu email e senha"
        );
      });
  }

  function fileinput(file) {
    if (file.target.files[0] !== undefined)
      setfilename(file.target.files[0].name)
    setfile(file.target.files[0])
  }

  function sendNotification() {
    if (keys[0] === instituicao) {
      users.forEach(element => {
        element.expoTokens.forEach(async token => await sendPushNotification(token))
      });
    }
    else {
      users.forEach(element => {
        if (keys.includes(element.id))
          element.expoTokens.forEach(async token => await sendPushNotification(token))
        else console.log('nao incluso')
      })
    }
  }

  function cadasnotificacao(event) {
    event.preventDefault();
    var curr = new Date();
    curr.setDate(curr.getDate());
    var date = curr.toISOString().substr(0, 10);
    setstateBt(true)
    const token = localStorage.getItem("token");
    api
      .post(
        "cadastrarNotificacoes",
        {
          titulo: titulo,
          corpo: corpo,
          key: keys,
          filename: filename,
          data: date,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
        setstateBt(false)
        if (file !== undefined) {
          setshow(true);
          firebase.storage().ref().child(res.data.path).put(file, { contentType: 'application/pdf', })
            .on('state_changed', function (snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              setPorcentagem(progress);
              if (progress === 100) {
                setTimeout(() => { setshow(false); history.push("/Comunicados"); }, 1000);
              }
            });
          setfile("");
        }
        settitulo("");
        setcorpo("");
        setkeys([]);
        if (file === undefined) history.push("/Comunicados");
      })
      .catch(e => {
        console.log(e);
      });
  }

  return <div id='cadNotificacao'>
    <Card id='CardcadNot'>
      <Card.Header id="modalHeader"><Card.Title id='CardcadNot-title'>Cadastrar Comunidado</Card.Title></Card.Header>
      <Card.Body>
        <Form>
          <Form.Group>
            <Form.Label>Título do Comunicado</Form.Label>
            <Form.Control
              value={titulo}
              onChange={(event) => settitulo(event.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Corpo do Comunicado</Form.Label>
            <Form.Control
              as='textarea'
              value={corpo}
              onChange={(event) => setcorpo(event.target.value)}
            />
          </Form.Group>
          <Form.Row id='imput-button'>
            <Form.Group>
              <Form.Label>Enviar arquivo(pdf)</Form.Label>
              <div id='imput-group'>
                <label for="container-cadnotificação-input" id="style-upload-cadnotificação">
                  Upload do Arquivo
                </label>
                <FaUpload style={{ color: 'white' }} />
                <input id="container-cadnotificação-input" type="file" onChange={(event) => fileinput(event)}></input>
              </div>
            </Form.Group>
            <Button onClick={() => setdestin(true)}>Selecionar Destinatários</Button>
          </Form.Row>
        </Form>
      </Card.Body>
      <Card.Footer>
        <BtLoad
          onClick={cadasnotificacao}
          state={stateBt}
          title='Cadastrar'
          id="button-cadastro-notificação"
        />
      </Card.Footer>
    </Card>
    <Modal show={destin} onHide={() => setdestin(false)} id='ModalDist' size='xl'>
      <Modal.Header closeButton id="modalHeader">
        <Modal.Title >Selecionar destinatários</Modal.Title>
      </Modal.Header>
      <Modal.Body id="modalBody">
        <Destinatarios
          alunos={alunos}
          turmas={turmas}
          keys={(event) => setkeys(event)}
          selecionados={keys}
          setselecionados={(event) => setkeys(event)}
          selectView={selectView}
          setselectView={(event) => setselectView(event)}
          closeModal={() => {
            setdestin(false);
            setselectView([{ type: 'Instituição', nome: 'Toda a nstituição', id: '' }]);
          }}
        />
      </Modal.Body>
    </Modal>
    <Modal show={show} style={{
      fontSize: 15, display: 'flex',
      justifyContent: 'center', alignItems: 'center'
    }}>
      <Modal.Title>
        <p id="container-cadnotificação-label">Upload de arquivo</p></Modal.Title>
      <Modal.Body>
        Upload está {Porcentagem.toFixed(2)}% concluido
        </Modal.Body>
    </Modal>
    <Modal show={Notify} onHide={() => setNotify(false)} centered size="lg">
      <Modal.Header id="modalHeader">
        <Modal.Title>Notificações</Modal.Title>
      </Modal.Header>
      <Modal.Body id="modalBody">
      <h5>Deseja enviar notificações para os responsáveis?</h5>
      </Modal.Body>
      <Modal.Footer id="modalBody">
        <Button id="BotaoEditS" onClick={() => sendNotification()}>Sim</Button>
        <Button id="BotaoEditN" onClick={() => setNotify(false)}>Não</Button>
      </Modal.Footer>
    </Modal>
  </div>;
}

async function sendPushNotification(destin) {
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    mode: 'no-cors',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': true,
    },
    body: JSON.stringify({
      to: destin,
      sound: 'default',
      body: 'Novo comunicado!',
      data: { data: 'goes here' },
    }),
  });
}
