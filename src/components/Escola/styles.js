import styled from 'styled-components';

import { Button } from 'react-bootstrap';

export const StyledButton = styled(Button)`
    border: 0;
    border-radius: 2px;
    padding: 5px 20px;
    font-size: 16px;
    font-weight: bold;
    color: "white";
    cursor: pointer;
    margin: 0 2px;
    position: fixed;
    display: flex;

    :hover {
        background: "hover_btn";
    }
`;

export const StyledImg = styled.img`
    width: 80px;
    margin: 0;
    padding: 0 10px;
    max-height: 200px;
    border-radius: 0;
`;

export const Perfil = styled.img`
    
`;


export const Container = styled.div`
  width: 100vw;
  display: flex;
  grid-template-columns: 1fr auto;
  grid-column-gap: 50px;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
  padding: 15px 0px 15px 0px;

  img {
      justify-self:flex-start;
      margin-left:0px;
      max-width:300px;
      display: block;
  }
  span{
    align-items:center;
  }

`;

export const UserMenu = styled.div`
  display:flex;
  justify-content: center;
  margin: 0 auto;
  width: 100%;
    
    
  
`
