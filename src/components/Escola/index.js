import React, { useEffect, useState } from "react";
import {
  Card, Image, Container, Row, Col,
  ListGroup, Button, OverlayTrigger, Tooltip,
  Modal, InputGroup, FormControl, Form, ProgressBar
} from "react-bootstrap";
import { FaUser, FaUserFriends, FaGraduationCap, FaPencilAlt, FaRegUserCircle } from 'react-icons/fa';
import firebase from "../../services/firebase"
import api from "../../services/api"
import { useHistory } from 'react-router-dom'
import './styles.css'
import school from '../../assets/school.svg'


export default function PerfilEscola() {
  const [escola, setescola] = useState({})
  const [perfilGestor, setperfilGestor] = useState({})
  const [alunos, setalunos] = useState('')
  const [turmas, setturmas] = useState('')
  const [Maxalunos, setMaxalunos] = useState(0)
  const [professores, setprofessores] = useState('')
  const [imagem, setimagem] = useState('')
  const [nome, setnome] = useState('')
  const [telefone, settelefone] = useState('')
  const [cpf, setcpf] = useState('')
  const [email, setemail] = useState('')
  const [emailOld, setemailOld] = useState('')
  const [id, setid] = useState('')
  const [show, setshow] = useState(false);
  const [gestor, setgestor] = useState(false);
  const [isLoading, setisLoading] = useState(true);

  const history = useHistory();

  useEffect(() => {
    DataInst();
  }, [])

  function update() {
    const token = localStorage.getItem("token");
    api
      .post("/updateInstituicao",
        {
          UpdateGestor: true,
          gestor: { id: id, email: email, telefone: telefone, cpf: cpf, nome: nome },
          emailOld: emailOld,
          instituicao: localStorage.getItem("instituicaes"),
        },
        {
          headers: { Authorization: `Bearer ${token}` }
        })
      .then(res => {
        setgestor(false);
        setshow(false);
      })
      .catch(e => {
        console.log(e);
        console.log("Erro de autenticação, verifique seu email e senha");
      })
  }

  async function DataInst() {
    const token = localStorage.getItem("token");

    await api
      .put("/getDataInst",
        {
          instituicao: localStorage.getItem("instituicao"),
        }, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {
        setisLoading(false);
        setescola(res.data.escola);
        setperfilGestor(res.data.escola.gestor);
        setnome(res.data.escola.gestor.nome);
        settelefone(res.data.escola.gestor.telefone);
        setemail(res.data.escola.gestor.email);
        setemailOld(res.data.escola.gestor.email);
        setid(res.data.escola.gestor.id)
        setcpf(res.data.escola.gestor.cpf);
        setMaxalunos(res.data.escola.maxAluno)
        setalunos(res.data.numAlunos);
        setturmas(res.data.numTurma);
        setprofessores(res.data.numProf);

        firebase.storage().ref().child(`Admins/${res.data.escola.gestor.id}`).getDownloadURL().then(function (url) {
          setimagem(url)
        });
      })
      .catch(e => {
        console.log(e);
      });
  }


  return (
    <div id="principal-escola">
      <div className="barraPrincipal">
        <Container fluid> 
          <Row className="justify-content-sm-center">
            <Col sm={3}>
            {(imagem !== '') ?
              <Image id="imagem" src={imagem} roundedCircle /> :
              <Image src={school} alt='school' id="imagem" style={{ backgroundColor: 'white' }} roundedCircle/>
            }
            </Col>
          </Row>
          <Row>
            <Col sm={8}>
              <Card style={{ width: '100%', margin: '5px 0 ' }}>
                <Card.Header id="cardHeader">
                  <h6>Sobre</h6>
                  <span>
                    <OverlayTrigger placement="left" overlay={<Tooltip>Visualizar</Tooltip>}>
                      <Button id="iconsPerfil">
                        <FaRegUserCircle size="20px" onClick={() => setgestor(true)} />
                      </Button>
                    </OverlayTrigger>

                    <OverlayTrigger placement="right" overlay={<Tooltip>Editar</Tooltip>}>
                      <Button id="iconsPerfil">
                        <FaPencilAlt size="20px" onClick={() => setshow(true)} />
                      </Button>
                    </OverlayTrigger>
                  </span>

                </Card.Header>
                <ListGroup variant="flush">
                  <ListGroup.Item>Nome: {escola.nomeEmpresarial}</ListGroup.Item>
                  <ListGroup.Item>Sigla: {escola.nomeFantasia}</ListGroup.Item>
                  <ListGroup.Item>CEP: {escola.cep}</ListGroup.Item>
                  <ListGroup.Item>CNPJ: {escola.cnpj}</ListGroup.Item>
                  <ListGroup.Item>Telefone: {escola.telefone}</ListGroup.Item>
                  <ListGroup.Item>Email: {escola.email}</ListGroup.Item>
                </ListGroup>
              </Card>
            </Col>
            <Col sm={4}>
              <Card className="text-center" style={{ margin: '5px 0 ' }}>
                <Card.Header ><h6>Plano Atual</h6></Card.Header>
                <Card.Body>
                  <Card.Title>Plano Básico</Card.Title>
                  <Card.Text>
                    Este plano te dá direito a {Maxalunos} alunos cadastrados
                </Card.Text>
                  <Button id="BotaoCadResp" onClick={() => history.push({
                    pathname: "/Contato",
                    state: {
                      titulo: 'Mudança de plano',
                      escola: escola.nomeEmpresarial,
                      telefone: perfilGestor.telefone,
                      nome: perfilGestor.nome
                    }
                  })} >Alterar meu plano</Button>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>

      <div id="cardsQtd">
        <span id="cardQtd"><FaUserFriends size='20px' id="iconPerfil" />{turmas} Turmas</span>
        <span id="cardQtd"><FaUser size='13px' id="iconPerfil" />{alunos} Alunos </span>
        <span id="cardQtd"><FaGraduationCap size='20px' id="iconPerfil" />{professores} Professores</span>
      </div>

      <Modal show={show} onHide={() => setshow(false)} centered >
        <Modal.Header closeButton id="modalHeader">
          <Modal.Title >Editar Perfil</Modal.Title>
        </Modal.Header>
        <Modal.Body >

          <InputGroup id="editInput">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor">Nome</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              value={nome}
              onChange={event => setnome(event.target.value)}
            />
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor" >Telefone</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              value={telefone}
              onChange={event => { settelefone(event.target.value); }}>
            </Form.Control>

          </InputGroup>
          <InputGroup id="editInput">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor" >Email</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              value={email}
              onChange={event => { setemail(event.target.value); }}>
            </Form.Control>

            <InputGroup.Prepend>
              <InputGroup.Text id="inputColor" >CPF</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              value={cpf}
              onChange={event => setcpf(event.target.value)}
            />
          </InputGroup>

        </Modal.Body>
        <Modal.Footer id="modalBody">
          <Button id="BotaoEditS" onClick={() => update()}>Salvar</Button>
          <Button id="BotaoEditN" onClick={() => setshow(false)}>Descartar</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={gestor} onHide={() => setgestor(false)} centered >
        <Modal.Header closeButton id="modalHeader">
          <Modal.Title >Sobre o Gestor</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup >
            <ListGroup.Item>Nome: {perfilGestor.nome}</ListGroup.Item>
            <ListGroup.Item>Telefone: {perfilGestor.telefone}</ListGroup.Item>
            <ListGroup.Item>CPF: {perfilGestor.cpf}</ListGroup.Item>
            <ListGroup.Item>E-mail: {perfilGestor.email}</ListGroup.Item>
          </ListGroup>
        </Modal.Body>
      </Modal>
      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>
    </div>
  );
}
