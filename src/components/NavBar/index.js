/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { OverlayTrigger, Popover, ListGroup } from "react-bootstrap";
import firebase from "../../services/firebase"
import api from "../../services/api"
import { useHistory } from 'react-router-dom'
import './style.css'
import school from '../../assets/school.svg'


export default function NavBar() {
  const [admin, setadmin] = useState({nome:'', email:'', telefone:''})
  const [imagem, setimagem] = useState(school)
  const history = useHistory();

  function handleClick() {
    firebase.auth().signOut().then(() => {
      localStorage.setItem("token", '');
      localStorage.setItem("instituicao", '');
      history.push('/login')
    });
  }

  useEffect(() => {
    if(!JSON.parse(localStorage.getItem('admin'))){
      Getuid();
    }else{
      setadmin(JSON.parse(localStorage.getItem('admin')));
      setimagem(localStorage.getItem('imgpfl'))

    }
    if (localStorage.getItem("token") === '') {
      alert("desconectado, entre novamente no sistema");
      history.push('/login')
    }

    

  }, []);

  function Getuid() {
    const token = localStorage.getItem("token");
    api.get("/getuid", {
      headers: { Authorization: `Bearer ${token}` }
    })
      .then(res => {
        firebase.firestore().collection('admins').doc(res.data.uid).get().then(doc => {
          setadmin(doc.data())
          localStorage.setItem('admin', JSON.stringify(doc.data()))
          firebase.storage().ref().child(`Admins/${doc.data().id}`).getDownloadURL().then(function (url) {
            setimagem(url)
            localStorage.setItem('imgpfl', url)
          });
        })
      })
      .catch(e => {
        console.log(e);
      });
  }

  const popover = (
    <Popover id="popover-basic-G">
      <Popover.Title as="h3" id='title-NB-G'>{admin.nome}</Popover.Title>
      <Popover.Content>
        <ListGroup id='LG-NB-G'>
          <ListGroup.Item id='item-NB-G'>Telefone: {admin.telefone}</ListGroup.Item>
          <ListGroup.Item id='item-NB-G'>Email: {admin.email}</ListGroup.Item>
          <ListGroup.Item id='item-NB-sair-G' onClick={() => handleClick()}>Sair</ListGroup.Item>
        </ListGroup>
      </Popover.Content>
    </Popover>
  );


  return (
    <div id="container-nav">
      <div className='position-nav'>
          <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
          <div id="container-nav2">
            <span>Olá, {admin.nome}</span>
           
            <img src={imagem} alt="Admin" className="avatar" style={{
                    width: imagem ? '10vh' : 0,
                    height: imagem ? '10vh' : 0
                }}/> 
            </div>
          </OverlayTrigger>
        
      </div>
    </div>
  );
}
