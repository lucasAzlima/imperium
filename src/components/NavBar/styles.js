import styled from 'styled-components';

import { Button } from 'react-bootstrap';

export const StyledButton = styled(Button)`
    border: 0;
    border-radius: 2px;
    padding: 5px 20px;
    font-size: 16px;
    font-weight: bold;
    color: "white";
    cursor: pointer;
    margin: 0 2px;
    position: fixed;
    display: flex;

    :hover {
        background: "hover_btn";
    }
`;

export const StyledImg = styled.img`
    width: 80px;
    margin: 0;
    padding: 0 10px;
    max-height: 200px;
    border-radius: 0;
`;

export const Perfil = styled.img`
    width: 80px;
    margin: 0;
    padding: 0 10px;
    max-height: 200px;
    border-radius: 10px;
    margin: 5px;
    cursor: pointer;
`;


export const Container = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr auto;
  grid-column-gap: 50px;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
  padding: 15px 0px 15px 0px;

  img {
      justify-self:flex-start;
      margin-left:0px;
      max-width:300px;
      display: block;
  }
  span{
    align-items:center;
  }

`;

export const UserMenu = styled.div`
  display:flex;
  justify-content: flex-end;
  align-items: center;

  form {
    background: white;
    max-width:600px;
    display: flex;
    border-radius: 10px;
    height: 45px;
    flex-direction: row;
    align-items: center;
    box-shadow: 1px 4px 5px rgba(0, 0, 0, 0.45);

  span {
    color:black;
    font-size:16px;
    margin-left: 15px;
  }

  svg{
        margin-left: 15px;
        margin-right: 15px;
        color: black;
        width:100%;
        height: 30px;
        max-width:30px;
      }

  button {
      background: transparent;
      border: none;
    }
  }
`
