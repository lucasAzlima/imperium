/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Card, Button, ListGroup, Row, Col, Form } from 'react-bootstrap';
import {  Link } from 'react-router-dom';
import NavBar from "../../components/NavBar";
import SideBar from "../../components/SideBar";
import { FiArrowLeft } from 'react-icons/fi'
import api from "../../services/api";
import { Container } from "./styles";

function Responsavel({ history }) {
    const [dataResp, setdataResp] = useState([])
    const [dataRespT, setdataRespT] = useState([])
    const [responsaveis, setresponsaveis] = useState([])
    const [uid, setuid] = useState({})
    const [instituicao, setinstituicao] = useState({})
    const [editar, seteditar] = useState(true)
    const [email, setemail] = useState('')
    const [endereco, setendereco] = useState('')
    const [emailOld, setemailOld] = useState('')
    const [cpf, setcpf] = useState('')
    const [nome, setnome] = useState('')

    useEffect(() => {
        set(history.location.state.data)
        Getdata(history.location.state.data.uid)
        setinstituicao(localStorage.getItem('instituicao'))
        getResponsaveis()
    }, []);

    function set(data) {
        setemail(data.email)
        setemailOld(data.email)
        setnome(data.nome);
        setcpf(data.cpf);
        setendereco(data.endereco)
    }

    function getResponsaveis() {
        const token = localStorage.getItem("token");

        api
            .get("/getResponsaveis", {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                setresponsaveis(res.data)
            })
            .catch(e => {
                console.log(e);
            });
    }

    function Getdata(uid) {
        setuid(uid);
        var inst = localStorage.getItem('instituicao')
        const token = localStorage.getItem("token");
        api.post("/getVinculos",
            { uid: uid },
            {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                setdataRespT(res.data)
                var data = []
                res.data.forEach(vinc => {
                    if (vinc.idInstituicao === inst) {
                        data.push(vinc)
                    }
                })
                setdataResp(data)
            })
            .catch(e => {
                console.log(e);
            });
    }

    function updateResponsaveis() {
        const token = localStorage.getItem("token");
        api.post("/updateResponsaveis",
            {
                id: uid,
                emailOld: emailOld,
                email: email,
                cpf: cpf,
                nome: nome,
                endereco: endereco
            },
            {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
            })
            .catch(e => {
                console.log(e);
            });
    }


    function excluirResp() {
        var alunoSelect = []
        dataRespT.forEach(element => {
            alunoSelect.push({
                idDependente: element.idDependente,
                idInstituicao: element.idInstituicao,
                contasAtivas: -1
            })
        })

        var respDel = responsaveis.filter(Doc => Doc.uid !== uid);

        const token = localStorage.getItem("token");
        api.put("/deleteResp",
            {
                uid: uid,
                responsaveis: respDel,
                alunos: alunoSelect,
                instituicao: instituicao
            },
            {
                headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => {
                history.push('/responsaveis')
            })
            .catch(e => {
                console.log(e);
            });

    }

    return (

        <Container fluid>
            <Row>
                <Col sm={2}>
                    <SideBar />
                </Col>
                <Col sm={10}>
                    <NavBar />
                    <div id="listagem">
                        <div id='EditResp'>

                            <Row>
                                <Col sm={2}>
                                    <Link id="bt-return" to="/responsaveis">
                                        <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                                        Retornar
                                        </Link>
                                </Col>
                                <Col sm={8}><h5>Dados do Responsável</h5></Col>
                            </Row>

                            <hr />
                        </div>

                        <div id="CardResp">
                            <Form id="CardFormRes">
                                <Row>
                                    <Col>
                                        <Form.Label>Nome</Form.Label>
                                        <Form.Control onChange={(e) => setnome(e.target.value)} value={nome} disabled={editar} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Label>CPF</Form.Label>
                                        <Form.Control onChange={(e) => setcpf(e.target.value)} value={cpf} disabled={editar} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control onChange={(e) => setemail(e.target.value)} value={email} disabled={editar} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Label>Endereço</Form.Label>
                                        <Form.Control onChange={(e) => setendereco(e.target.value)} value={endereco} disabled={editar} />
                                    </Col>
                                </Row>

                            </Form>

                            <Row >
                                <Col  >{(editar)
                                    ? <Button id="BotaoTelaRespo" onClick={() => seteditar(!editar)}>Editar</Button>
                                    : <Button id="BotaoTelaRespo" variant='danger' onClick={() => seteditar(!editar)}>Descartar</Button>}
                                    <Button id="BotaoTelaRespo" variant='danger' onClick={() => excluirResp()}>Excluir</Button></Col>
                                {(!editar) ?
                                    <Col sm={2}><Button id="BotaoTelaRespo" onClick={() => updateResponsaveis()}>Atualizar</Button></Col> :
                                    <div />}
                            </Row>

                        </div>

                        <Card>

                            <Card.Header>Alunos tutorados na instituição <Button id="BotaoTelaRespo"
                                onClick={() =>
                                    history.push({
                                        pathname: '/alunoDependente',
                                        state: { instituicao: instituicao, uid: uid }
                                    })}>Editar tutorados</Button></Card.Header>

                            <ListGroup variant="flush">
                                {dataResp.map(vinc =>
                                    <ListGroup.Item style={{ display: 'flex', flexDirection: 'row', }}>
                                        <Card.Body>
                                            <Card.Subtitle>Nome</Card.Subtitle>
                                            <Card.Text>{vinc.dependente}</Card.Text>
                                        </Card.Body>
                                        <Card.Body>
                                            <Card.Subtitle>Instituição</Card.Subtitle>
                                            <Card.Text>{vinc.instituicao}</Card.Text>
                                        </Card.Body>
                                    </ListGroup.Item>)}
                            </ListGroup>
                        </Card>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default Responsavel;