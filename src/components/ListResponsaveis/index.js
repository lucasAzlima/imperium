/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { Table, Form, Button, OverlayTrigger, Tooltip, Modal,ProgressBar } from 'react-bootstrap';
import "./style.css";
import { FaAddressCard } from "react-icons/fa";
import { BsSearch } from 'react-icons/bs';
import ReactPaginate from 'react-paginate';




export default function ListResponsaveis({ history }) {
  const [responsaveis, setresponsaveis] = useState([]);
  const [filter, setfilter] = useState("");
  const [postData, setpostData] = useState('');
  const [offset, setoffset] = useState(0);
  const perPage = 5;
  const [pagecount, setpagecount] = useState(0);
  const [offsetOld, setoffsetOld] = useState(0);
  const [forceNew, setforceNew] = useState(0);
  const [forceOld, setforceOld] = useState(0);
  const [isLoading, setisLoading] = useState(true);
  

  useEffect(() => {
    GetREsp();
  }, []);

  useEffect(() => { receivedData(offset, filter); }, [offset, filter, responsaveis])

  function GetREsp() {
    const token = localStorage.getItem("token");

    api
      .get("/getResponsaveis", {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(res => {

        if (res.data.length !== 0)
          setresponsaveis(res.data);
        else
        setresponsaveis([{
                nome: "Nenhum dado cadastrado",
                email: "-",
                cpf: "-",
            }])
        setisLoading(false);
      })
      .catch(e => {
        console.log(e);
      });
  }

  const lowercasedFilter = filter.toLowerCase();
  const filteredData = responsaveis.filter(item => {
    return Object.keys(item).some(key =>
      item[key].toLowerCase().includes(lowercasedFilter)
    );
  });

  function receivedData(offset, filter) {
    const slice = filteredData.slice(offset, offset + perPage)
    const postData1 = slice.map(data =>
      <tr>
        <td id='bordaArreEsq' >{data.nome}</td>
        <td>{data.email}</td>
        <td>{data.cpf}</td>
        <td id='bordaArreDir'>
          <OverlayTrigger placement="right" overlay={<Tooltip>Visualizar</Tooltip>}>
            <Button id="BotaoResp" disabled={(data.email === '-')} onClick={() => history.push({ pathname: '/responsavel', state: { data: data } })}>
              <FaAddressCard size="24px" />
            </Button>
          </OverlayTrigger>
        </td>
      </tr>)
    setpagecount(Math.ceil(filteredData.length / perPage))
    setpostData(postData1);
  }

  useEffect(()=>{
    if(filter !== ''){
      setoffset(0)
      setforceNew(0)
    }
    else {
      setoffset(offsetOld)
      setforceNew(forceOld)
    }
  },[filter]);

  function handlePageClick(e) {
    const selectedPage = e.selected;
    setoffset(selectedPage * perPage);
    setforceNew(selectedPage)
    if(filter === ''){
      setforceOld(selectedPage)
      setoffsetOld(selectedPage * perPage)
    }
  };

  return (
    <>

      <div id="tagBarraPesquisa" >
        <span className="barraDePesquisa">
          <Form.Control id="barraPesquisa" value={filter} onChange={event => setfilter(event.target.value)} placeholder={'Pesquisar por Responsável'} />
          <span id="iconPesquisa"  >
            <BsSearch size='18px' />
          </span>

        </span>

        <Button id="BotaoCadResp" onClick={() => history.push('/register')}>Cadastrar Responsável</Button>
      </div>

      <div id='listagem' >
        <Table id="tabelaPadrao" borderless striped responsive="sm" >
          <thead >
            <tr >
              <th>Nome</th>
              <th >Email</th>
              <th >CPF</th>
              <th >Ações</th>
            </tr>
          </thead>
          <tbody >
            {postData}
          </tbody>
        </Table>


        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pagecount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"} 
          forcePage={forceNew}/>

      </div>

      <Modal show={isLoading} size='lg' centered animation>
          <Modal.Body>
              <ProgressBar animated now={100} />
          </Modal.Body>
      </Modal>

    </>

  );
}
