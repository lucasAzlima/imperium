import React, { useState, useEffect } from 'react';
import { Button, Table, Form , OverlayTrigger, Tooltip, Modal} from 'react-bootstrap';
import {TiArrowLeft} from 'react-icons/ti';
import './styles.css';
import { BsSearch } from 'react-icons/bs';
import api from "../../services/api";
import { useHistory } from 'react-router-dom';

export default function Teste(props) {
  const [alunos, setalunos] = useState([{ nome: 'testeF', notas:[1,2,3,4,5,6] }]);
  const [alunosList, setalunosList] = useState([]);
  const [turma, setturma] = useState([]);
  const [disciplina, setdisciplina] = useState([]);
  const EstrutEduc = Number(localStorage.getItem("EstrutCurric"));
  const [editor,seteditor] = useState(false);
  const [matrix, setMatrix] = useState(Array.from({ length: alunos.length }, () => Array.from({ length: EstrutEduc }, () => 0)));
  const [filter, setfilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  const history = useHistory();
  const filteredData = alunos.filter(item => {
    return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
  });




  useEffect(() => { 
    const putNotes = (materias,alunos, turma) => {
      var AlunosList = alunosList;
      var matriz = Array.from({ length: materias.alunos.length }, 
        () => Array.from({ length: turma.EstrutEstudo }, () => 0))
      alunos.forEach((data, lin) => {
        AlunosList.push({ nome: data.nome, lin, id: data.id })
        Array(EstrutEduc).fill(0).forEach((index, col) => {
          matriz[lin][col] = Number(data.notas[col]);
            })});
      setMatrix(matriz);
      setalunosList(AlunosList);
    }   
    putNotes(
      props.history.location.state.disciplina, 
      props.history.location.state.disciplina.alunos, 
      props.history.location.state.turma)
    setdisciplina(props.history.location.state.disciplina)
    setturma(props.history.location.state.turma)
    setalunos(props.history.location.state.disciplina.alunos)
  }, [props.history.location.state.disciplina, 
    props.history.location.state.disciplina.alunos, 
    props.history.location.state.turma, EstrutEduc, alunosList])

  const handleChange = (row, column, event) => {
    let copy = [...matrix];
    copy[row][column] = +event.target.value;
    setMatrix(copy);
  };

  function media(tabela) {
    var notaP = 0;
    for (let i = 0; i < EstrutEduc; i++) {
      notaP += tabela[i];
    }
    return notaP / EstrutEduc;
  }

  function upgrad(){
    let aln = [...alunos];
    let disc = disciplina
    alunosList.forEach((aluno, indx)=>{
      aln[indx].notas = matrix[indx]
    })
    var mat= turma.materias.filter(Doc => Doc.id !== disciplina.id);
    disc.alunos = aln;
    mat.push(disc);
    var notasAl = [];
    turma.alunos.forEach((aluno, index) =>{
      notasAl.push({id:aluno.id, notas:[]});
      var nt =[];
      turma.materias.forEach(materia=>{
        materia.alunos.forEach(aln=>{
          if(aln.id===aluno.id){nt.push({materia:materia.nome, notas:aln.notas, professor: materia.professor}); return}
        })
      })
      Object.assign(notasAl[index].notas, nt);
    })

    addnotas(mat, notasAl)
  }

  function addnotas(materias, notasAlunos){
    const token = localStorage.getItem("token");
    api
      .put(
        "addNotas",
        {
          materias: materias,
          id: turma.id,
          notas:notasAlunos,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(res => {
      })
      .catch(e => {
        console.log(e);
      });
  }

  return (
    <div>
      
      <div id="header-todas-asnotas">
        <span className="barraDePesquisa">
            <Form.Control
            id="barraPesquisa"
            value={filter}
            onChange={event => setfilter(event.target.value)}
            placeholder={'Pesquisar por Nome ou Turma'}
          />
          <span>
            <BsSearch type="button" id="iconPesquisa"/>
          </span>
        </span>
        <Button onClick={()=>{upgrad();seteditor(true)}}>Salvar alterações</Button>
      </div>
      <div id='todasNotas'>
        <div id='title-notas'>
          <OverlayTrigger placement="right" overlay={<Tooltip>Retornar</Tooltip>}>
            <Button id="BotaoResp" onClick={() => history.push("/turma")}>
              <TiArrowLeft size="35" color={'gray'}></TiArrowLeft>
            </Button>
          </OverlayTrigger>
          <p>{disciplina.nome}</p>
        </div>
        <Table borderless striped responsive="sm">
          <thead>
            <tr >
              <th id="nome">Nome</th>
              {Array(EstrutEduc).fill(0).map((d, indx) => <th>{`Nota ${indx + 1}`}</th>)}
              <th>Média</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map((data, lin) => {
            
              return (
                <tr>
                  <td><p>{data.nome}</p></td>
                  {Array(EstrutEduc).fill(0).map((index, col) => {
                    return (
                      <td><Form.Control
                        type='number'
                        value={matrix[lin][col]}
                        onChange={e => handleChange(lin, col, e)} />
                      </td>)
                  })}
                  <td><Form.Control disabled value={media(matrix[lin])} /></td>
                </tr>
              )
            })}
          </tbody>
        </Table>
        </div>
        <Modal show={editor} onHide={()=> seteditor(false)}>
          <Modal.Header>
            <Modal.Title id="title-modal-notasSalvar">Ação do Administrador</Modal.Title>
          </Modal.Header>
          <Modal.Body id="body-modal-notasSalvar">Suas alterações foram salvas :D, deseja continuar modificando? </Modal.Body>
          <Modal.Footer id="footer-modal-notasSalver">
            <Button variant="primary" id="button-modal-dicTurma" onClick={()=>seteditor(false)}>Sim</Button>
            <Button variant="primary" id="button-modal-dicturmaNo" onClick={()=>{seteditor(false);history.push('/turma')}}>Não</Button>
          </Modal.Footer>
        </Modal>
    </div>
  );
}
