import React, { useState } from "react";
import './styles.css'
import { Form, Button, Tabs, Tab, Table } from 'react-bootstrap'
import { BsSearch } from 'react-icons/bs';

export default function Destinatarios({
    keys, alunos, turmas,
    selecionados, setselecionados,
    closeModal, setselectView, selectView }) {
  
    const [filter, setfilter] = useState('');
    const lowercasedFilter = filter.toLowerCase();
    const filteredData = alunos.filter(item => {
      return Object.keys(item).some(key => item['nome'].toLowerCase().includes(lowercasedFilter));
    });
  
    function select({ id, nome, state, type }) {
      if (state) {
        if (selecionados.includes(localStorage.getItem('instituicao'))) { selecionados = []; selectView = [] }
        var select1 = selectView.slice();
        var select2 = selecionados.slice();
        select1.push({ type: type, nome: nome, id: id });
        select2.push(id);
        setselectView(select1);
        setselecionados(select2);
        alunos.forEach(aluno => (aluno.state === false && aluno.id === id) ? aluno.state = true : null)
        turmas.forEach(turma => (turma.state === false && turma.id === id) ? turma.state = true : null)
        keys(select2)
  
      } else {
        alunos.forEach(aluno => (aluno.state === true) ? aluno.state = false : null)
        turmas.forEach(turma => (turma.state === true) ? turma.state = false : null)
        keys(selecionados.filter(Doc => Doc !== id))
        setselectView(selectView.filter(Doc => Doc.id !== id));
        setselecionados(selecionados.filter(Doc => Doc !== id));
  
  
      }
    }
  
    return <div id='selectDist'>
      <Tabs className='abas' defaultActiveKey="Instituicao" transition={false}>
        {/* Aba de Instituição */}
        <Tab eventKey='Instituicao' title='Instituição'>
          <div className="Instituicao">
            <p>O destinatário como instituição, mandará a
                            notificação para todos os responsáveis que tem acesso ao App</p>
            <Button onClick={() => {
              keys([localStorage.getItem('instituicao')]);
              alunos.forEach(aluno => aluno.state = false);
              turmas.forEach(turma => turma.state = false);
              closeModal();
            }}>Selecionar</Button>
          </div>
  
        </Tab>
        {/* Aba de alunos */}
        <Tab eventKey='Alunos' title='Alunos'>
          <span className="filter-cadNot">
            <Form.Control
              id='form-cadnot'
              value={filter}
              onChange={event => setfilter(event.target.value)}
              placeholder={'Pesquisar por Nome ou Turma'}
            />
            <BsSearch id='icon' size='18px' />
          </span>
          <div id='table-alunos'>
  
            <Table borderless striped responsive="sm" id='Table'>
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Grau</th>
                  <th>Turma</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                {filteredData.map((aluno, _id) =>
                  <Aluno
                    ano={aluno.turma.ano}
                    nome={aluno.nome}
                    nivel={aluno.nivel}
                    key={_id}
                    id={aluno.id}
                    select={select}
                    estado={aluno.state}
                  />
                )}
              </tbody>
  
            </Table>
          </div>
        </Tab>
        {/* Aba de Turmas */}
        <Tab eventKey='Turmas' title='Turmas'>
          <div id='table-alunos'>
            <Table borderless striped responsive="sm">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Sala</th>
                  <th>Turno</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                {turmas.map((turma, _id) =>
                  <Turma
                    ano={turma.ano}
                    turno={turma.turno}
                    sala={turma.sala}
                    key={_id}
                    id={turma.id}
                    select={select}
                    estado={turma.state}
                  />
                )}
              </tbody>
            </Table>
          </div>
        </Tab>
        <Tab eventKey='Destinatarios' title='Destinatários selecionados'>
          <div id='table-alunos'>
            <Table borderless striped responsive="sm">
              <thead>
                <tr>
                  <th>Tipo</th>
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                {(selectView.length!==0)?selectView.map((item, _id) =>
                  <tr key={_id}>
                    <td>{item.type}</td>
                    <td>{item.nome}</td>
                  </tr>
                ):<h5>Nenhum destinatário foi selecioado ainda</h5>}
              </tbody>
            </Table>
          </div>
        </Tab>
      </Tabs>
    </div>;
  }
  
  function Turma({ ano, turno, sala, key, id, estado, select }) {
    const [state, setstate] = useState(false);
    if (estado && state === false) setstate(true);
    return (
      <tr
        style={{ cursor: 'pointer', fontWeight: (state | estado) ? 'bold' : 'normal' }}
        key={key}
        onClick={() => { select({ id: id, nome: ano, state: !state, type: 'Turma' }); setstate(!state); }}>
        <td>{ano}</td>
        <td>{sala}</td>
        <td>{turno}</td>
        <td>{state ? <p>Selecionado</p> : <p>Não Selecionado</p>}</td>
      </tr>
  
    )
  }
  
  function Aluno({ ano, nome, nivel, key, estado, id, select }) {
    const [state, setstate] = useState(false);
    if (estado && state === false) setstate(true);
    return (
      <tr
        style={{ cursor: 'pointer', fontWeight: (state | estado) ? 'bold' : 'normal'}}
        key={key}
        onClick={() => { select({ id: id, nome: nome, state: !state, type: 'Aluno' }); setstate(!state); }}>
        <td>{nome}</td>
        <td>{nivel}</td>
        <td>{ano}</td>
        <td>{state ? <p>Selecionado</p> : <p>Não Selecionado</p>}</td>
      </tr>
  
    )
  }
  