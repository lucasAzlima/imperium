const axios = require("axios");

const api = axios.create({
  baseURL: "https://us-central1-palmcontrol-29e35.cloudfunctions.net"
});

export default api;