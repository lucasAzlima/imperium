import React from 'react';
import { Route, Redirect, useHistory } from "react-router-dom";
import firebase from '../services/firebase'

function Routes({ permissions, component, path }) {
    const history = useHistory();
    firebase.auth().onAuthStateChanged(user => {
        if (user);
        else return false
    });

    function CheckPermission() {
        const userPermissions = JSON.parse(localStorage.getItem('Permissions'))
        const Permissions = permissions;

        if ((userPermissions.admin === Permissions.admin) &&
            (userPermissions.gestor === Permissions.gestor)) return true;
        else return false
    }

    function Rota() {
        return <>
            { CheckPermission() ? <Route component={component} path={path} /> : (
                alert("Usuário não têm permissão para usar essa sessão!"),
                history.goBack())}
        </>
    }

    function IsAuthenticated() {
        
        var user = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                user.getIdToken(true).then(function (idToken) {
                    localStorage.setItem("token", idToken);
                })
                return true
            }
            else return false
        });
        return (user) ? <Rota /> : <Redirect to="/login" />;
    }
    
    return <IsAuthenticated />
}

export default Routes;