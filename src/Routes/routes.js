import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PrivateRoute from './PrivateRoute';

import TelaLogin from "../pages/telaLogin";
import TelaDashboard from "../pages/telaDashboard";
import telaCadastral from "../pages/Aluno/telaCad";
import TelaCadNoticia from "../pages/Noticia/telaCadNoticia";
import TelaHome from "../pages/telaHome/Home";
import TelaListagemTurmas from "../pages/Turma/telaListagemTurmas";
import TelaRegister from "../pages/Responsaveis/telaRegister";
import TelaEditAluno from "../pages/Aluno/telaEditAluno";
import TelaListAluno from "../pages/Aluno/telaListAluno";
import TelaListNoticia from "../pages/Noticia/telaListNoticia";
import TelaEditNoticia from "../pages/Noticia/telaEditarNoticia";
import TelaEditNotify from "../pages/Notificados/telaEditNotify";
import TelaCadProf from "../pages/Professor/telaCadProf";
import TelaAdmin from "../pages/Admin/telaDashAdmin";
import TelaDisciplinas from "../pages/telaDisciplinas";
import TelaCameras from "../pages/telaCameras";
import TelaNotificacao from "../pages/Notificados/telaNotificacao";
import TelaCadNot from "../pages/Notificados/telaCadNotify";
import TelaListResp from "../pages/Responsaveis/telaListResponsaveis";
import TelaAlunoDependente from "../pages/Responsaveis/telaTutorados";
import TelaResponsavel from "../components/ListResponsaveis/responsavel";
import TelaForm from "../pages/telaForm";
import SystemWebPage from '../pages/telaHome/SytemWeb';
import SystemMoboPage from '../pages/telaHome/SytemMob';
import TelaTurma from "../pages/Turma/telaTurma";
import TelaListInst from "../pages/Admin/telaListInstituicoes";
import TelaNewInst from "../pages/Admin/telaNewInstituicao";
import TelaEditInstituicao from "../pages/Admin/telaEditInstituicao"
import TelaInst from "../pages/Admin/telaInstituicao";
import TelaAdmins from "../pages/Admin/telaAdmins";
import TelaCadAdmins from "../pages/Admin/telaCadAdmin";
import TelaEditAdmins from "../pages/Admin/telaEditAdmin";
import TelaRecSenha from "../components/ForgotPasswordPage";
import teste from "../pages/telaTeste";
import TelaViewAluno from "../pages/Aluno/TelaViewAluno";
import TelaPerfilProfessor from "../pages/Professor/telaPerfilProfessor";
import TelaNotasDisc from "../pages/Turma/telaNotasDisc";
import TelaNotasAluno from "../pages/Turma/telaNotasAluno";
import TelaListAlunoTurma from "../pages/Turma/telaListAlunoTurma";
import TelaAddAlunoTurma from "../pages/Turma/telaAddAlunoTurma";
import TelaPerfilEscola from "../pages/Escola/telaPerfilEscola";
import TelaContato from "../pages/Escola/telaContato";
import TelaDashAdmin from "../pages/Admin/telaDashAdmin";
import TelaPlanos from "../pages/Admin/telaPlanos";


const AdminPermissons = { admin: true, gestor: true }
const GestorPermissons = { admin: false, gestor: true }

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={TelaHome} />
      <Route path="/login" component={TelaLogin} />
      <Route path="/form" component={TelaForm} />
      <Route path="/sistemaweb" component={SystemWebPage} />
      <Route path="/sistemamobo" component={SystemMoboPage} />
      <Route path="/teste" component={teste} />
      <Route path="/forgotpassword" component={TelaRecSenha} />

      <PrivateRoute path="/dashboard" component={TelaDashboard} permissions={GestorPermissons} />
      <PrivateRoute path="/cadastroDeAlunos" component={telaCadastral} permissions={GestorPermissons} />
      <PrivateRoute path="/cadastroDeNoticias" component={TelaCadNoticia} permissions={GestorPermissons} />
      <PrivateRoute path="/turmas" component={TelaListagemTurmas} permissions={GestorPermissons} />
      <PrivateRoute path="/turma" component={TelaTurma} permissions={GestorPermissons} />
      <PrivateRoute path="/register" component={TelaRegister} permissions={GestorPermissons} />
      <PrivateRoute path="/Alunos" component={TelaListAluno} permissions={GestorPermissons} />
      <PrivateRoute path="/editarAluno" component={TelaEditAluno} permissions={GestorPermissons} />
      <PrivateRoute path="/Noticias" component={TelaListNoticia} permissions={GestorPermissons} />
      <PrivateRoute path="/EditarNoticia" component={TelaEditNoticia} permissions={GestorPermissons} />
      <PrivateRoute path="/editarNotificacao" component={TelaEditNotify} permissions={GestorPermissons} />
      <PrivateRoute path="/Professores" component={TelaCadProf} permissions={GestorPermissons} />
      <PrivateRoute path="/Disciplinas" component={TelaDisciplinas} permissions={GestorPermissons} />
      <PrivateRoute path="/Cameras" component={TelaCameras} permissions={GestorPermissons} />
      <PrivateRoute path="/comunicados" component={TelaNotificacao} permissions={GestorPermissons} />
      <PrivateRoute path="/cadnotify" component={TelaCadNot} permissions={GestorPermissons} />
      <PrivateRoute path="/responsaveis" component={TelaListResp} permissions={GestorPermissons} />
      <PrivateRoute path="/responsavel" component={TelaResponsavel} permissions={GestorPermissons} />
      <PrivateRoute path="/alunoDependente" component={TelaAlunoDependente} permissions={GestorPermissons} />
      <PrivateRoute path="/VerAluno" component={TelaViewAluno} permissions={GestorPermissons} />
      <PrivateRoute path="/Perfil" component={TelaPerfilProfessor} permissions={GestorPermissons} />
      <PrivateRoute path="/NotasDisciplina" component={TelaNotasDisc} permissions={GestorPermissons} />
      <PrivateRoute path="/NotasAluno" component={TelaNotasAluno} permissions={GestorPermissons} />
      <PrivateRoute path="/Alunosturma" component={TelaListAlunoTurma} permissions={GestorPermissons} />
      <PrivateRoute path="/AddAluno" component={TelaAddAlunoTurma} permissions={GestorPermissons} />
      <PrivateRoute  path="/PerfilEscola" component={TelaPerfilEscola} permissions={GestorPermissons}/>
      <PrivateRoute  path="/Contato" component={TelaContato} permissions={GestorPermissons} />

      <PrivateRoute path="/Admin" component={TelaAdmin} permissions={AdminPermissons} />
      <PrivateRoute path="/ListInstituicoes" component={TelaListInst} permissions={AdminPermissons} />
      <PrivateRoute path="/novaInst" component={TelaNewInst} permissions={AdminPermissons} />
      <PrivateRoute path="/EditInstituicao" component={TelaEditInstituicao} permissions={AdminPermissons}/>
      <PrivateRoute path="/instituicao" component={TelaInst} permissions={AdminPermissons} />
      <PrivateRoute path="/ListAdmins" component={TelaAdmins} permissions={AdminPermissons} />
      <PrivateRoute path="/CadAdmins" component={TelaCadAdmins} permissions={AdminPermissons} />
      <PrivateRoute path="/EditAdmin" component={TelaEditAdmins} permissions={AdminPermissons} />
      <PrivateRoute path="/DashAdmin" component={TelaDashAdmin} permissions={AdminPermissons} />
      <PrivateRoute path="/Planos" component={TelaPlanos} permissions={AdminPermissons} />

    </Switch>
  </BrowserRouter>
);

export default Routes;
 