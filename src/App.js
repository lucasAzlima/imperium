import React from 'react';
import Routes from "./Routes/routes"
import 'bootstrap/dist/css/bootstrap.min.css'
import "./styles.css";

const App = () => (
    <div className="App">
      <Routes />
    </div>
  );

export default App;
