import React from 'react';

import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import { Container, Col, Row} from "./styles";
import './styles.css'
import Alunodependente from '../../../components/NewDependente'

export default function Disciplinas(props){
    return (
      <div class='Fundotutorados'>
        <Container >
          <Row>
            <Col grid={2}><SideBar/></Col>
          
            
            <Col grid={10}>
              <NavBar />
              <Alunodependente history={props.history}/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }