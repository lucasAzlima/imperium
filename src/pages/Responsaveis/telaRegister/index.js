import React, { Component } from "react";
import "./styles.css";
import SideBar from "../../../components/SideBar";
import NavBar from "../../../components/NavBar";
import { Container ,Row, Col} from "./styles.js";
import Register from "../../../components/Register";


export default class TelaRegister extends Component {
  render() {
    return (
      <Container >
        <Row>
          <Col grid={2}><SideBar/></Col>
        
            
            <Col grid={10}>
              <NavBar />
              <Register history={this.props.history} />
            </Col>
        </Row>
        
      </Container>
    );
  }
}
