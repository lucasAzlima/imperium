import React from "react";
import "./styles.css";

import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import { Container, Col, Row} from "./styles";
import ListagemResp from "../../../components/ListResponsaveis";

export default function TelaListagemTurmas(props){
    return (
      <>
      <div class='FundoLista'>
        <Container >
          <Row>
            <Col grid={2}><SideBar/></Col>
          
            
            <Col grid={10}>
              <NavBar />
              
              <ListagemResp history={props.history}/>
            </Col>
          </Row>
        </Container>
      </div>
      </>
    );
  }

