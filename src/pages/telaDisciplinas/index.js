import React, { Component } from "react";

import "./styles.css";
import SideBar from "../../components/SideBar";
import NavBar from "../../components/NavBar";
import { Container, Col, Row, } from "./styles.js";
import Disciplinas from "../../components/NavDisciplinas";

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div className="FundoLista">
        
        
          <Container>
            <Row>
              <Col grid={2}>
              <SideBar />
              </Col>

              <Col grid={10}>

              <NavBar />

              <Disciplinas key='TelaDisc' history={this.props.history} />

              </Col>
              

            </Row>
          </Container>
        
      </div>
    );
  }
}
