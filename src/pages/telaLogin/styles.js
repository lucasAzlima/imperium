import styled from 'styled-components';


export const StyledHr = styled.hr`
  background-color: #328cb4;
  height: 0.2px;
`;


export const StyledImg = styled.img`
  align-self: middle;
  border-radius: 0;
  max-width: 80%;
  margin: 10px 0;
  
`;


