import React, { Component } from "react";
import "./styles.css";
import Login from "../../components/Login";

export default class TelaLogin extends Component {
  constructor() {
    super();
    this.state = {
      login: "",
      Senha: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  Clogin = evento => {
    this.setState({ login: evento.target.value });
  };
  Csenha = evento => {
    this.setState({ senha: evento.target.value });
  };

  handleSubmit(event) {
    alert("Dados submetidos");
    event.preventDefault();
  }

  render() {
    return (
      <div className="tela-inicial-login">
        <div className="pi">
          <div className="container-login">
            <div className="content-login">
              <Login history={this.props.history} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
