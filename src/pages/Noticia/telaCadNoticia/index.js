import React, { Component } from "react";

import "./styles.css";
import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import { Container, Row, Col } from "./styles.js";
import CadastroNoticia from "../../../components/CadastroNoticia";

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <Container id='fundocadnoticias'>  
      <Row>
          <Col grid={2}>
              <SideBar/>
          </Col>
          <Col grid={10}>
              <NavBar history={this.props.history}/>
              <CadastroNoticia history={this.props.history}/>
          </Col>
      </Row>
  </Container>
    );
  }
}
