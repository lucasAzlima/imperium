import React, { Component } from "react";
import "./styles.css";

import Teste from "../../components/teste";
import SideBar from "../../components/SideBar";

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div className="container-teste">
          <div>
            <SideBar/>
          </div>
          <div>
               
                <Teste/>
          </div>
      </div>
    );
  }
}
