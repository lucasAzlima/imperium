import React, { Component } from "react";

import "./styles.css";
import SideBar from "../../../components/SideBar";
import NavBar from "../../../components/NavBar";
import { Container , Col} from "./styles.js";
import EditarAluno from "../../../components/EditarAluno"
import { Row } from 'react-bootstrap';

export default class telaEditAluno extends Component {

  render() {
    return (
      <Container >
        <Row>
          <Col grid={2}><SideBar/></Col>
        </Row>
        <Row>
            <Col grid={2}></Col>
            <Col grid={10}>
              <NavBar history={this.props.history}/>
              <EditarAluno data={this.props.location.state.data}/>
            </Col>
        </Row>
        
      </Container>
    );
  }
}