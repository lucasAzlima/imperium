import React, { Component } from 'react';


import './styles.css';
import SideBar from "../../../components/SideBar";
import NavBar from '../../../components/NavBar';
import { Container, Col } from './styles.js';
import CadastroAluno from '../../../components/CadastroAluno';
import { Row } from 'react-bootstrap';

export default class telaCadastral extends Component{

    render() {
        return(
            <Container id='form-aluno' >  
                <Row>
                    <Col grid={2}><SideBar/></Col>
                    
                    
                </Row>
                <Row>
                    <Col grid={2}></Col>
                    <Col grid={10} >
                        
                        <NavBar history={this.props.history}/>

                        <CadastroAluno history={this.props.history}/>
                    </Col>
                </Row>
                
            </Container>
        );
    }
}