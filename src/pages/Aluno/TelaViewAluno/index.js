import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi'
import './style.css';
import { Container, Col, Row} from "./style.js";
import NavBar from '../../../components/NavBar';
import SideBar from '../../../components/SideBar';
import PerfilAluno from '../../../components/PerfilAluno';
import ResponsavelAluno from '../../../components/ResponsavelView';
import ComunicadoAluno from '../../../components/ComunicadoAluno';
import {Button, Tabs, Tab} from 'react-bootstrap';


export default class TelaViewAluno extends Component {
    render() {
        return (
            <div className="img-back">
                <Container>
                    <Row>
                        <Col grid={2}>
                            <SideBar />
                        </Col>
                        <Col grid={10}>
                            <NavBar history={this.props.history} />
                                <div>
                                    <div>
                                        <Link to="/Alunos">
                                            <Button className="button-teste-return" variant="light">
                                                <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                                                Retornar
                                            </Button>
                                        </Link>
                                    </div>
                                </div>
                                <div id='fundo-View-Aluno'>
                                    <Tabs ActiveKey="Perfil" className="Tab-Style">
                                        <Tab eventKey="Perfil" title="Perfil">
                                            <PerfilAluno history={this.props.history}></PerfilAluno>
                                        </Tab>
                                        <Tab eventKey="Responsavel" title="Responsável">
                                           <ResponsavelAluno history={this.props.history}></ResponsavelAluno>
                                        </Tab>
                                        <Tab eventKey="Comunicados" title="Comunicados">
                                            <ComunicadoAluno history={this.props.history}></ComunicadoAluno>
                                        </Tab>
                                    </Tabs>
                                </div>
                         </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
