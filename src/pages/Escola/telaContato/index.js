import React,{Component} from 'react';
import SideBar from '../../../components/SideBar';
import NavBar from '../../../components/NavBar';
import Contato from '../../../components/Contato';
import {Container, Col, Row} from './styles.js';
import './style.css';

export default class TelaContato extends Component{
    render(){
        return(
            <div id="img-back-telaContao">
                <Container>
                    <Row>
                        <Col grid={2}>
                            <SideBar/>
                        </Col>
                        <Col grid={10}>
                            <NavBar history={this.props.history}></NavBar>
                            <Contato history={this.props.history}></Contato>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    };
}
