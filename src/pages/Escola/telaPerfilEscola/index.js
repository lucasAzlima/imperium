import React from 'react';
import Escola from '../../../components/Escola';
import NavBar from '../../../components/NavBar';
import SideBar from '../../../components/SideBar';
import Fundo from '../../../components/Fundo'

import "./styles.css";
import { Container, Row, Col } from "./styles.js";

export default function PerfilEscola() {

    
        return (
            <div className="escolaperfilteste">
                <Container fluid>
                    <Row>
                    <Col grid={2}>
                        <SideBar/>
                    </Col>
                    <Col grid={10}>
                        <NavBar />
                        <Fundo>
                            <Escola/>
                        </Fundo>
                        
                    </Col>

                    </Row>
                </Container>
                
                    
                
            
            </div>
            
        );
    
}