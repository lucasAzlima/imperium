import styled from 'styled-components';

//import { Card } from 'react-bootstrap';

export const Container = styled.div`


    
    height: 100vh;
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    overflow-x: hidden;
    
    
    
`;

export const Styledh4 = styled.h4`
    text-align: center;
    margin-bottom: 40px;
    color: #185296;
`;

export const Row = styled.div`
    height: 100%;
    float: left;
    box-sizing: border-box;
    margin: 0;
`

export const Col = styled.div`
    float: left;
    min-height: 1px;
    box-sizing: border-box;
    @media only screen and (min-width: 1px) {
        width: ${props => (props.grid ? props.grid / 12 * 100 : '8:33')}%;
    }
    
`;