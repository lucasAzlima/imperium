import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../components/NavBar";
import SideBar from "../../components/SideBar";
import { Container, Col, Row, Styledh4} from "./styles";
import Cameras from "../../components/Cameras";

export default class TelaCadNoticia extends Component {
  render() {
    return (
        <div class='FundoLista'>
          <Container fluid>
            <Row>
              <Col grid={2}><SideBar/></Col>
              <Col grid={10}>
                <NavBar/>
                <Styledh4>
                  Câmeras Cadastradas
                </Styledh4>
                <Cameras key='cam' history={this.props.history} />
                </Col>
            </Row>
            
              
              
            
          </Container>
        </div>
    );
  }
}
