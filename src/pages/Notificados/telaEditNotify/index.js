import React, { Component } from "react";
import {Link} from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi'
import {Button} from 'react-bootstrap';
import "./styles.css";
import Sidebar from "../../../components/SideBar";
import NavBar from "../../../components/NavBar";
import EditarNotificacao from "../../../components/EditarNotificacao";

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div className="container-editarnotificação-back">
        <div>
          <Sidebar/>
        </div>
        <div>
          <div>
            <NavBar />
          </div>
          <div className="container-editnotify">
            <div className="return-button-lisandedi">
              <Link to="/Comunicados">
                  <Button variant="light" id="button-return-notificação"> 
                      <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                          Retornar
                  </Button>
              </Link>
            </div>
            <EditarNotificacao 
                history={this.props.history} 
                data={this.props.location.state.data}/>
          </div>
        </div>
      </div>
    );
  }
}
