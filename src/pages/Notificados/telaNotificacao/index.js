import React, { Component } from "react";
import "./styles.css";

import { Container, Col,Row } from "./styles";
import NavBar from "../../../components/NavBar";
import Notificacoes from "../../../components/ListNotificacao";
import SideBar from "../../../components/SideBar";

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div class='FundoLista'>
        <Container >
          <Row>
            <Col grid={2}><SideBar/></Col>
          
            <Col grid={10}>
              <NavBar />
              
              <Notificacoes history={this.props.history}/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
