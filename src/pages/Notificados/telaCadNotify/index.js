import React, { Component } from "react";

import "./styles.css";

import NavBar from "../../../components/NavBar";
import CadNotify from "../../../components/Notificacao";
import SideBar from "../../../components/SideBar";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { FiArrowLeft } from 'react-icons/fi'

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div className="container-notificação-back">
        <div className="side-bar-notificação">
            <SideBar></SideBar>
        </div>
        <div className="body-notificação">
            <div className="nav-bar-notficação"> 
              <NavBar></NavBar>
            </div>
            <div className="container-cadnotify">
              <div>
                <Link to="/Comunicados">
                    <Button variant="light" id="button-return-notificação"> 
                      <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                            Retornar
                    </Button>
                  </Link>
              </div>
              <CadNotify 
                  history={this.props.history} 
                  id={this.props.location.state.id}
                  alunos={this.props.location.state.alunos}
                  turmas={this.props.location.state.turmas}/>
            </div>
        </div>
      </div>
    );
  }
}
