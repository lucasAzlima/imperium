import React, { Component } from "react";

import "./styles.css";
import SideBar from "../../../components/SideBar";
import NavBar from "../../../components/NavBar";
import { Container, Styledh4, Col, Row } from "./styles.js";
import Professor from "../../../components/Professor";


export default class TelaCadNoticia extends Component {
  render() {
    return (
      
        <Container id='FundoListaProf' >
          <Row>
            <Col grid={2}><SideBar/></Col>
          
            <Col grid={10}>
              <NavBar history={this.props.history}/>
              <Styledh4> Lista de Professores</Styledh4>
              <Professor history={this.props.history} />  
            </Col>
          </Row>
          
        </Container>
      
      
    );
  }
}
