import styled from 'styled-components';

//import { Card } from 'react-bootstrap';

export const Container = styled.div`
width:100%;
height:100%;

/* max-width: 1000px; */
display: flex;
flex-direction: column;
box-sizing: border-box;
&:before,
&:after {
    content: "";
    display: table;
}
&:after {
    clear: both;
}
`;

export const Content = styled.div`
    width: 100%;
    background: #FFF;
    margin-top: 8px;
    border-radius: 4px;
    padding: 30px;
`;

export const Styledh4 = styled.h4`
    text-align: center;
`;
export const Row = styled.div`
    height: 100%;
    float: left;
    box-sizing: border-box;
    &:before,
    &:after {
        content: " ";
        display: table;
    }
    &:after {
        clear: both;
    }
`
export const Col = styled.div`
    float: left;
    min-height: 1px;
    box-sizing: border-box;
    @media only screen and (min-width: 786px) {
        width: ${props => (props.grid ? props.grid / 12 * 100 : '8:33')}%;
    }
`;

