import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/NavBar";
import PerfilProfessor from "../../../components/PerfilProfessor";
import SideBar from "../../../components/SideBar";
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import { FiArrowLeft } from 'react-icons/fi';

export default class TelaCadNoticia extends Component {
  render() {
    return (
      <div className="container-professor-global">
          <div>
            <SideBar/>
          </div>
          <div>
              <div>
                <NavBar history={this.props.history}></NavBar>
              </div>
              <div>
                <div className="ajust-buttonreturn-perfilaluno">
                    <Link to="/Professores" >
                      <Button variant="light" id="button-return-notificação"> 
                          <FiArrowLeft size={25} color='#323bb4'></FiArrowLeft>
                              Retornar
                      </Button>
                    </Link>
                </div>
                <PerfilProfessor history={this.props.history}/>
              </div>
          </div>
      </div>
    );
  }
}
