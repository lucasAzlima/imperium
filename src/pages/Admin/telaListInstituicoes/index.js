import React, { Component } from "react";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import Intituicoes from "../../../components/Admin/ListIntituicoes";
import { Container, Row, Col } from "./styles.js";
import "./style.css";

export default class TelaDashboard extends Component {
  render() {
    return (
      <Container id='fundotli'>
        <Row>
          <Col grid={2}>
            <SideBar />
          </Col>
          <Col grid={10}>
            <NavBar history={this.props.history} />
            <Intituicoes history={this.props.history} />
          </Col>
        </Row>
      </Container>
    );
  }
}
