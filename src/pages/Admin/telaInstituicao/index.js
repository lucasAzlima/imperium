import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import { Container, Row, Col } from "./styles.js";
import Intituicao from "../../../components/Admin/Instituicao";

export default class TelaCadNoticia extends Component {
  render() {
    return (

      <Container id='fundoI'>
        <Row>
          <Col grid={2}>
            <SideBar />
          </Col>
          <Col grid={10}>
              <NavBar history={this.props.history} />
              <Intituicao history={this.props.history} />
          </Col>
        </Row>
      </Container>
    );
  }
}
