import React, { Component } from "react";

import "./style.css";

import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import DashboardContentAd from "../../../components/Admin/DashboardContent";
import { Container, Row, Col } from "./styles.js";

export default class TelaDashboard extends Component {
  render() {
    return (
      <div id='fundoda'>
        <Container>  
            <Row>
                <Col grid={2}>
                    <SideBar/>
                </Col>
                <Col grid={10}>
                    <NavBar history={this.props.history}/>
                    <DashboardContentAd history={this.props.history}/>
                </Col>
            </Row>
        </Container>
      </div>
    );
  }
}
