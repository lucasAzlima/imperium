import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import { Container, Row, Col } from "./styles.js";

import EditAdmin from "../../../components/Admin/EditAdmin";

export default class TelaCadNoticia extends Component {
  render() {
    return (
       <Container id='fundoEA'>  
       <Row>
           <Col grid={2}>
               <SideBar/>
           </Col>
           <Col grid={10}>
               <NavBar history={this.props.history}/>
               <div id='fundoEA2'> 
               <EditAdmin  history={this.props.history} />
               </div>
           </Col>
       </Row>
   </Container>
    );
  }
}
