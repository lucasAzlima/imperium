import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import Planos from "../../../components/Admin/Planos";
import { Container, Row, Col } from "./styles.js";

export default class TelaPlanos extends Component {
  render() {
    return (
      <Container id='fundoad'>
        <Row>
          <Col grid={2}>
            <SideBar />
          </Col>
          <Col grid={10}>
            <NavBar history={this.props.history} />
            <Planos history={this.props.history} />
          </Col>

        </Row>

      </Container>
    );
  }
}
