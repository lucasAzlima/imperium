import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import { Container, Row, Col } from "./styles.js";
import EditIntituicao from "../../../components/Admin/EditInstituicao";
import Fundo from "../../../components/Fundo"

export default class EditInstituicao extends Component  {
    render() {
    return (
        <>
            <Container>
                <Row>
                    <Col grid={2}>
                        <SideBar/>
                    </Col>
                    <Col grid={10}>
                        <NavBar/>
                        <Fundo>
                         <EditIntituicao history={this.props.history}/>
                        </Fundo>
                        
                    </Col>
                </Row>
            </Container>
        </>
    );
    }
}