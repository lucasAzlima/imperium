import React, { Component } from "react";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import CadAdmins from "../../../components/Admin/CadAdmin";
import { Container, Row, Col } from "./styles.js";
import "./style.css";

export default class TelaNewInstituicao extends Component {
  render() { 
    return ( 
      <Container id='fundotni'>  
      <Row>
          <Col grid={2}>
              <SideBar/>
          </Col>
          <Col grid={10}>
              <NavBar history={this.props.history}/>
              <CadAdmins  history={this.props.history} />
          </Col>
          
      </Row>
      
  </Container>
    );
  }
}
