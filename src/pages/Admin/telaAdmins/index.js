import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import Admins from "../../../components/Admin/ListAdmins";
import { Container, Row, Col } from "./styles.js";

export default class TelaAdmins extends Component {
  render() {
    return (
        <Container id='fundoad'>  
        <Row>
            <Col grid={2}>
                <SideBar/>
            </Col>
            <Col grid={10}>
                <NavBar history={this.props.history}/>
              <Admins  history={this.props.history} />
            </Col>
            
        </Row>
        
    </Container>
    );
  }
}
