import React, { Component } from "react";
import NavBar from "../../../components/Admin/NavBar";
import SideBar from "../../../components/Admin/SideBar";
import Intituicao from "../../../components/Admin/NewInstituicao";
import { Container, Row, Col } from "./styles.js";
import "./style.css";

export default class TelaNewInstituicao extends Component {
  render() { 
    return ( 
      <Container id='fundotNI'>  
        <Row>
            <Col grid={2}>
                <SideBar/>
            </Col>
            <Col grid={10}>
                <NavBar history={this.props.history}/>
                <Intituicao  history={this.props.history} />
            </Col>
        </Row>
      
  </Container>
    );
  }
}
