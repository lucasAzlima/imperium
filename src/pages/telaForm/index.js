import React from "react";
import "./styles.css";
import { Container } from "./styles.js";
import FormSolicit from "../../components/FormSolicit";

export default function FormInscricao(props) {
  
    return (
      
        <div id="fundoform">
            <Container >
              <FormSolicit history={props.history} />
            </Container>
        </div>
    );
  
}
