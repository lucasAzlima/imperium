import React, { Component } from "react";
import "./style.css";
import NavBar from "../../components/NavBar";
import SideBar from "../../components/SideBar";
import DashboardContent from "../../components/DashboardContent";
import { Container, Row, Col } from "./styles.js";

export default class TelaDashboard extends Component {
  render() {
    return (
     
      <Container id='fundodgestor'>  
      <Row>
          <Col grid={2}>
              <SideBar/>
          </Col>
          <Col grid={10}>
              <NavBar history={this.props.history}/>
              <DashboardContent history={this.props.history}/>
          </Col>
      </Row>
      
  </Container>
    );
  }
}
