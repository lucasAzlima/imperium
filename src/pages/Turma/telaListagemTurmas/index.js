import React from "react";
import "./styles.css"

import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar"
import { Container, Row, Col } from "./styles";
import ListagemTurmas from "../../../components/ListagemTurmas";

export default function TelaListagemTurmas(props){
    return (
      <div class='FundoLista'>
          <Container fluid>
            <Row>
              <Col grid={2}>
                <SideBar/>
              </Col>
              <Col grid={10}>
                <NavBar />
              
                <ListagemTurmas history={props.history}/>
              </Col>

            </Row>
          </Container>
        
      </div>
    );
  }

