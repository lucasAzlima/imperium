import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import {Container, Col, Row} from './styles.js';
import Notas from "../../../components/NotasAluno";

export default class TelaEditAluno extends Component {
  render() {
    return(
      <div className="img-back-Notas">
        <Container>  
              <Row>
                  <Col grid={2}>
                      <SideBar/>
                  </Col>
                  <Col grid={10} id="container-ajust-global">
                      <NavBar history={this.props.history}/>
                      <Notas id="container-list-Notas" history={this.props.history} />
                 </Col>
              </Row>
          </Container>
        </div>
    );
  }
}
