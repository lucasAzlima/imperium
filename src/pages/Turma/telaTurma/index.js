import React, { Component } from "react";
import "./styles.css";
import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import {Container, Col, Row} from './styles.js';
import Turma from "../../../components/Turma";

export default class TelaEditAluno extends Component {
  render() {
    return(
      <div className="img-back-turma">
        <Container>  
              <Row>
                  <Col grid={2}>
                      <SideBar/>
                  </Col>
                  <Col grid={10} id="container-ajust-global">
                      <NavBar history={this.props.history}/>
                      <Turma id="container-list" history={this.props.history}/>
                 </Col>
              </Row>
          </Container>
        </div>
    );
  }
}
