import React from 'react';

import NavBar from "../../../components/NavBar";
import SideBar from "../../../components/SideBar";
import { Container, Col, Row} from "./styles";

import AddAluno from '../../../components/AddAlunoTurma'

export default function Disciplinas(props){
    return (
      <div class='FundoLista'>
        <Container >
          <Row>
            <Col grid={2}><SideBar/></Col>
          
            
            <Col grid={10}>
              <NavBar />
              
              <AddAluno history={props.history}/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }